<?php

namespace App\Console\Commands;

use Log;
use Illuminate\Console\Command;
use App\Services\CustomerService;
use Illuminate\Support\Facades\DB;

class UpdateCustomerOrderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customer:total_order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update total money order customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            $customerService = new CustomerService();
            $count = $customerService->cronJobCalculateOrder();
            
            DB::commit();

            //send output to the console
            $this->info('Success!');
            Log::info(__CLASS__.'- Success: '.$count);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info(__CLASS__.'- Failed: '.$e->getMessage());
            $this->error($e->getMessage());
        }
    }
}
