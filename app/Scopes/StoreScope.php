<?php

namespace App\Scopes;

use App\Models\Store;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;

class StoreScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $user = Auth::user();
        $activeStoreId = $user->store_active;

        if($activeStoreId){
            $builder->where('store_id', (int)$activeStoreId);
        }
        else{
            $storeDefault = Store::getDefaultStore();
            $builder->where('store_id', (int)$storeDefault);
        }
    }
}