<?php
namespace App\Exceptions;

class ApiHandleException extends \Exception
{
    const CODE_MANY_REQUEST = -32;

    const ZALO_ERROR_MESSAGE = [
        self::CODE_MANY_REQUEST => "Vượt quá giới hạn request/phút. Chi tiết tại đây",
    ];

    const GUZZLE_ERROR = 502;
    const GUZZLE_ERROR_MESSAGE = "Không kết nối được server";
    const ERROR_HTTP = 400;
    const ERROR_HTTP_MESSAGE = "Yêu cầu không hợp lệ";

    protected $subMessage;
    protected $errorCode;

    public function __construct($errorMessage, $subMessage = null, $errorCode = null){
        $this->subMessage = $subMessage;
        $this->errorCode = $errorCode;
        parent::__construct($errorMessage);
    }

    public function getSubMessage(){
        return $this->subMessage;
    }

    public function getErrorCode(){
        return $this->errorCode;
    }
}