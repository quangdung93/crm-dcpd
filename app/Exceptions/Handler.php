<?php

namespace App\Exceptions;

use App\Exceptions\ApiHandleException;
use App\Traits\ResponseFormatTrait;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    use ResponseFormatTrait;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Validation\ValidationException::class,
        ApiHandleException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (ApiHandleException $e) {
            return $this->responseJson(CODE_ERROR, null, $e->getMessage(), $e->getSubMessage(), $e->getErrorCode());
        });
    }
}
