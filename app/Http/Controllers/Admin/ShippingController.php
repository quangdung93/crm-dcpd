<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Carbon\Carbon;
use App\Api\GHTKApi;
use App\Models\Ward;
use App\Models\Order;
use App\Models\District;
use App\Models\Province;
use App\Models\Shipping;
use Illuminate\Http\Request;
use App\Models\ShippingPartner;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class ShippingController extends Controller
{
    protected $GHTKApi;

    public function __construct(GHTKApi $GHTKApi)
    {
        $this->GHTKApi = $GHTKApi;
    }

    public function index(){
        return view('admin.shippings.index');
    }

    public function getDatatable(){
        $object = Shipping::orderBy('id', 'DESC')->select('*');
        return $this->renderDatatable($object);
    }

    public function renderDatatable($table){
        $data = Datatables::of($table)
            ->editColumn('code', function ($row) {
                return '<a href="#" class="label label-warning">'.$row->code.'</a>';
            })
            ->editColumn('order_id', function ($row) {
                return '<a href="#" class="text-info btn-detail-order" data-id="'.$row->order_id.'" data-code="'.$row->order->code.'" data-toggle="modal" data-target="#modal-order-detail">'.$row->order->code.'</a>';
            })
            ->editColumn('shipping_parner_id', function ($row) {
                return 'Giao hàng tiết kiệm';
            })
            ->editColumn('fee', function ($row) {
                return number_format($row->fee).' đ';
            })
            ->editColumn('status', function ($row) {
                return '<span class="label label-info">'.$row->getShippingStatus().'</span>';
            })
            // ->editColumn('info', function ($row) {
            //     $name = '<div>';
            //     $name .= '<div class="text-left">Nhân viên: '.optional($row->user)->name.'</div>';
            //     $name .= '<div class="text-left">Khách hàng: '.optional($row->customer)->name.'</div>';
            //     $name .= '<div class="text-left">Ngày bán: '.Carbon::parse($row->sell_date)->format('d/m/Y').'</div>';
            //     $name .= '<div class="text-left">Ngày chăm sóc: '.Carbon::parse($row->customer_date)->format('d/m/Y').'</div>';
            //     $name .= '<div class="text-left">Nguồn: '.renderOrderSource($row->source_id).'</div>';
            //     $name .= '</div>';

            //     return $name;

            // })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";
                if($user->can('delete_shippings')){
                    $action .= '<a href="'.url('admin/orders/delete/'.$row->id).'" class="btn btn-danger notify-confirm" title="Xóa">
                        <i class="feather icon-trash-2"></i>
                    </a>';
                }
                $action .= '<a href="'.url('admin/shippings/order/'.$row->order_id).'" class="btn btn-success"><i class="feather icon-eye" title="Xem"></i></a>';

                return $action;
            })
            ->rawColumns(['order_id', 'code', 'info', 'status', 'action'])
            ->make(true);
        return $data;
    }

    public function getShipping($id){
        $order = Order::where('id', $id)->with('customer', 'detail')->first();
        $shipping = Shipping::where('order_id', $id)->first();

        if(!$order){
            return abort(404);
        }

        $customer = $order->customer;
        $shippingPartners = ShippingPartner::all();
        $provinces = Province::all();
        $listPickingApi = $this->GHTKApi->getListPicking();

        $listPicking = [];
        if($listPickingApi['success']){
            $listPicking = $listPickingApi['data'];
        }

        return view('admin.shippings.add', [
            'order' => $order,
            'customer' => $customer,
            'listPicking' => $listPicking,
            'shippingPartners' => $shippingPartners,
            'provinces' => $provinces,
            'shipping' => $shipping,
        ]); 
    }

    public function postShipping(Request $request, $orderId){
        $validator = \Validator::make($request->all(), [
            'shipping_province' => 'required',
            'shipping_district' => 'required',
            'shipping_ward' => 'required',
            'shipping_address' => 'required',
            'value' => 'required',
            'pick_money' => 'required',
            'shipping_phone' => 'required',
        ],[
            'shipping_province.required' => 'Bạn chưa nhập tỉnh/thành phố giao hàng',
            'shipping_district.required' => 'Bạn chưa nhập quận/huyện giao hàng',
            'shipping_ward.required' => 'Bạn chưa nhập phường/xã giao hàng',
            'shipping_address.required' => 'Bạn chưa nhập địa chỉ giao hàng',
            'shipping_phone.required' => 'Bạn chưa nhập số điện thoại giao hàng',
            'value.required' => 'Giá trị hàng không được trống',
            'pick_money.required' => 'Thu tiền COD không được trống',
        ]);

        if($validator->fails()) {
            return $this->responseJson(CODE_ERROR, null, $validator->errors()->first());
        }

        $order = Order::where('id', $orderId)->with('customer', 'detail')->first();
        $customer = $order->customer;

        if(!$order || !$order->detail){
            return $this->responseJson(CODE_ERROR, null, 'Đơn hàng không tồn tại!');
        }

        $dataProducts = [];
        foreach ($order->detail as $detail) {
            $dataProducts[] = [
                'name' => $detail->name,
                'quantity' => $detail->pivot->qty
            ];
        }

        $dataOrder = [
            'id' => $request->order_private_id ? "$request->order_private_id" : "$order->id",
            'pick_address_id' => $request->pick_address_id,
            'pick_name' => "Đẳng Cấp Phái Đẹp HCM",
            'pick_address' => $request->pick_address,
            'pick_province' => "TP. Hồ Chí Minh",
            'pick_district' => 'Quận Phú Nhuận',
            'pick_ward' => 'Phường 15',
            'pick_tel' => $request->pick_tel,
            'tel' => $request->shipping_phone,
            'name' => $customer->name,
            'address' => $request->shipping_address,
            'province' => $request->shipping_province,
            'district' => $request->shipping_district,
            'ward' => $request->shipping_ward,
            'hamlet' => "Khác",
            'is_freeship' => $request->is_freeship ? 1 : 0,
            'pick_date' => format_date($request->pick_date, 'Y-m-d'),
            'pick_money' => (int)$request->pick_money ? str_replace(',', '', $request->pick_money) : 0,
            'note' => $request->note,
            'value' => (int)str_replace(',', '', $request->value),
            'transport' => 'road',
            'pick_option' => 'cod',
            'total_weight' => $request->total_weight,
            'tags' => $request->tags
            // 'deliver_option' => $request->shipping_phone,
            // 'pick_session' => $request->shipping_phone,
        ];

        $dataShipping['products'] = $dataProducts;
        $dataShipping['order'] = $dataOrder;

        $sendOrderApi = $this->GHTKApi->sendOrder($dataShipping);

        $sendOrder = [];
        if($sendOrderApi['success']){
            $sendOrder = $sendOrderApi['order'];
        }

        if($sendOrder){
            $dataCreateShipping = [
                'code' => $sendOrder['label'],
                'partner_id' => $sendOrder['partner_id'],
                'tracking_id' => $sendOrder['tracking_id'],
                'sorting_code' => $sendOrder['sorting_code'],
                'is_xfast' => $sendOrder['is_xfast'],
                'area' => $sendOrder['area'],
                'fee' => $sendOrder['fee'],
                'insurance_fee' => $sendOrder['insurance_fee'],
                'estimated_pick_time' => $sendOrder['estimated_pick_time'],
                'estimated_deliver_time' => $sendOrder['estimated_deliver_time'],
                'status_id' => $sendOrder['status_id'],
                'pick_address_id' => $request->pick_address_id,
                'shipping_parner_id' => 1,
                'order_id' => $order->id,
                'shipping_address' => $request->shipping_address,
                'shipping_province' => $request->shipping_province,
                'shipping_district' => $request->shipping_district,
                'shipping_ward' => $request->shipping_ward,
                'is_freeship' => $request->is_freeship ? 1 : 0,
                'pick_date' => format_date($request->pick_date, 'Y-m-d'),
                'pick_money' => $request->pick_money ? str_replace(',', '', $request->pick_money) : 0,
                'note' => $request->note,
                'value' => str_replace(',', '', $request->value),
            ];

            $create = Shipping::create($dataCreateShipping);
            if ($create) {
                $order->status = Order::SHIPPING;
                $order->save();
                
                return $this->responseJson(CODE_SUCCESS, $create, 'Đã tạo vận đơn');
            }
            else{
                return $this->responseJson(CODE_SUCCESS, null, 'Đã tạo vận đơn');
            }
        }

        return $this->responseJson(CODE_ERROR, null, 'Xử lý thất bại!');
    }

}
