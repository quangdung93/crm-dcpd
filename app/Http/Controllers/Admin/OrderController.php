<?php

namespace App\Http\Controllers\Admin;

use N2W;
use PDF;
use Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Brand;
use App\Models\Order;
use App\Models\Store;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Supplier;
use App\Models\Inventory;
use App\Models\Warehouse;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Models\ProductExport;
use App\Services\DebtService;
use App\Services\UserService;
use App\Services\OrderService;
use App\Models\InventoryDetail;
use Yajra\DataTables\DataTables;
use App\Services\CustomerService;
use Illuminate\Support\Facades\DB;
use App\Models\ProductExportDetail;
use App\Http\Controllers\Controller;
use App\Models\Debt;
use Illuminate\Support\Facades\Cache;
use Illuminate\Validation\ValidationException;

class OrderController extends Controller
{
    protected $orderService;
    protected $userService;
    protected $customerService;

    public function __construct(
        OrderService $orderService,
        UserService $userService,
        CustomerService $customerService
    ){
        $this->orderService = $orderService;
        $this->userService = $userService;
        $this->customerService = $customerService;
    }

    public function index(){
        return view('admin.orders.index');
    }

    public function getDatatable(){
        $query = Order::query();

        //Lấy DSKH theo quyền user
        $userIds = $this->userService->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('saler_id', $userIds);
        }

        //Kiểm tra quyền Đại lý/Khách lẻ
        $query = $this->customerService->buildQueryCheckRole($query);

        $orders = $query->with('user')->orderBy('id', 'DESC')->select('*');
        return $this->orderService->renderDatatable($orders);
    }

    public function getLists(Request $request){
        $query = Order::query();
        $exportOrderId = '';

        //Lấy DSKH theo quyền user
        $userIds = $this->userService->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('saler_id', $userIds);
        }

        if($request->code){
            $keyword = trim($request->code);
            $query->where('code', $keyword);
        }

        if($request->export_order_id){
            $exportOrderId = $request->export_order_id;
        }
        
        if((int)$request->status >= 0){
            $query->where('status', (int)$request->status);
        }

        if((int)$request->source > 0){
            $query->where('source_id', (int)$request->source);
        }

        if((int)$request->payment > 0){
            $query->where('payment_method', (int)$request->payment);
        }

        $orders = $query->with('user', 'customer')->orderBy('id', 'DESC')->select('*');

        return $this->orderService->renderDatatable($orders, $exportOrderId);
    }

    public function detail($id){
        $order = Order::findOrFail($id)->load('detail');
        return view('admin.orders.detail')->with([
            'order' => $order
        ]);
    }

    public function create(Request $request){
        $customers = Customer::all();
        if($request->customer_id){
            $customer = Customer::where('id', $request->customer_id)->first();
        }
        $warehouses = Warehouse::all();

        $queryUser = User::active();

        $userIds = $this->userService->getUserIdsByPermission();
        if($userIds){
            $queryUser->whereIn('id', $userIds);
        }

        $users = $queryUser->get();

        return view('admin.orders.add')->with([
            'customers' => $customers,
            'customer' => $customer ?? null,
            'warehouses' => $warehouses,
            'users' => $users,
        ]);
    }

    public function store(Request $request){
        $request->validate([
            'products' => 'required',
            'customer_id' => 'required',
        ],[
            'products.required' => '(*) Bạn chưa chọn sản phẩm',
            'customer_id.required' => '(*) Bạn chưa chọn Khách hàng',
        ]);

        DB::beginTransaction();

        $customer = Customer::where('id', $request->customer_id)->first();
        //Check points
        if($customer && $request->points && (int)$request->points > $customer->points){
            return back()->withErrors(["points" => "(*) Điểm tích lũy khách hàng không đủ"])->withInput();
        }

        $data = $request->except('_token', 'products', 'points', 'submit');
        $data['status'] = 0;
        $data['total_quantity'] = array_sum(array_column($request->products, 'qty'));
        $data['sell_date'] = format_date(Carbon::now(), 'Y-m-d');
        $data['code'] = $this->orderService->getOrderCode();
        $data['created_by'] = Auth::id();
        $data['total_price'] = $request->total_price ? str_replace(',', '', $request->total_price) : 0;
        $discount = str_replace(',', '', $request->coupon);
        $data['coupon'] = $discount;
        $data['total_money'] = str_replace(',', '', $request->total_money);
        $data['customer_pay'] = str_replace(',', '', $request->customer_pay);
        $data['lack'] = str_replace(',', '', $request->lack);
        $data['customer_date'] = format_date(str_replace('/', '-', $request->customer_date), 'Y-m-d');

        //Lưu tạm điểm tích lũy quy đổi (Khi trang thái đơn hàng là hoàn thành, hệ thống sẽ trừ điểm tích lũy này của KH)
        if($request->points){
            $data['exchange_points'] = $request->points;
        }

        $order = Order::create($data);

        if($order){
            $products = $request->products;
            foreach($products as $product){
                $productQty = $product['qty'] ? (int)$product['qty'] : 0;
                $productDiscount = $product['discount'] ? (float)$product['discount'] : 0;
                $productPrice = $product['price'] ? (int)str_replace(',', '', $product['price']) : 0;
                $orderDetail = [
                    'order_id' => $order->id,
                    'product_id' => $product['id'],
                    'qty' => $productQty,
                    'price' => $productPrice,
                    'discount' => $productDiscount,
                    'subtotal' => $productQty * ($productPrice - $productPrice * $productDiscount/100),
                ];

                OrderDetail::create($orderDetail);
            }

            //Lưu công nợ KH
            if($data['lack'] > 0){

                //Lưu vào bảng công nợ
                $debtCode = (new DebtService())->getCode();
                $dataDebt = [
                    'store_id' => $data['store_id'],
                    'code' => $debtCode,
                    'document_id' => $order->id,
                    'object_id' => $customer->id,
                    'total' => $data['total_money'],
                    'debt' => $data['lack'],
                    'pay' => $data['customer_pay'],
                    'created_by' => Auth::id()
                ];

                Debt::create($dataDebt);

                //Lưu vào KH
                $totalLack = $customer->total_lack;
                $customer->total_lack = $totalLack + $data['lack'];
                $customer->save();
            }
            
            DB::commit();

            if($request->submit == 'print'){
                return redirect(route('order.print', ['id' => $order->id]));
            }

            return redirect('admin/orders')->with('success', 'Tạo thành công!');
        }
        else{
            DB::rollback();
            return redirect('admin/orders/create')->with('danger', 'Tạo thất bại!');
        }
    }

    public function detailOrder(Request $request, $id){
        try {
            $order = Order::where('id', $id)->with('detail')->first();
            $result = $this->orderService->renderOrderDetail($order, $request->type);
            return $this->responseJson(CODE_SUCCESS, $result);
        } catch (\Throwable $th) {
            return $this->responseJson(CODE_ERROR, null, $th->getMessage());
        }
    }

    public function getOrdersByCustomer($customerId){
        try {
            $orders = Order::where('customer_id', $customerId)->get();

            return $this->responseJson(CODE_SUCCESS, $orders);
        } catch (\Throwable $th) {
            return $this->responseJson(CODE_ERROR, null, $th->getMessage());
        }
    }

    public function printOrder($orderId){
        $order = Order::where('id', $orderId)->with('customer', 'detail')->first();

        if(!$order){
            return abort(404);
        }

        $moneyToVietnamese = N2W::toCurrency($order->total_money);

        $store = Store::where('id', $order->store_id)->first();

        $pdf = PDF::loadView('admin.orders.print', [
            'order' => $order,
            'store' => $store,
            'moneyToVietnamese' => $moneyToVietnamese
        ]);

        $pdf->setOptions(['isRemoteEnabled' => true]);

        return $pdf->stream();
    }

    public function destroy($id){
        $order = Order::findOrFail($id);
        $delete = $order->delete();

        if($delete){
            return redirect('admin/orders')->with('success', 'Xóa thành công!');
        }
    }

    public function updateApi(Request $request){
        try {
            DB::beginTransaction();

            $orderId = $request->order_id;
            $status = $request->status;
            $adminStatus = $request->admin_status;

            $order = Order::where('id', $orderId)->first();

            if(!$order){
                return $this->responseJson(CODE_ERROR, null, 'Không tìm thấy đơn hàng!');
            }

            $oldOrder = $order;

            $dataUpdate = [];

            if(isset($request->status)){
                $dataUpdate['status'] = $status;
            }

            if(isset($request->admin_status)){
                $dataUpdate['admin_status'] = $adminStatus;
            }

            $update = $order->update($dataUpdate);

            if ($update) {
                // Hoàn kho nếu đơn hàng bị hủy/thất bại
                if($oldOrder->detail && in_array($status, [Order::FAILED, Order::CANCEL])){
                    foreach ($oldOrder->detail as $detail) {
                        $productId = $detail->id;
                        $orderDetailId = $detail->pivot->id;
                        $exportDetail = ProductExportDetail::where('order_detail_id', $orderDetailId)->first();
                        if($exportDetail){ //Sản phẩm đã xuất kho
                            $exportId = $exportDetail->export_id;
                            $export = ProductExport::where('id', $exportId)->first();
                            $export->status = ProductExport::CANCEL;
                            $export->notes = "Đơn hàng ".renderOrderStatus($status);
                            $export->save();

                            //
                            InventoryDetail::where([
                                'order_code' => $oldOrder->code,
                                'product_id' => $productId,
                            ])->update([
                                'is_export' => 0,
                                'export_user_id' => null,
                                'export_date' => null,
                                'order_code' => null
                            ]);
        
                            $inventory = Inventory::where([
                                'product_id' => $productId,
                                'warehouse_id' => $exportDetail->warehouse_id,
                            ])->first();

                            if($inventory){
                                $onHand = $inventory->on_hand;
                                $amountSold = $inventory->amount_sold;
                                $inventory->on_hand = $onHand + $exportDetail->qty;
                                $inventory->amount_sold = $amountSold - $exportDetail->qty;
                                $inventory->save();
                            }
                        }
                    }
                }

            DB::commit();
            return $this->responseJson(CODE_SUCCESS, $update);
        }
        else{
            DB::rollback();
            return $this->responseJson(CODE_ERROR, null, 'Xử lý thất bại!');
        }
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return $this->responseJson(CODE_ERROR, null, $th->getMessage());
        }
    }

    public function fix(){
        $data = Customer::all();

        foreach ($data as $key => $value) {
            if($value->saler_username){
                $user = User::where('username', $value->saler_username)->first();
                $value->saler_id = $user->id;
                $value->save();
            }
        }

        return 'Done';
    }
}
