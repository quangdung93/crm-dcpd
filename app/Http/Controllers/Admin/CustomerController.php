<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use Carbon\Carbon;
use App\Models\Task;
use App\Models\User;
use App\Models\Order;
use App\Models\Customer;
use App\Models\Province;
use App\Models\CustomerCare;
use Illuminate\Http\Request;
use App\Models\CustomerPoint;
use App\Services\UserService;
use App\Services\CustomerService;
use App\Http\Controllers\Controller;
use App\Models\Debt;
use App\Models\Receipt;

class CustomerController extends Controller
{
    protected $userService;
    protected $customerService;

    public function __construct(UserService $userService, CustomerService $customerService)
    {
        $this->userService = $userService;
        $this->customerService = $customerService;
    }

    public function index(){
        //Gán cứng quyền user binhnguyen
        $user = Auth::user();
        if($user->username == 'binhnguyen'){
            return abort(403);
        }

        return view('admin.customers.index', [
            'users' => $this->userService->getListUserByPermission()
        ]);
    }

    public function listAgency(){
        return view('admin.customers.agency', [
            'users' => $this->userService->getListUserByPermission()
        ]);
    }

    public function getDatatableAgency(){
        $query = Customer::query();

        //Lấy DSKH theo quyền user
        $userIds = $this->userService->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('saler_id', $userIds);
        }

        $query->where('customer_group', Customer::GROUP_AGENCY);

        $customers = $query->with('user')->orderBy('id', 'DESC')->select('*');

        return $this->customerService->renderDatatable($customers);
    }

    public function getDatatable(){
        $query = Customer::query();

        //Lấy DSKH theo quyền user
        $userIds = $this->userService->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('saler_id', $userIds);
        }

        $query->where('customer_group', Customer::GROUP_SINGLE);

        $customers = $query->with('user')->orderBy('id', 'DESC')->select('*');

        return $this->customerService->renderDatatable($customers);
    }

    public function getLists(Request $request){
        $query = Customer::query();

        if($request->name){
            $keyword = trim($request->name);
            $query->where(function ($q) use($keyword) {
                $q->where('name', 'like', '%'.$keyword.'%');
                $q->orWhere('phone', $keyword);
                $q->orWhere('customer_code', $keyword);
            });
        }

        //Lấy DSKH theo quyền user
        $userIds = $this->userService->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('saler_id', $userIds);
        }

        if($request->saler_id){
            $query->where('saler_id', $request->saler_id);
        }

        $query->where('customer_group', Customer::GROUP_SINGLE);

        if($request->birtday_month){
            $query->whereMonth('birthday', (int)$request->birtday_month);
        }

        if($request->customer_level){
            $query = $this->customerService->buildQueryFilterCustomerLevel($query, $request->customer_level);
        }

        if($request->price_from || $request->price_to){
            $query = $this->customerService->buildQueryFilterCustomerTotalOrderMoney($query, $request->price_from, $request->price_to);
        }

        if($request->sort_by){
            $query = $this->customerService->buildQuerySortCustomer($query, $request->sort_by);
        }

        $customers = $query->with('user')->select('*');

        return $this->customerService->renderDatatable($customers);
    }

    public function getListsAgency(Request $request){
        $query = Customer::query();

        if($request->name){
            $keyword = trim($request->name);
            $query->where(function ($q) use($keyword) {
                $q->where('name', 'like', '%'.$keyword.'%');
                $q->orWhere('phone', $keyword);
                $q->orWhere('customer_code', $keyword);
            });
        }

        //Lấy DSKH theo quyền user
        $userIds = $this->userService->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('saler_id', $userIds);
        }

        if($request->saler_id){
            $query->where('saler_id', $request->saler_id);
        }

        $query->where('customer_group', Customer::GROUP_AGENCY);

        if($request->birtday_month){
            $query->whereMonth('birthday', (int)$request->birtday_month);
        }

        if($request->customer_level){
            $query = $this->customerService->buildQueryFilterCustomerLevel($query, $request->customer_level);
        }

        if($request->price_from || $request->price_to){
            $query = $this->customerService->buildQueryFilterCustomerTotalOrderMoney($query, $request->price_from, $request->price_to);
        }

        $customers = $query->with('user')->select('*');

        return $this->customerService->renderDatatable($customers);
    }


    //Saler
    public function listSaler(){
        return view('admin.customers.saler', [
            'users' => $this->userService->getListUserByPermission()
        ]);
    }

    public function getDatatableSaler(){
        $query = Customer::query();

        //Lấy DSKH theo quyền user
        $userIds = $this->userService->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('saler_id', $userIds);
        }

        $query->where('customer_group', Customer::GROUP_SALE);

        $customers = $query->with('user')->orderBy('id', 'DESC')->select('*');

        return $this->customerService->renderDatatable($customers);
    }

    public function getListsSaler(Request $request){
        $query = Customer::query();

        if($request->name){
            $keyword = trim($request->name);
            $query->where(function ($q) use($keyword) {
                $q->where('name', 'like', '%'.$keyword.'%');
                $q->orWhere('phone', $keyword);
                $q->orWhere('customer_code', $keyword);
            });
        }

        //Lấy DSKH theo quyền user
        $userIds = $this->userService->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('saler_id', $userIds);
        }

        if($request->saler_id){
            $query->where('saler_id', $request->saler_id);
        }

        $query->where('customer_group', Customer::GROUP_SALE);

        if($request->birtday_month){
            $query->whereMonth('birthday', (int)$request->birtday_month);
        }

        if($request->customer_level){
            $query = $this->customerService->buildQueryFilterCustomerLevel($query, $request->customer_level);
        }

        if($request->price_from || $request->price_to){
            $query = $this->customerService->buildQueryFilterCustomerTotalOrderMoney($query, $request->price_from, $request->price_to);
        }

        $customers = $query->with('user')->select('*');

        return $this->customerService->renderDatatable($customers);
    }

    public function detail($id){
        //Gán cứng quyền user binhnguyen
        $user = Auth::user();
        if($user->username == 'binhnguyen'){
            return abort(403);
        }

        $customer = Customer::findOrFail($id);
        $orders = Order::with('user')->where('customer_id', $id)->orderBy('id', 'DESC')->get();
        $customerCares = Task::where('customer_id', $id)->get();
        $pointsHistory = CustomerPoint::where('customer_id', $id)->get();
        $receipts = Receipt::where('customer_id', $id)->orderBy('id', 'DESC')->get();
        $totalMoney = calTotalBuyCustomer($id);

        $debt = Debt::where('object_id', $id)->sum('debt');

        return view('admin.customers.detail')->with([
            'customer' => $customer,
            'orders' => $orders,
            'debt' => $debt,
            'customerCares' => $customerCares,
            'pointsHistory' => $pointsHistory,
            'receipts' => $receipts,
            'typeCustomer' => calCustomerLevel($totalMoney),
            'totalMoney' => $totalMoney,
            'users' => $this->userService->getListUserByPermission(),
            'customers' => Customer::all()
        ]);
    }

    public function create(){
        $provinces = Province::all();
        $users = User::active()->get();
        return view('admin.customers.add-edit')->with([
            'provinces' => $provinces,
            'users' => $users,
        ]);
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'customer_group' => 'required',
        ],[
            'name.required' => 'Bạn chưa nhập tên khách hàng',
            'customer_group.required' => 'Bạn chưa chọn loại Khách hàng',
        ]);

        $data = $request->except('token');
        $data['status'] = 1;
        $data['birthday'] = format_date($request->birthday, 'Y-m-d');
        $data['customer_code'] = $this->customerService->getCustomerCode();
        $data['saler_id'] = isset($request->saler_id) ? $request->saler_id : Auth::id();
        $data['created_by'] = Auth::id();

        $customer = Customer::create($data);

        $prefix = $this->customerService->getPrefix($customer->customer_group);

        if($customer){
            return redirect('admin/customers/'.$prefix)->with('success', 'Tạo thành công!');
        }
        else{
            return redirect('admin/customers/'.$prefix)->with('danger', 'Tạo thất bại!');
        }
    }

    public function edit($id){
        $customer = Customer::findOrFail($id);
        $users = User::active()->get();
        $provinces = Province::all();
        return view('admin.customers.add-edit')->with([
            'customer' => $customer,
            'provinces' => $provinces,
            'users' => $users,
        ]);
    }

    public function update(Request $request, $id){
        $request->validate([
            'name' => 'required',
            'customer_group' => 'required',
        ],[
            'name.required' => 'Bạn chưa nhập tên khách hàng',
            'customer_group.required' => 'Bạn chưa chọn loại Khách hàng',
        ]);

        $model = Customer::findOrFail($id);

        $data = $request->except('token');
        $data['birthday'] = format_date($request->birthday, 'Y-m-d');
        $data['status'] = isset($request->status) ? 1 : 0;

        $update = $model->update($data);

        if($update){
            return redirect('admin/customers/edit/' . $id)->with('success', 'Sửa thành công!');
        }
        else{
            return redirect('admin/customers/edit/'. $id)->with('danger', 'Sửa thất bại!');
        }
    }

    public function destroy($id)
    {
        $model = Customer::findOrFail($id);
        $customer = $model;
        $delete = $model->delete();

        $prefix = $this->customerService->getPrefix($customer->customer_group);

        if($delete){
            return redirect('admin/customers/'.$prefix)->with('success', 'Xóa thành công!');
        }
    }

    public function customerCare(Request $request){
        $request->validate([
            'task' => 'required',
            'customer_id' => 'required',
        ],[
            'customer_id.required' => 'Khách hàng không tồn tại',
            'task.required' => 'Bạn chưa chọn công việc',
        ]);

        $data = $request->except('token');
        $data['created_by'] = Auth::id();
        $customer = CustomerCare::create($data);

        if($customer){
            return redirect('admin/customers/detail/'.$request->customer_id)->with('success', 'Tạo thành công!');
        }
        else{
            return redirect('admin/customers/detail/'.$request->customer_id)->with('danger', 'Tạo thất bại!');
        }
    }

    public function calcPoint(){
        $result = Order::selectRaw("count(store_id) as quantity, count(customer_id) as count, customer_id,
                SUM(total_money) as total_money, 
                SUM(lack) as total_debt, 
                SUM(coupon) as total_discount, 
                SUM(total_quantity) as total_quantity")
                ->where('status', 1)
                ->where('admin_status', 1)
                ->groupBy('customer_id')
                ->get();

        $count = 0;
        foreach ($result as $value) {
            $customer = Customer::find($value['customer_id']);

            if($customer){
                $points = round($value['total_money'] / 100000, 0);
                $customer->points = $points;
                $customer->save();
                $count++;

                $dataCustomerPoints = [
                    'customer_id' => $value['customer_id'],
                    'order_id' => null,
                    'action' => 'add',
                    'points' => $points,
                    'note' => "Khởi tạo ($points) điểm!"
                ];
    
                CustomerPoint::create($dataCustomerPoints);
            }
        }

        echo 'Done:' . $count;
    }

    public function search(Request $request)
    {
        $key = $request->get('key');
        $keyword = trim($key);
        if (!empty($keyword)) {

            $query = Customer::query();
            $query->where(function ($q) use($keyword) {
                $q->where('name', 'like', '%'.$keyword.'%');
                $q->orWhere('phone', $keyword);
                $q->orWhere('customer_code', $keyword);
            });

            //Tìm KH theo quyền user
            $userIds = $this->userService->getUserIdsByPermission();
            if($userIds){
                $query->whereIn('saler_id', $userIds);
            }

            //Kiểm tra quyền Đại lý/Khách lẻ
            if(!isRoleAgency()){ //Nếu ko có quyền QL đại lý
                $query->where('customer_group', '!=', Customer::GROUP_AGENCY);
            }
    
            if(!isRoleCustomer()){ //Nếu ko có quyền QL KH lẻ
                $query->where('customer_group', '!=', Customer::GROUP_SINGLE);
            }

            if(!isRoleCustomerSale()){ //Nếu ko có quyển QL KH là nhân viên sale
                $query->where('customer_group', '!=', Customer::GROUP_SALE);
            }

            $customer = $query->limit(50)->get();

            if ($customer) {
                return $this->responseJson(CODE_SUCCESS, $customer);
            }
        }

        return $this->responseJson(CODE_ERROR, null, 'Không tìm thấy khách hàng');
    }

    public function getCustomerDetail($customerId){
        $customer = Customer::where('id', $customerId)->first();

        if ($customer) {
            return $this->responseJson(CODE_SUCCESS, $customer);
        }
        else{
            return $this->responseJson(CODE_ERROR, null, 'Không tìm thấy khách hàng');
        }
    }

    public function createApi(Request $request){
        $data = $request->except('token');
        $data['status'] = 1;
        $data['birthday'] = format_date($request->birthday, 'Y-m-d');
        $data['customer_code'] = $this->customerService->getCustomerCode();
        $data['created_by'] = Auth::id();

        $customer = Customer::create($data);

        if ($customer) {
            return $this->responseJson(CODE_SUCCESS, $customer);
        }
        else{
            return $this->responseJson(CODE_ERROR, null, 'Xử lý thất bại!');
        }
    }

    public function updateApi(Request $request){
        $customerId = $request->customer_id;
        $salerId = $request->saler_id;

        $order = Customer::where('id', $customerId)->first();

        $dataUpdate = [];

        if($request->saler_id){
            $dataUpdate['saler_id'] = Auth::id();
        }

        $update = $order->update($dataUpdate);

        if ($update) {
            return $this->responseJson(CODE_SUCCESS, $update);
        }
        else{
            return $this->responseJson(CODE_ERROR, null, 'Xử lý thất bại!');
        }
    }

    public function updatePointsApi(Request $request){
        try {
            $customerId = $request->customer_id;
            $points = $request->points;
            $note = $request->note;
            $note .= ' (Trừ điểm trực tiếp)';

            DB::beginTransaction();

            $customer = Customer::where('id', $customerId)->first();

            if($points <= 0){
                return $this->responseJson(CODE_ERROR, null, 'Số điểm cần trừ phải lớn hơn 0!');
            }

            if(!$customer){
                return $this->responseJson(CODE_ERROR, null, 'Không tìm thấy khách hàng!');
            }

            $customerPoints = $customer->points;
            $customer->points = $customerPoints - $points;
            $customer->save();

            $dataCustomerPoints = [
                'customer_id' => $customerId,
                'order_id' => null,
                'action' => CustomerPoint::REMOVE,
                'points' => $points,
                'note' => $note
            ];

            CustomerPoint::create($dataCustomerPoints);

            DB::commit();

            return $this->responseJson(CODE_SUCCESS, null, 'Trừ điểm thành công!');
        } catch (\Throwable $th) {
            DB::rollback();
            return $this->responseJson(CODE_ERROR, null, 'Xử lý thất bại!');
        }           
    }
}
