<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Brand;
use App\Models\Order;
use App\Models\Convert;
use App\Models\Product;
use App\Models\Customer;
use App\Models\Inventory;
use App\Scopes\StoreScope;
use App\Models\ConvertUser;
use App\Models\OrderDetail;
use App\Models\ConvertBrand;
use App\Models\ConvertOrder;
use Illuminate\Http\Request;
use App\Models\ConvertPoints;
use App\Models\CustomerPoint;
use App\Models\ProductExport;
use App\Models\ConvertProduct;
use App\Models\ConvertCustomer;
use App\Models\InventoryDetail;
use App\Models\ConvertInventory;
use App\Services\CustomerService;
use Illuminate\Support\Facades\DB;
use App\Models\ProductExportDetail;
use App\Models\ProductImportDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\ConvertInventoryDetail;
use App\Services\ProductExportService;

class ConvertDataController extends Controller
{

    public function convertCustomer(){
        // Remove all
        Customer::truncate();

        $customers = ConvertCustomer::all()->toArray();
        $count = 0;
        foreach ($customers as $key => $value) {
            $data = [
                'id' => $value['id'],
                'name' => $value['name'],
                'nickname' => $value['nickname'],
                'birthday_int' => $value['birthday'],
                'birthday' => $value['birthday'] ? $this->int_to_date($value['birthday']) : null,
                'address' => $value['add'],
                'phone' => $value['phone'],
                'id_pro' => $value['id_pro'],
                'note' => $value['note'],
                'joindate' => $value['joindate'] ? $this->int_to_date($value['joindate']) : null,
                'email' => $value['email'],
                'company' => $value['company'],
                'status' => $value['enable'],
                'gender' => $value['gender'],
                'note_genitive' => $value['note_genitive'],
                'note_skin' => $value['note_skin'],
                'image' => $value['picture'],
                'province_id' => $value['tinhthanh_id'],
                'district_id' => $value['quanhuyen_id'],
                'ward_id' => $value['phuongxa_id'],
                'customer_group' => $value['customer_group'],
                'created_by' => $value['user_id'],
                'saler_id' => $value['user_id'],
                'customer_code' => $value['customer_code'],
                'customer_diem' => $value['customer_diem'],
                'customer_total' => $value['customer_total'],
                'customer_birthday' => $value['customer_birthday'],
                'customer_date' => $value['customer_date'] ? $this->int_to_date($value['customer_date']) : null,
                'created_at' => $value['joindate'] ? $this->int_to_date($value['joindate']) : null,
            ];

            Customer::create($data);
            $count++;
        }

        echo $count;
    }

    public function convertCustomerPoints(){
        CustomerPoint::truncate();
        
        Customer::where('id', '>', 0)->update(['points' => 0]);

        $result = Order::selectRaw("count(store_id) as quantity, count(customer_id) as count, customer_id,
                SUM(total_money) as total_money, 
                SUM(lack) as total_debt, 
                SUM(coupon) as total_discount, 
                SUM(total_quantity) as total_quantity")
                ->where('status', 1)
                ->where('admin_status', 1)
                ->groupBy('customer_id')
                ->get();

        $count = 0;
        foreach ($result as $value) {
            $customer = Customer::find($value['customer_id']);

            if($customer){
                $points = round($value['total_money'] / 100000, 0);
                $customer->point_total = $points;

                $customerPoints = ConvertPoints::selectRaw("sum(points) as points")
                ->where('customer_id', $value['customer_id'])
                ->groupBy('customer_id')
                ->first();

                $pointsUsed = $customerPoints ? (int)$customerPoints->points : 0;

                $customer->points = $points > 0 ? $points - $pointsUsed : 0;
                $customer->point_used = $pointsUsed;
                $customer->save();
                $count++;

                $dataCustomerPoints = [
                    'customer_id' => $value['customer_id'],
                    'order_id' => null,
                    'action' => 'add',
                    'points' => $points,
                    'note' => "Khởi tạo ($points) điểm!",
                    'created_at' => format_date($value['creat_date'], 'Y-m-d h:i')
                ];
    
                CustomerPoint::create($dataCustomerPoints);
            }
        }

        echo $count;
    }

    function int_to_date($int){
		$time  = gmdate("Y-m-d", $int);
		return $time;
	}

    public function convertBrand(){

        Brand::truncate();

        $lists = ConvertBrand::all();

        $data = [];
        $count = 0;
        foreach ($lists as $key => $value) {
            $data[] = [
                'id' => $value['ID'],
                'name' => $value['prd_manuf_name'],
                'type' => $value['data_type_id'],
                'created_at' => $value['created'],
            ];

            $count++;
        }

        Brand::insert($data);

        echo $count;
    }

    public function convertUsers(){
        $lists = ConvertUser::where('iduser', '>', 137)->get();

        $count = 0;
        foreach ($lists as $key => $value) {
            $data = [
                'id' => $value['iduser'],
                'name' => $value['hoten'],
                'username' => $value['username'],
                'phone' => $value['dienthoai'],
                'gender' => $value['gioitinh'],
                'email' => $value['email'],
                'birthday' => $value['ngaysinh'] != '0000-00-00' ? $value['ngaysinh'] : null,
                'address' => $value['diachi'],
                'password' => Hash::make(123456),
                'status' => $value['enable'],
                'group' => $value['group'],
                'idgroup' => $value['idgroup'],
                'sadmin' => $value['sadmin'],
                'phanquyen' => $value['phanquyen'],
                'chuyenmuc' => $value['chuyenmuc'],
                'created_at' => $value['ngaydangky'],
            ];

            $count++;
            User::create($data);
        }

        echo $count;
    }

    public function convertProduct(){
        Product::truncate();

        $lists = ConvertProduct::all();

        $data = [];
        $count = 0;
        foreach ($lists as $key => $value) {
            $createdAt = $this->int_to_date($value['product_date']);
            if($createdAt == '1970-01-01'){
                $createdAt = '2022-01-01';
            }

            $data[] = [
                'id' => $value['id_product'],
                'name' => $value['name'],
                'price' => $value['price'] ?: 0,
                'code' => $value['pro_code'],
                'origin_price' => $value['origin_price'] ?: 0,
                'inventory_qty' => $value['sl_kho'] ?: 0,
                'showroom_qty' => $value['sl_showroom'] ?: 0,
                'price_sell' => $value['prd_sell_price'] ?: 0,
                'discount' => 0,
                'bonus' => $value['bonus'] ?: 0,
                'qty' => $value['soluong'] ?: 0,
                'created_at' => $value['created'],
                'brand_id' => $value['manufacture_id'],
                'origin_id' => $value['xuatxu_id'],
                'image_code' => $value['pro_code_image'],
                'created_at' => $createdAt,
            ];

            $count++;
        }

        Product::insert($data);

        echo $count;
    }

    public function convertInventory(){ //tbl_kho
        Inventory::truncate();
        ProductImportDetail::truncate();

        $lists = ConvertInventory::all();

        $dataImport = $dataInventoryDetail = [];
        $count = 0;
        foreach ($lists as $key => $value) {
            $dataImport[] = [
                'id' => $value['pro_nhap_id'],
                'import_id' => 1,
                'product_id' => $value['id_product'],
                'qty' => $value['soluong'],
                'price' => (int)$value['price'],
                'subtotal' => (int)$value['price'] * (int)$value['soluong'],
                'expired_date' => $this->int_to_date($value['ngayhethan']),
            ];
            // Showrrom :2 
            //Kho :1

            $warehouseId = $value['data_id'] == 1 ? 2 : 1;

            $inventoryData = [
                'product_id' => $value['id_product'],
                'warehouse_id' => $warehouseId,
                'price' => (int)$value['price'],
                'on_hand' => (int)$value['soluong'],
                'amount_import' => (int)$value['soluong'],
                'import_date' => $this->int_to_date($value['ngaynhap']),
                'expires_date' => $this->int_to_date($value['ngayhethan'])
            ];

            //Inventory
            $inventory = Inventory::where([
                'product_id' => $value['id_product'], 
                'warehouse_id' => $warehouseId
            ])->first();

            if($inventory){
                $inventory->update([
                    'on_hand' => (int)$inventory->on_hand + (int)$value['soluong'], 
                    'amount_import' => $inventory->amount_import + (int)$value['soluong'], 
                    'price' => (int)$value['price']
                ]);
            }
            else{
                Inventory::create($inventoryData);
            }

            $count++;
        }

        ProductImportDetail::insert($dataImport);

        echo $count;
    }

    //
    public function convertInventoryCode(){ // tbl_kho_code
        InventoryDetail::truncate();

        $lists = ConvertInventoryDetail::all();

        $count = 0;
        foreach ($lists as $key => $value) {
            $warehouseId = $value['data_id'] == 1 ? 2 : 1;
            $data = [
                'product_import_detail_id' => $value['pro_nhap_id'],
                'product_code' => $value['pro_code'],
                'product_id' => $value['pro_id'],
                'code_file' => $value['pro_code_file'],
                'warehouse_id' => $warehouseId,
                'price' => $value['pro_price'],
                'is_export' => $value['check_xuat_id'],
                'export_user_id' => $value['user_xuat_id'],
                'export_date' => $value['date_xuat_time'],
                'import_date' => $value['ngaynhap'] ? $this->int_to_date($value['ngaynhap']) : null,
                'expried_date' => $value['ngayhethan'] ? $this->int_to_date($value['ngayhethan']) : null,
                'order_code' => $value['order_code'] ? str_replace('PX', 'DH', $value['order_code']) : null,
            ];

            InventoryDetail::create($data);

            $count++;
        }

        echo $count;
    }

    //Đơn hàng
    public function convertOrders(){ //tbl_order
        $offset = 0; //5000
        $limit = 5000; //15000

        // Remove all
        if($offset == 0){
            Order::truncate();
            OrderDetail::truncate();
        } 

        $lists = ConvertOrder::offset($offset)->limit($limit)->get();

        $count = 0;
        foreach ($lists as $key => $value) {
            $data = [
                'id' => $value['ID'],
                'code' => str_replace('PX', 'DH', $value['output_code']),
                'customer_id' => $value['customer_id'],
                'store_id' => $value['store_id'],
                'sell_date' => $value['sell_date'],
                'notes' => $value['notes'],
                'payment_method' => $value['payment_method'],
                'total_price' => $value['total_price'],
                'total_origin_price' => $value['total_origin_price'],
                'coupon' => $value['coupon'],
                'customer_pay' => $value['customer_pay'],
                'total_money' => $value['total_money'],
                'total_quantity' => $value['total_quantity'],
                'lack' => $value['lack'],
                'status' => $value['order_status'],
                'saler_id' => $value['sale_id'],
                'created_by' => $value['user_init'],
                'customer_date' => $value['customer_date'] ? $this->int_to_date($value['customer_date']) : null,
                'admin_status' => $value['admin_status'],
                'canreturn' => $value['canreturn'],
                'source_id' => $value['nguon_id'],
                'check_status' => $value['check_status'],
                'check_tichdiem' => $value['check_tichdiem'],
                'shop_id' => $value['shop_id'],
                'detail_order' => $value['detail_order'],
                'created_at' => $value['created'],
                'updated_at' => ($value['updated'] == '0000-00-00 00:00:00') ? $value['created'] : $value['updated'],
                'is_deleted' => $value['deleted'],
            ];

            $orderDetail = json_decode($value['detail_order'], true);
            $order = Order::create($data);

            if($order && $orderDetail){
                $dataDetail = [];
                foreach ($orderDetail as $item) {
                    $dataDetail[] = [
                        'order_id' => $order->id,
                        'product_id' => $item['id'],
                        'qty' => (int)$item['quantity'] ?? 0,
                        'price' => (int)$item['price'] ?? 0,
                        'discount' => $item['discount'] ?? 0,
                        'subtotal' => ($item['quantity'] && $item['quantity']) ? (int)$item['quantity'] * (int)$item['price'] : 0,
                        'inventory_type' => $item['loaikho'] ?? 0,
                    ];
                }

                OrderDetail::insert($dataDetail);
            }

            $count++;
        }

        echo $count;
    }

    //Tạo phiếu xuất
    public function createExportProduct(){
        try {
            $exportService = new ProductExportService();

            $inventoryDetails = InventoryDetail::whereNotNull('order_code')->get();
            $orders = $inventoryDetails->groupBy('order_code');
            $count = 0;
            foreach ($orders as $key => $value) {
                $orderCode = $key;
                $exportDate = $value->first()->export_date;
                $exportUserId = $value->first()->export_user_id;
                $order = Order::where('code', $orderCode)->with('detail')->first();

                $orderDetail = $order->detail;

                $dataExport = [
                    'code' => $exportService->getExportCode(),
                    'order_id' => $order->id,
                    'export_date' => $exportDate,
                    'created_by' => $exportUserId,
                    'notes' => 'Xuất hàng khởi tạo'
                ];

                $export = ProductExport::create($dataExport);

                $value = $value->groupBy('product_id');
                $products = [];

                foreach ($value as $key => $item) {
                    $products[] = [
                        'product_id' => $key,
                        'qty' => $item->count(),
                    ];

                    $qty = $item->count();
                    $productId = $key;
                    $warehouseId = $item->first()->warehouse_id;
                    
                    $order_detail_id = null;
                    foreach ($orderDetail as $detail) {
                        if($detail->pivot->product_id == $productId){
                            $order_detail_id = $detail->pivot->id;
                        }
                    }

                    $dataExportDetail = [
                        'export_id' => $export->id,
                        'order_detail_id' => $order_detail_id,
                        'product_id' => $key,
                        'qty' => $qty,
                        'warehouse_id' => $warehouseId
                    ];

                    DB::beginTransaction();

                    //Tạo export detail
                    $exportDetail = ProductExportDetail::create($dataExportDetail);

                    // Update inventory detail
                    foreach ($item as $key => $sub) {
                        $sub->update(['product_export_detail_id' => $exportDetail->id]);
                    }

                    //Update inventory

                    $inventory = Inventory::where([
                        'product_id' => $productId,
                        'warehouse_id' => $warehouseId,
                    ])->first();

                    if($inventory){
                        $amountSold = $inventory->amount_sold;
                        $onHand = $inventory->on_hand;
                        $inventory->on_hand = $onHand - $qty;
                        $inventory->amount_sold = $amountSold + $qty;
                        $inventory->save();
                    }

                    $count++;

                    DB::commit();
                }
            }
            return $count;

        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception($th->getMessage(), 1);
        }
    }


    public function exchangeCustomerBySale() {
        $data = [
            [
                'sale_id' => 151,
                'customer_codes' => ["CX001742","CX001837","CX001840","CX001846","CX001890","CX001902","CX001903","CX001912","CX001914","CX001924","CX001943","CX001968","CX001996","CX001997","CX002001","CX002009","CX002011","CX002013","CX002014","CX002023","CX002028","CX002030","CX002032","CX002044","CX002045","CX002049","CX002055","CX002058","CX002060","CX002068","CX002076","CX002077","CX002082","CX002083","CX002084","CX002086","CX002089","CX002092","CX002096","CX002097","CX002107","CX002110","CX002113","CX002118","CX002120","CX002123","CX002125","CX002126","CX002130","CX002136","CX002139","CX002140","CX002144","CX002145","CX002146"]
            ],
            // [
            //     'sale_id' => 150,
            //     'customer_codes' => ["CX0031","CX0071","CX00140","CX00141","CX00160","CX00163","CX00191","CX00319","CX00576","CX00643","CX00669","CX00717","CX00733","CX00768","CX00784","CX00789","CX00846","CX00913","CX00957","CX001085","CX001086","CX001094","CX001095","CX001104","CX001108","CX001111","CX001115","CX001120","CX001133","CX001155"]
            // ]
        ];

        $count = 0;

        foreach ($data as $item) {
            $update = Customer::whereIn('customer_code', array_values(array_filter($item['customer_codes'])))
                            ->update(['saler_id' => $item['sale_id']]);
            $count += $update;
        }

        echo $count;
    }

    public function exchangeCustomer(){
            $customers = Customer::where('saler_id', 135)
                ->where('customer_group', 0)
                ->update(['saler_id' => 144]);
            echo 'Done';
    }

    public function convertOrderInventoryStatus(){
        $orders = Order::with('detail')->where('id', '>', 10000)->orderBy('id', 'DESC')->get();

        foreach($orders as $order){
            $array = $order->toArray();
            $ids = data_get($array, 'detail.*.pivot.id'); //Số SP trong đơn hàng
            $exportDetail = ProductExportDetail::whereIn('order_detail_id', $ids)->get()->toArray();

            $count = count($exportDetail); //Số sản phẩm đã xuất

            if($count >= count($ids)){
                $order->inventory_status = 1;

                if($order->status == 0){
                    $order->status = 5;
                }

                $order->save();
            }
        }

        echo 'Done';
    }

    public function fixExport(){
        $exports = ProductExport::withoutGlobalScope(StoreScope::class)->where('id', '>', 3389)->get();
        $count = 0;

        foreach($exports as $export){
            $orderId = $export->order_id;
            $order = Order::withoutGlobalScope(StoreScope::class)->where('id', $orderId)->first();

            if($order){
                $count++;
                $export->store_id = $order->store_id;
                $export->save();
            }
        }

        echo 'Done: '. $count;
    }

    public function convertCustomerOrders(){
        $customerService = new CustomerService();
        $count = $customerService->cronJobCalculateOrder();
        echo $count;
    }

}
