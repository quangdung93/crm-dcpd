<?php

namespace App\Http\Controllers\Admin;

use App\Models\Supplier;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Supplier::all();
        return view('admin.suppliers.index')->with('lists', $lists);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.suppliers.add-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ],[
            'name.required' => 'Bạn chưa nhập tên nhà phân phối',
        ]);

        $data = $request->all();
        $data['status'] = isset($request->status) ? 1 : 0;

        $brand = Supplier::create($data);

        if($brand){
            return redirect('admin/suppliers')->with('success', 'Tạo thành công!');
        }
        else{
            return redirect('admin/suppliers')->with('danger', 'Tạo thất bại!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $object = Supplier::findOrFail($id);
        return view('admin.suppliers.add-edit')->with([
            'object' => $object,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ],[
            'name.required' => 'Bạn chưa nhập tên nhà phân phối',
        ]);

        $object = Supplier::findOrFail($id);

        if(!$object){
            return abort(404);
        }

        $data = $request->all();
        $data['status'] = isset($request->status) ? 1 : 0;

        $update = $object->update($data);

        if($update){
            return redirect('admin/suppliers/edit/' . $id)->with('success', 'Sửa thành công!');
        }
        else{
            return redirect('admin/suppliers/edit/'. $id)->with('danger', 'Sửa thất bại!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = Supplier::findOrFail($id);

        if(!$object){
            return abort(404);
        }

        $delete = $object->delete();

        if($delete){
            return redirect('admin/suppliers')->with('success', 'Xóa thành công!');
        }
    }
}
