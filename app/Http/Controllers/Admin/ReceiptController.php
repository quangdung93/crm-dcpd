<?php

namespace App\Http\Controllers\Admin;

use N2W;
use PDF;
use Auth;
use Carbon\Carbon;
use App\Models\Debt;
use App\Models\User;
use App\Models\Brand;
use App\Models\Order;
use App\Models\Store;
use App\Models\Receipt;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Supplier;
use App\Models\Inventory;
use App\Models\Warehouse;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Models\ProductExport;
use App\Services\UserService;
use App\Services\OrderService;
use App\Models\InventoryDetail;
use App\Services\ReceiptService;
use Yajra\DataTables\DataTables;
use App\Services\CustomerService;
use Illuminate\Support\Facades\DB;
use App\Models\ProductExportDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Validation\ValidationException;

class ReceiptController extends Controller
{
    protected $receiptService;
    protected $userService;
    protected $customerService;

    public function __construct(
        ReceiptService $receiptService,
        UserService $userService,
        CustomerService $customerService
    ){
        $this->receiptService = $receiptService;
        $this->userService = $userService;
        $this->customerService = $customerService;
    }

    public function index(){
        return view('admin.receipt.index');
    }

    public function getDatatable(){
        $query = Receipt::select('*');

        //Lấy DSKH theo quyền user
        $userIds = (new UserService)->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('created_by', $userIds);
        }
        
        return $this->receiptService->renderDatatable($query);
    }

    public function create(Request $request){
        if($request->debt_id){
            $debt = Debt::where('id', $request->debt_id)->first();
        }

        $users = User::active()->get();

        return view('admin.receipts.add')->with([
            'debt' => $debt,
            'customer' => $customer ?? null,
            'users' => $users,
        ]);
    }

    public function store(Request $request, $debt_id){
        $request->validate([
            'value' => 'required|min:1',
        ],[
            'value.required' => '(*) Bạn chưa nhập số tiền thu',
            'value.min' => '(*) Số tiền thu phải lớn hơn 0',
        ]);

        DB::beginTransaction();

        $debt = Debt::where('id', $debt_id)->first();

        $data = $request->except('_token', 'lack_total', 'lack_remain', 'submit');
        $data['code'] = $this->receiptService->getCode();
        $data['customer_id'] = $debt->object_id;
        $data['order_id'] = $debt->document_id;
        $data['created_by'] = Auth::id();
        $data['value'] = $request->value ? str_replace(',', '', $request->value) : 0;

        $receipt = Receipt::create($data);

        if($receipt){
            //Cập nhật công nợ KH
            $remain = str_replace(',', '', $request->lack_remain);
            $pay = $debt->pay;
            $debt->pay = $pay + $data['value'];
            $debt->debt = $remain;

            if($remain == 0){
                $debt->status = Debt::PAID;
            }

            $debt->save();
            
            DB::commit();

            // if($request->submit == 'print'){
            //     return redirect(route('order.print', ['id' => $order->id]));
            // }

            return redirect('admin/debts')->with('success', 'Tạo thành công!');
        }
        else{
            DB::rollback();
            return redirect('admin/receipts/create')->with('danger', 'Tạo thất bại!');
        }
    }

    public function print($id){
        $receipt = Receipt::where('id', $id)->first();

        if(!$receipt){
            return abort(404);
        }

        $customer = Customer::where('id', $receipt->customer_id)->first(); 

        $moneyToVietnamese = N2W::toCurrency($receipt->value);

        $store = Store::where('id', $receipt->store_id)->first();

        $pdf = PDF::loadView('admin.receipt.print', [
            'receipt' => $receipt,
            'store' => $store,
            'customer' => $customer,
            'moneyToVietnamese' => $moneyToVietnamese
        ])->setPaper('a4', 'landscape');

        $pdf->setOptions(['isRemoteEnabled' => true]);

        return $pdf->stream();
    }

}
