<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\Store;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $userModel;

    public function __construct(User $userModel){
        $this->userModel = $userModel;
    }
    //
    public function index(){
        $users = $this->userModel->getAllUser();
        return view('admin.users.index')->with(compact('users'));
    }

    public function create(){
        $roles = Role::all();
        $stores = Store::active()->get();
        $managers = $this->userModel->getAllUser();

        return view('admin.users.add-edit', [
            'roles' => $roles,
            'managers' => $managers,
            'stores' => $stores,
        ]);
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'username' => 'required|min:3|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'stores' => 'required',
            'role' => 'required'
        ],[
            'name.required' => 'Bạn chưa nhập tên người dùng',
            'username.required' => 'Bạn chưa nhập username',
            'username.min' => 'Username phải nhiều hơn 3 ký tự',
            'username.unique' => 'Username đã tồn tại',
            'email.required' => 'Bạn chưa nhập email',
            'email.email' => 'Định dạng email không đúng',
            'email.unique' => 'Email đã tồn tại',
            'password.required' => 'Bạn chưa nhập mật khẩu',
            'stores.required' => 'Bạn chưa chọn chi nhánh',
            'role.required' => 'Bạn chưa chọn quyền'
        ]);

        //Avatar image
        if($request->hasFile('input_file')){
            $avatarPath = $this->uploadImage('users', $request->file('input_file'));
        }

        $requestStores = $request->stores;

        $data = [
            'name' => $request->name,
            'username' => Str::slug($request->username),
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'status' => isset($request->status) ? 1 : 0,
            'phone' => $request->phone,
            'address' => $request->address,
            'gender' => (int)$request->gender,
            'stores' => $requestStores ? implode(',', $requestStores) : null,
            'birthday' => format_date(str_replace('/', '-', $request->birthday), 'Y-m-d'),
            'avatar' => $avatarPath ?? '/admin/images/user-default.png'
        ];

        //Set store mặc định
        if($requestStores){
            $data['store_active'] = $requestStores[0];
        }

        $user = User::create($data);

        //Assign Role
        if($user && $request->role){
            $user->assignRole($request->role);

            return redirect('admin/users');
        }
        else{
            return redirect('admin/users')->with('danger', 'Tạo thất bại!');
        }
    }

    public function edit($id){
        $user = $this->userModel->findUser($id);

        if(!$user){
            return abort(404);
        }

        if(!isAdmin() && Auth::id() != $id){
            return abort(403);
        }

        $roles = Role::all();
        $stores = Store::active()->get();
        $managers = $this->userModel->getAllUser();

        return view('admin.users.add-edit')->with([
            'roles' => $roles,
            'managers' => $managers,
            'stores' => $stores,
            'user' => $user
        ]);
    }

    public function update(Request $request, $id){
        $user = $this->userModel->findUser($id);

        if(!$user){
            return abort('404');
        }

        if(!isAdmin() && Auth::id() != $id){
            return abort(403);
        }

        $request->validate([
            'name' => 'required',
            'username' => 'required|min:3|unique:users,username,'.$user->id,
            'email' => 'required|email|unique:users,email,'.$user->id,
            'role' => 'required',
            'stores' => 'required',
        ],[
            'name.required' => 'Bạn chưa nhập tên người dùng',
            'username.required' => 'Bạn chưa nhập username',
            'username.min' => 'Username phải nhiều hơn 3 ký tự',
            'username.unique' => 'Username đã tồn tại',
            'email.required' => 'Bạn chưa nhập email',
            'email.email' => 'Định dạng email không đúng',
            'email.unique' => 'Email đã tồn tại',
            'stores.required' => 'Bạn chưa chọn chi nhánh',
            'role.required' => 'Bạn chưa chọn quyền'
        ]);

        //Avatar image
        if($request->hasFile('input_file')){
            $avatarPath = $this->uploadImage('users', $request->file('input_file'));
            if($avatarPath){
                $this->deleteImage($user->image);
            }
        }else{
            $avatarPath = $user->avatar;
        }

        $requestStores = $request->stores;

        $data = [
            'name' => $request->name,
            'username' => Str::slug($request->username),
            'email' => $request->email,
            'status' => isset($request->status) ? 1 : 0,
            'phone' => $request->phone,
            'address' => $request->address,
            'gender' => (int)$request->gender,
            'managers' => $request->managers ? implode(',', $request->managers) : null,
            'stores' => $requestStores ? implode(',', $requestStores) : null,
            'birthday' => format_date(str_replace('/', '-', $request->birthday), 'Y-m-d'),
            'avatar' => $avatarPath ?? null
        ];

        //Set store mặc định trong trường hợp thêm mới store
        if(!$user->store_active){
            $data['store_active'] = $requestStores[0];
        }

        //Set store mặc định trong trường hợp stores có thay đổi
        if($user->store_active && !in_array($user->store_active, $requestStores)){
            $data['store_active'] = $requestStores[0];
        }

        //Change password
        if($request->password){
            $data['password'] = Hash::make($request->password);
        }

        $user->update($data);

        //Assign Role
        if($request->role){
            $user->syncRoles($request->role);
        }

        return redirect('admin/users/edit/'.$id)->with('success','Cập nhật thành công!');
    }

    public function destroy($id){
        $user = $this->userModel->findUser($id);

        if(!$user){
            return abort('404');
        }
        $delete = $user->delete();

        if($delete){
            return redirect('admin/users')->with('success', 'Xóa thành công!');
        }
    }
}
