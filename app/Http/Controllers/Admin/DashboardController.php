<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ChartService;

class DashboardController extends Controller
{
    public function __construct()
    {

    }

    public function index(){
        return view('admin.dashbroad');
    }
}
