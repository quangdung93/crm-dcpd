<?php

namespace App\Http\Controllers\Admin;

use Log;
use Auth;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Store;
use App\Models\Inventory;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use App\Models\ProductExport;
use Yajra\DataTables\DataTables;
use App\Services\InventoryService;
use Illuminate\Support\Facades\DB;
use App\Models\ProductExportDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Services\ProductExportService;

class ExportController extends Controller
{

    protected $productExportService;
    protected $inventoryService;

    public function __construct(
        ProductExportService $productExportService, 
        InventoryService $inventoryService
    ){
        $this->productExportService = $productExportService;
        $this->inventoryService = $inventoryService;
    }

    public function index(){
        return view('admin.exports.index');
    }

    public function getDatatable(){
        $exports = ProductExport::with('user', 'warehouse', 'order')->orderBy('id', 'DESC')->select('*');
        return $this->productExportService->renderDatatable($exports);
    }

    public function detail($id){
        $order = ProductExport::findOrFail($id)->load('detail');
        return view('admin.exports.detail')->with([
            'order' => $order
        ]);
    }

    public function create(){
        $orders = Order::where('inventory_status', 0)->get();
        $warehouses = Warehouse::all();
        return view('admin.exports.add')->with([
            'orders' => $orders,
            'warehouses' => $warehouses
        ]);
    }

    public function store(Request $request){
        try {
            $validator = \Validator::make($request->all(), [
                'inventory_ids' => 'required',
            ],[
                'inventory_ids.required' => 'Bạn chưa chọn kho xuất',
            ]);
    
            if($validator->fails()) {
                return $this->responseJson(CODE_ERROR, null, $validator->errors()->first());
            }
    
            $inventoryIds = $request->inventory_ids;
    
            if(array_sum(array_values($inventoryIds)) == 0){
                return $this->responseJson(CODE_ERROR, null, 'Bạn chưa chọn kho xuất');
            }
    
            DB::beginTransaction();
    
            $orderId = $request->order_id;
            $order = Order::where('id', $orderId)->with('detail')->first();
    
            $data = $request->except('_token', 'datatable_length', 'inventory_ids');
            $data['code'] = $this->productExportService->getExportCode();
            $data['created_by'] = Auth::id();
            $data['export_date'] = format_date(Carbon::now(), 'Y-m-d');

            $user = Auth::user();
            $activeStoreId = $user->store_active;

            $data['store_id'] = $activeStoreId ? (int)$activeStoreId : Store::getDefaultStore();
    
            $create = ProductExport::create($data);
    
            if($create){
                $products = $order->detail;

                foreach($products as $product){
    
                    if(!isset($inventoryIds[$product->id]) || (int)$inventoryIds[$product->id] == 0){
                        continue;
                    }
    
                    $warehouseId = (int)$inventoryIds[$product->id];
    
                    $exportDetail = [
                        'export_id' => $create->id,
                        'warehouse_id' => $warehouseId,
                        'product_id' => $product['id'],
                        'qty' => (int)$product['pivot']['qty'],
                        'price' => (int)str_replace(',', '', $product['pivot']['price']),
                        'subtotal' => (int)str_replace(',', '', $product['pivot']['price']) * (int)$product['pivot']['qty']
                    ];
    
                    $exportDetail['order_detail_id'] = $product['pivot']['id'];
                    $createProductExportDetail = ProductExportDetail::create($exportDetail);
    
                    //Export Inventory
                    $checkExport = $this->inventoryService->exportInventory($product, $warehouseId, $order->code, $createProductExportDetail->id);
                }
    
                DB::commit();

                //Nếu đã xuất hết đơn hàng thì cập nhật trạng thái đã xuất hàng
                $exports = ProductExport::where('order_id', $orderId)->with('detail')->get();
                if($exports){
                    $exports = $exports->toArray();
                    $productIds = data_get($exports, '*.detail.*.pivot.product_id'); //Tổng sản phẩm đã xuất kho
                    $countOrder = $products->count(); //$countOrder: tổng sản phẩm trong đơn hàng
                    if(count($productIds) >= $countOrder){
                        $order->status = Order::EXPORTED;
                        $order->inventory_status = Order::INVENTORY_EXPORT;
                        $order->save();
                    }
                }
    
                return $this->responseJson(CODE_SUCCESS, $create, 'Xuất kho thành công!');
            }
            else{
                DB::rollback();
                return $this->responseJson(CODE_ERROR, null, 'Xuất kho thất bại!');
            }
        } catch (\Throwable $th) {
            DB::rollback();
            return $this->responseJson(CODE_ERROR, null, $th->getMessage());
        }
    }

    public function detailExport(Request $request, $id){
        try {
            $export = ProductExport::where('id', $id)->with('detail', 'order')->first();
            $result = $this->productExportService->renderExportDetail($export, $request->type);
            return $this->responseJson(CODE_SUCCESS, $result);
        } catch (\Throwable $th) {
            return $this->responseJson(CODE_ERROR, null, $th->getMessage());
        }
    }

    public function destroy($id){
        $order = ProductExport::findOrFail($id);
        $delete = $order->delete();

        if($delete){
            return redirect('admin/exports')->with('success', 'Xóa thành công!');
        }
    }

}
