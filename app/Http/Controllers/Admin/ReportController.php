<?php

namespace App\Http\Controllers\Admin;

use N2W;
use PDF;
use Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Brand;
use App\Models\Order;
use App\Models\Customer;
use App\Models\Supplier;
use App\Models\Warehouse;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Models\ProductImport;
use App\Services\UserService;
use App\Services\OrderService;
use App\Services\ReportService;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Models\ProductImportDetail;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Services\ProductExportService;
use App\Services\ProductImportService;

class ReportController extends Controller
{
    protected $orderService;
    protected $reportService;
    protected $importService;
    protected $exportService;
    protected $userService;

    public function __construct(
        OrderService $orderService,
        ReportService $reportService,
        ProductImportService $importService,
        ProductExportService $exportService,
        UserService $userService
    ){
        $this->orderService = $orderService;
        $this->reportService = $reportService;
        $this->importService = $importService;
        $this->exportService = $exportService;
        $this->userService = $userService;
    }

        
    /**
     * Báo cáo doanh số
     *
     * @return void
     */
    public function revenue(){        
        return view('admin.reports.revenue',[
            'users' => $this->userService->getListUserByPermission(),
        ]);
    }
    
    /**
     * Báo cáo doanh số đại lý
     *
     * @return void
     */
    public function revenueAgency(){        
        $queryAgency = Customer::where('customer_group', Customer::GROUP_AGENCY);

        //Lấy DSKH theo quyền user
        $userIds = $this->userService->getUserIdsByPermission();
        if($userIds){
            $queryAgency->whereIn('saler_id', $userIds);
        }

        $agency = $queryAgency->get();


        return view('admin.reports.revenue-agency',[
            'users' => $this->userService->getListUserByPermission(),
            'agency' => $agency,
        ]);
    }
    
    /**
     * Báo cáo doanh số sản phẩm
     *
     * @return void
     */
    public function revenueProduct(){
        $products = Product::active()->get();
        $brands = Brand::where('type', 0)->active()->get();
        return view('admin.reports.revenue-product',[
            'brands' => $brands,
            'products' => $products
        ]);
    }
    
    /**
     * Báo cáo doanh số nhập hàng
     *
     * @return void
     */
    public function import(){
        $suppliers = Supplier::active()->get();
        return view('admin.reports.import',[
            'users' => $this->userService->getListUserByPermission(),
            'suppliers' => $suppliers,
        ]);
    }
    
    /**
     * Báo cáo doanh số xuất hàng
     *
     * @return void
     */
    public function export(){

        $queryCustomers = Customer::where('customer_group', Customer::GROUP_SINGLE);
        $queryAgency = Customer::where('customer_group', Customer::GROUP_AGENCY);
        $queryCustomerSale = Customer::where('customer_group', Customer::GROUP_SALE);

        //Lấy DSKH theo quyền user
        $userIds = $this->userService->getUserIdsByPermission();
        if($userIds){
            $queryCustomers->whereIn('saler_id', $userIds);
            $queryAgency->whereIn('saler_id', $userIds);
            $queryCustomerSale->whereIn('saler_id', $userIds);
        }

        $customers = $queryCustomers->get();
        $agency = $queryAgency->get();
        $customerSale = $queryCustomerSale->get();


        return view('admin.reports.export',[
            'users' => $this->userService->getListUserByPermission(),
            'customers' => $customers,
            'agency' => $agency,
            'customerSale' => $customerSale,
        ]);
    }

    //DS đơn hàng
    public function getFilterRevenue(Request $request){
        $query = $this->reportService->buildQueryRevenue($request);

        if($request->customer_new == 1){ //Lọc ra KH mới
            $orders = $query->get();
            $filterOrders = [];
            if($orders){
                $newCustomerIds = $this->orderService->getNewCustomerIds();
                if($newCustomerIds){
                    $customerIds = [];
                    foreach ($orders as $order) {
                        $customerIds[] = (int)$order['customer_id'];
                        if(in_array((int)$order['customer_id'], $newCustomerIds)){
                            $filterOrders[] = $order;
                        }
                    }
                }
            }
        }
        else{
            $filterOrders = $query->get();

            //Gán cứng quyền user binhnguyen
            $user = Auth::user();
            if($user->username == 'binhnguyen'){
                $filterOrders = [];
            }
        }

        return $this->orderService->renderDatatableRevenue($filterOrders);
    }

    // Thống kê tổng đơn hàng
    public function getFilterRevenueTotal(Request $request){
        $revenue = $this->reportService->buildQueryRevenue($request);
        $orders = $revenue->get()->toArray();

        //
        $totalMoney = 0;
        $totalDebt = 0;
        $countTotal = 0;
        $totalDiscount = 0;
        $totalQuantity = 0;
        $totalMoneyShipping = 0;
        $totalMoneyNewCustomer = 0;
        $totalShippingNewCustomer = 0;

        if($orders){
            $newCustomerIds = $this->orderService->getNewCustomerIds();
            foreach ($orders as $order) {
                if($request->revenue_type == ReportService::REVENUE_SHIPPING 
                && $newCustomerIds 
                && in_array((int)$order['customer_id'], $newCustomerIds)){  
                    if($order['status'] == Order::FINISH){
                        $totalMoneyNewCustomer += $order['total_money'];
                    }
                    else if(in_array((int)$order['status'], [Order::WAITING, Order::EXPORTED, Order::SHIPPING])){
                        $totalShippingNewCustomer += $order['total_money'];
                    }
                }

                //Khách hàng mới
                if($request->customer_new == 1
                && $newCustomerIds 
                && !in_array((int)$order['customer_id'], $newCustomerIds)){
                    continue;
                }
                
                $countTotal++;
                $totalQuantity += $order['total_quantity'];

                if($order['status'] == Order::FINISH){
                    $totalMoney += $order['total_money'];
                    $totalDebt += $order['lack'];
                    $totalDiscount += $order['coupon'];
                }
                else if(in_array((int)$order['status'], [Order::WAITING, Order::EXPORTED, Order::SHIPPING])){ //Chờ xác nhận, đã xuất hàng, đang vận chuyển
                    $totalMoneyShipping += $order['total_money'];
                }
            }
        }

        if($request->customer_new != 1){
            //Gán cứng quyền user binhnguyen
            $user = Auth::user();
            if($user->username == 'binhnguyen'){
                $totalMoney = 0;
            }
        }

        $html = view('admin.reports.revenue-analytic', [
            'revenue_type' => $request->revenue_type,
            'totalMoneyNewCustomer' => $totalMoneyNewCustomer,
            'totalMoney' => $totalMoney,
            'totalShippingNewCustomer' => $totalShippingNewCustomer,
            'totalMoneyShipping' => $totalMoneyShipping,
            'countTotal' => $countTotal,
            'totalQuantity' => $totalQuantity,
            'totalDiscount' => $totalDiscount,
        ])->render();

        return $this->responseJson(CODE_SUCCESS, $html);
    }

    public function getFilterRevenueProduct(Request $request){

        $query = $this->reportService->buildQueryRevenueProduct($request);

        return $this->reportService->renderDatatableRevenueProduct($query);
    }

    public function getFilterRevenueTotalProduct(Request $request){
        $revenue = $this->reportService->buildQueryRevenueProduct($request);
        $orders = $revenue->get()->toArray();

        //
        $totalMoney = 0;
        if($orders){
            foreach ($orders as $order) {
                $totalMoney += $order['total_money'];
            }
        }

        $html = view('admin.reports.revenue-product-analytic', [
            'totalMoney' => $totalMoney,
        ])->render();

        return $this->responseJson(CODE_SUCCESS, $html);
    }

    public function getFilterImport(Request $request){
        $import = $this->reportService->buildQueryImport($request);
        return $this->importService->renderDatatableReport($import);
    }

    public function getFilterImportAnalytic(Request $request){
        $import = $this->reportService->buildQueryImport($request);

        $imports = $import->get();

        if($imports){
            $imports = $imports->toArray();
            $totalProduct = 0;
            $totalPrice = 0;
            foreach ($imports as $key => $value) {
                $totalProduct += $value['qty'];
                $totalPrice += $value['price'];
            }
        }

        $html = view('admin.reports.import-analytic', [
            'totalProduct' => $totalProduct,
            'totalPrice' => $totalPrice,
        ])->render();

        return $this->responseJson(CODE_SUCCESS, $html);
    }

    public function getFilterExport(Request $request){
        $export = $this->reportService->buildQueryExport($request);
        return $this->exportService->renderDatatableReport($export);
    }

    public function getFilterExportAnalytic(Request $request){
        $export = $this->reportService->buildQueryExport($request);

        $export = $export->get();

        if($export){
            $exports = $export->toArray();
            $totalProduct = 0;
            $totalPrice = 0;
            foreach ($exports as $key => $value) {
                $totalProduct += $value['qty'];
                $totalPrice += $value['price'];
            }
        }

        $html = view('admin.reports.export-analytic', [
            'totalProduct' => $totalProduct,
            'totalPrice' => $totalPrice,
        ])->render();

        return $this->responseJson(CODE_SUCCESS, $html);
    }
}
