<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Order;
use App\Models\Customer;
use App\Models\Supplier;
use App\Models\Inventory;
use App\Models\Warehouse;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Models\ProductImport;
use App\Models\InventoryDetail;
use Yajra\DataTables\DataTables;
use App\Services\InventoryService;
use Illuminate\Support\Facades\DB;
use App\Models\ProductImportDetail;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Services\ProductImportService;
use Illuminate\Support\Facades\Storage;

class ImportController extends Controller
{
    protected $productImportService;
    protected $inventoryService;

    public function __construct(
        ProductImportService $productImportService, 
        InventoryService $inventoryService
    ){
        $this->productImportService = $productImportService;
        $this->inventoryService = $inventoryService;
    }

    public function index(){
        return view('admin.imports.index');
    }

    public function getDatatable(){
        $imports = ProductImport::with('user', 'warehouse', 'supplier')->orderBy('id', 'DESC')->select('*');
        return $this->productImportService->renderDatatable($imports);
    }

    public function detail($id){
        $order = ProductImport::findOrFail($id)->load('detail');
        return view('admin.imports.detail')->with([
            'order' => $order
        ]);
    }

    public function create(Request $request){
        $suppliers = Supplier::all();
        $warehouses = Warehouse::all();

        if($request->product_id){
            $products = Product::whereIn('id', $request->product_id)->get();
        }

        return view('admin.imports.add')->with([
            'suppliers' => $suppliers,
            'warehouses' => $warehouses,
            'products' => $products ?? null
        ]);
    }

    public function store(Request $request){
        try {
            $request->validate([
                'products' => 'required',
                'warehouse_id' => 'required',
                'supplier_id' => 'required',
            ],[
                'products.required' => 'Bạn chưa chọn sản phẩm',
                'warehouse_id.required' => 'Bạn chưa chọn kho',
                'supplier_id.required' => 'Bạn chưa chọn nhà phân phối',
            ]);
    
            $data = $request->except('_token', 'products');
            $data['status'] = 1;
            $data['code'] = $this->productImportService->getImportCode();
            $data['created_by'] = Auth::id();
            $data['total'] = $request->total ? str_replace(',', '', $request->total) : 0;
            $data['import_date'] = format_date($request->import_date, 'Y-m-d');
    
            DB::beginTransaction();
    
            $import = ProductImport::create($data);
    
            if($import){
                $products = $request->products;
                $importDetail = [];
                foreach($products as $product){
                    $productQty = $product['qty'] ? (int)$product['qty'] : 0;
                    $productPrice = $product['price'] ? (int)str_replace(',', '', $product['price']) : 0;

                    $importDetail = [
                        'import_id' => $import->id,
                        'product_id' => $product['id'],
                        'qty' => $productQty,
                        'price' => $productPrice,
                        'subtotal' => $productPrice * $productQty,
                        'expired_date' => format_date($product['expired_date'], 'Y-m-d'),
                    ];
    
                    $productImportDetail = ProductImportDetail::create($importDetail);
    
                    //Inventory
                    $this->inventoryService->importInventory($product, $productImportDetail->id, $request->warehouse_id, $request->store_id, $request->import_date);
                }
                
                DB::commit();
                return redirect('admin/imports')->with('success', 'Tạo thành công!');
            }
            else{
                DB::rollback();
                return redirect('admin/imports/create')->with('danger', 'Tạo thất bại!');
            }
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect('admin/imports/create')->with('danger', $th->getMessage());
        }
    }

    public function detailImport(Request $request, $id){
        try {
            $import = ProductImport::where('id', $id)->with('detail')->first();
            $result = $this->productImportService->renderImportDetail($import, $request->type);
            return $this->responseJson(CODE_SUCCESS, $result);
        } catch (\Throwable $th) {
            return $this->responseJson(CODE_ERROR, null, $th->getMessage());
        }
    }

    public function destroy($id){
        $import = ProductImport::findOrFail($id);
        $delete = $import->delete();

        if($delete){
            return redirect('admin/imports')->with('success', 'Xóa thành công!');
        }
    }

}
