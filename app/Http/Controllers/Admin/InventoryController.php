<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Carbon\Carbon;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Inventory;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use App\Models\InventoryDetail;
use Yajra\DataTables\DataTables;
use App\Services\InventoryService;
use App\Http\Controllers\Controller;

class InventoryController extends Controller
{
    protected $inventoryService;

    public function __construct(InventoryService $inventoryService){
        $this->inventoryService = $inventoryService;
    }

    public function index(){
        $brands = Brand::where('type', 0)->active()->get();
        $warehouses = Warehouse::all();
        return view('admin.inventories.index', compact('brands', 'warehouses'));
    }

    public function getDatatable(){
        $imports = Inventory::with('product', 'warehouse')->orderBy('id', 'DESC')->select('*');
        return $this->inventoryService->renderDatatable($imports);
    }

    public function detail(Request $request){
        $productId = $request->product_id;
        $product = Product::where('id', $productId)->first();
        return view('admin.inventories.detail', ['productId' => $productId, 'product' => $product]);
    }

    public function detailView(Request $request){
        $productId = $request->productId;
        $query = InventoryDetail::with('product', 'warehouse');

        if($productId){
            $query->whereHas('product', function($q) use($productId){
                $q->where('id', $productId);
            });
        }

        $detail = $query->orderBy('id', 'DESC')->select('*');

        return $this->inventoryService->renderDatatableDetail($detail);
    }

    public function getLists(Request $request){
        $inventories = $this->inventoryService->buildQueryInventory($request);

        return $this->inventoryService->renderDatatable($inventories);
    }

    public function getFilterInventoryAnalytic(Request $request){
        $inventories = $this->inventoryService->buildQueryInventory($request);

        $inventories = $inventories->get();

        if($inventories){
            $inventories = $inventories->toArray();
            $totalProduct = 0;
            $totalPrice = 0;
            $amountImport = 0;
            $amountSold = 0;
            foreach ($inventories as $key => $value) {
                $totalProduct += $value['on_hand'];
                $totalPrice += (int)$value['price'] * (int)$value['on_hand'];
                $amountImport += $value['amount_import'];
                $amountSold += $value['amount_sold'];
            }
        }

        $html = view('admin.inventories.analytic', [
            'totalProduct' => $totalProduct,
            'totalPrice' => $totalPrice,
            'amountImport' => $amountImport,
            'amountSold' => $amountSold,
        ])->render();

        return $this->responseJson(CODE_SUCCESS, $html);
    }

}
