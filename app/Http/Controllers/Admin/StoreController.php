<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Store;
use App\Models\Supplier;
use App\Models\Warehouse;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Store::active()->get();
        return view('admin.stores.index')->with('lists', $lists);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.stores.add-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ],[
            'name.required' => 'Bạn chưa nhập tên chi nhánh',
        ]);

        //Upload image
        if($request->hasFile('logo')){
            $avatarPath = $this->uploadImage('stores', $request->file('logo'));
        }

        $data = $request->all();
        $data['status'] = isset($request->status) ? 1 : 0;
        $data['logo'] = $avatarPath ?? null;

        $create = Store::create($data);

        if($create){
            return redirect('admin/stores')->with('success', 'Tạo thành công!');
        }
        else{
            return redirect('admin/stores')->with('danger', 'Tạo thất bại!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $object = Store::findOrFail($id);
        return view('admin.stores.add-edit')->with([
            'object' => $object,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ],[
            'name.required' => 'Bạn chưa nhập tên chi nhánh',
        ]);

        $object = Store::findOrFail($id);

        if(!$object){
            return abort(404);
        }

        //Upload image
        if($request->hasFile('logo')){
            $avatarPath = $this->uploadImage('stores', $request->file('logo'));
            if($avatarPath){
                $this->deleteImage($object->logo);
            }
        }else{
            $avatarPath = $object->logo;
        }

        $data = $request->all();
        $data['status'] = isset($request->status) ? 1 : 0;
        $data['logo'] = $avatarPath ?? null;

        $update = $object->update($data);

        if($update){
            return redirect('admin/stores/edit/' . $id)->with('success', 'Sửa thành công!');
        }
        else{
            return redirect('admin/stores/edit/'. $id)->with('danger', 'Sửa thất bại!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = Store::findOrFail($id);

        if(!$object){
            return abort(404);
        }

        $delete = $object->delete();

        if($delete){
            return redirect('admin/stores')->with('success', 'Xóa thành công!');
        }
    }

    public function postActiveStore(Request $request){
        $storeId = $request->store_id;

        if(!$storeId){
            return $this->responseJson(CODE_ERROR, null, 'Xử lý thất bại!');
        }

        $user = Auth::user();

        $userObj = User::where('id', $user->id)->first();
        $userObj->store_active = $storeId;
        $userObj->save();

        return $this->responseJson(CODE_SUCCESS, $userObj);
    }

    public function checkCurrentStore(Request $request){
        $user = Auth::user();
        $userObj = User::where('id', $user->id)->first();
        $activeStoreId = $userObj->store_active;

        if(!$activeStoreId){
            return $this->responseJson(CODE_ERROR, null, 'Không tìm thấy chi nhánh hiện tại!');
        }

        return $this->responseJson(CODE_SUCCESS, ['store_id' => $activeStoreId]);
    }
}
