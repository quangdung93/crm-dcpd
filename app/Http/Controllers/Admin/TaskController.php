<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use Carbon\Carbon;
use App\Models\Task;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use App\Services\TaskService;

class TaskController extends Controller
{
    protected $userService;
    protected $taskService;

    public function __construct(UserService $userService, TaskService $taskService)
    {
        $this->userService = $userService;
        $this->taskService = $taskService;
    }

    public function index(){
        $customers = Customer::all();
        return view('admin.tasks.index', [
            'customers' => $customers,
            'users' => $this->userService->getListUserByPermission()
        ]);
    }

    public function getDatatable(){
        $query = Task::query();

        //Lấy DSKH theo quyền user
        $userIds = $this->userService->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('assign_by', $userIds);
        }

        $tasks = $query->with('assignBy')
                    ->orderBy('id', 'DESC')
                    ->select('*');

        return $this->taskService->renderDatatable($tasks);
    }

    public function getLists(Request $request){
        $query = Task::query();

        if($request->name){
            $keyword = trim($request->name);
            $query->where('task_name', 'like', '%'.$keyword.'%');
        }

        //Lấy DSKH theo quyền user
        $userIds = $this->userService->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('assign_by', $userIds);
        }

        if($request->assign_by){
            $query->where('assign_by', $request->assign_by);
        }
        
        if($request->task_type){
            $query->where('task_type', $request->task_type);
        }

        if($request->priority){
            $query->where('priority', $request->priority);
        }

        if($request->status){
            $query->where('status', $request->status);
        }

        $tasks = $query->with('assignBy')->select('*');

        return $this->taskService->renderDatatable($tasks);
    }

    public function destroy($id)
    {
        $model = Task::findOrFail($id);
        $delete = $model->delete();

        if($delete){
            return redirect('admin/tasks')->with('success', 'Xóa thành công!');
        }
    }

    public function createApi(Request $request){
        $data = $request->except('_token');
        $data['date_start'] = format_date($request->birthday, 'Y-m-d');
        $data['date_end'] = format_date($request->birthday, 'Y-m-d');
        $data['task_code'] = $this->taskService->getTaskCode();
        $data['task_time'] = $request->task_time ?: 0;
        $data['created_by'] = Auth::id();

        $task = Task::create($data);

        if ($task) {
            return $this->responseJson(CODE_SUCCESS, $task);
        }
        else{
            return $this->responseJson(CODE_ERROR, null, 'Xử lý thất bại!');
        }
    }

    public function showEdit(Request $request){
        $taskId = $request->task_id;

        $task = Task::where('id', $taskId)->with('customer')->first();

        $html = view('admin.modal.add-task', [
            'mode' => 'edit_task',
            'task' => $task,
            'customers' => Customer::all(),
            'customerId' => $task->customer_id,
            'customerName' => optional($task->customer)->name ?? '',
            'users' => $this->userService->getListUserByPermission()
        ])->render();

        if ($html) {
            return $this->responseJson(CODE_SUCCESS, $html);
        }
        else{
            return $this->responseJson(CODE_ERROR, null, 'Xử lý thất bại!');
        }
    }

    public function updateApi(Request $request){
        $data = $request->except('_token', 'task_id');
        $data['date_start'] = format_date($request->birthday, 'Y-m-d');
        $data['date_end'] = format_date($request->birthday, 'Y-m-d');
        $data['task_time'] = $request->task_time ?: 0;
        $data['updated_by'] = Auth::id();

        $taskId = $request->task_id;

        $task = Task::where('id', $taskId)->first();

        $update = $task->update($data);

        if ($update) {
            return $this->responseJson(CODE_SUCCESS, $update);
        }
        else{
            return $this->responseJson(CODE_ERROR, null, 'Xử lý thất bại!');
        }
    }

}
