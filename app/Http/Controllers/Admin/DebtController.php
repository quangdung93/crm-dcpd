<?php

namespace App\Http\Controllers\Admin;

use Excel;
use App\Models\Debt;
use App\Models\Store;
use App\Models\Customer;
use App\Exports\DebtExport;
use Illuminate\Http\Request;
use App\Services\DebtService;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use N2W;
use PDF;

class DebtController extends Controller
{
    protected $debtService;
    protected $userService;

    public function __construct(
        DebtService $debtService,
        UserService $userService
    ){
        $this->debtService = $debtService;
        $this->userService = $userService;
    }
    
    /**
     * Báo cáo công nợ
     *
     * @return void
     */
    public function index(){

        $queryCustomers = Customer::where('customer_group', Customer::GROUP_SINGLE);
        $queryAgency = Customer::where('customer_group', Customer::GROUP_AGENCY);
        $queryCustomerSale = Customer::where('customer_group', Customer::GROUP_SALE);

        //Lấy DSKH theo quyền user
        $userIds = $this->userService->getUserIdsByPermission();
        if($userIds){
            $queryCustomers->whereIn('saler_id', $userIds);
            $queryAgency->whereIn('saler_id', $userIds);
            $queryCustomerSale->whereIn('saler_id', $userIds);
        }

        $customers = $queryCustomers->get();
        $agency = $queryAgency->get();
        $customerSale = $queryCustomerSale->get();


        return view('admin.debts.index',[
            'users' => $this->userService->getListUserByPermission(),
            'customers' => $customers,
            'agency' => $agency,
            'customerSale' => $customerSale,
        ]);
    }

    public function getLists(Request $request){
        $export = $this->debtService->buildQueryCustomerDebt($request);
        return $this->debtService->renderDatatableCustomerDebt($export);
    }

    public function getAnalytic(Request $request){
        $debt = $this->debtService->buildQueryCustomerDebt($request);
        $debt = $debt->get();

        if($debt){
            $totalCustomer = 0;
            $totalLack = 0;
            foreach ($debt as $value) {
                $totalLack += $value->debt;
                $totalCustomer++;
            }
        }

        $html = view('admin.debts.debt-analytic', [
            'totalCustomer' => $totalCustomer,
            'totalLack' => $totalLack,
        ])->render();

        return $this->responseJson(CODE_SUCCESS, $html);
    }

    public function export(Request $request){
        $debts = $this->debtService->buildQueryCustomerDebt($request)->get();
        $dataExport = [];
        foreach ($debts as $key => $value) {
            $dataExport[] = [
                'customer_code' => $value->customer_code,
                'customer_name' => $value->name,
                'customer_group' => getCustomerGroup($value->customer_group),
                'total_lack' => number_format($value->total_lack),
            ];
        }

        return Excel::download(new DebtExport($dataExport), 'cong-no-khach-hang.xlsx');
    }

    public function print($customerId){
        $debts = Debt::where('object_id', $customerId)
                    ->where('status', Debt::UNPAID)
                    ->with('customer', 'order.detail')
                    ->get();

        if(!$debts){
            return abort(404);
        }

        $customer = Customer::where('id', $customerId)->first(); 

        // $moneyToVietnamese = N2W::toCurrency($order->total_money);

        $store = Store::where('id', $debts->first()->store_id)->first();

        $pdf = PDF::loadView('admin.debts.print', [
            'debts' => $debts,
            'store' => $store,
            'customer' => $customer,
            // 'moneyToVietnamese' => $moneyToVietnamese
        ])->setPaper('a4', 'landscape');

        $pdf->setOptions(['isRemoteEnabled' => true]);

        return $pdf->stream();
    }
}
