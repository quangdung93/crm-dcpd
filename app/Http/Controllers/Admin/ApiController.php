<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ChartService;

class ApiController extends Controller
{
    protected $chartService;

    public function __construct(ChartService $chartService)
    {
        $this->chartService = $chartService;
    }

    public function chartOrderSourceAction(Request $request){

        $data = $this->chartService->getOrderSource($request->all());

        $dataChart = [];
        $newData = [];
        if($data){
            foreach ($data as $key => $item) {
                $source = renderOrderSource($key);
                $newData[$source] = (double)$item;
            }

            $dataChart = [
                'label' => array_keys($newData),
                'data' => array_values($newData),
            ];
        }

        return $this->responseJson(CODE_SUCCESS, $dataChart);
    }

    public function chartOrderCustomerAction(Request $request){

        $data = $this->chartService->getOrderTopCustomer($request->all());

        $dataChart = [];
        if($data){
            $dataChart = [
                'label' => array_keys($data),
                'data' => array_values($data),
            ];
        }

        return $this->responseJson(CODE_SUCCESS, $dataChart);
    }

    public function chartOrderRevenueAction(Request $request){

        $period = getRangeDay(format_date($request->date_from, 'Y-m-d'), format_date($request->date_to, 'Y-m-d'));
        $countPeriod = $period->count();

        $days = [];
        foreach ($period as $date) {
            if($countPeriod <= 31){
                $days[] = (int)$date->format('d');
            }
            elseif($countPeriod > 31 && $countPeriod <= 365){
                $days[] = (int)$date->format('m');
            }
            elseif($countPeriod > 365){
                $days[] = (int)$date->format('Y');
            }
        }

        $type = 'day';
        if($countPeriod > 31 && $countPeriod <= 365){
            $days = array_values(array_unique($days));
            $type = 'month';
        }
        elseif($countPeriod > 365){
            $days = array_values(array_unique($days));
            $type = 'year';
        }

        $data = $this->chartService->getOrderRevenue($request->all(), $type);

        $dataChart = [];
        if($data){
            $dataChart = [
                'days' => $days,
                'data' => $data,
                'type' => $type
            ];
        }

        return $this->responseJson(CODE_SUCCESS, $dataChart);
    }

    public function chartOrderEmployeeAction(Request $request){
        $data = $this->chartService->getOrderEmployee($request->all());

        $dataChart = [];
        if($data){
            $dataChart = [
                'label' => array_keys($data),
                'data' => array_values($data),
            ];
        }

        return $this->responseJson(CODE_SUCCESS, $dataChart);
    }
    
}
