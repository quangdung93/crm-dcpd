<?php

namespace App\Models;

use App\Scopes\StoreScope;
use App\Traits\LogActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductExport extends Model
{
    use SoftDeletes;
    use LogActivity;

    protected $table = 'product_exports';

    protected $guarded = [];
    protected $dates = ['deleted_at'];
    public $timestamps = false;

    const FINISH = 'finish';
    const CANCEL = 'cancel';

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new StoreScope);
    }

    public function detail(){
        return $this->belongsToMany(Product::class, 'product_export_detail','export_id', 'product_id')
        ->withPivot('product_id', 'qty', 'price', 'subtotal', 'warehouse_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function warehouse(){
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }

    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }
}
