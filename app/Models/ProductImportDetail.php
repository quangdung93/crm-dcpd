<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImportDetail extends Model
{
    use HasFactory;

    protected $table = 'product_import_detail';

    protected $guarded = [];

    public $timestamps = false;
}
