<?php

namespace App\Models;

use App\Models\Ward;
use App\Models\District;
use App\Models\Province;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    protected $guarded = [];

    const TYPE_NON_MEMBER = 1000000;
    const TYPE_MEMBER = 30000000;
    const TYPE_SILVER = 60000000;
    const TYPE_GOLDEN = 90000000;
    const TYPE_PLATINUM = 150000000;

    //Customer Group
    const GROUP_SINGLE = 0; //KH lẻ
    const GROUP_AGENCY = 1; //KH đại lý
    const GROUP_SALE = 2; //KH Nhân viên sale

    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function saler(){
        return $this->belongsTo(User::class, 'saler_id');
    }

    public function province(){
        return $this->belongsTo(Province::class);
    }

    public function district(){
        return $this->belongsTo(District::class);
    }

    public function ward(){
        return $this->belongsTo(Ward::class);
    }

    public function pointsHistory(){
        return $this->hasMany(CustomerPoint::class, 'customer_id', 'id');
    }
}
