<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingPartner extends Model
{
    protected $table = 'shipping_partners';
    protected $guarded = [];

    public function scopeActive($query){
        return $query->where('status', 1);
    }
}
