<?php

namespace App\Models;

use App\Scopes\StoreScope;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = 'inventories';

    protected $guarded = [];

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new StoreScope);
    }

    public function warehouse(){
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
