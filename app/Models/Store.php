<?php

namespace App\Models;

use App\Traits\LogActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Store extends Model
{
    use SoftDeletes;
    use LogActivity;
    
    protected $table = 'stores';

    protected $guarded = [];
    protected $dates = ['deleted_at'];
    const DEFAULT = 2;

    public function scopeActive($query){
        return $query->where('status', 1);
    }

    public static function getDefaultStore(){
        $stores = Auth::user()->stores;
        $userStores = [];
        if($stores){
            $userStores = explode(',', $stores);
        }

        if(count($userStores) > 0){
            return $userStores[0];
        }

        return null;
    }
}
