<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerCare extends Model
{
    protected $table = 'customer_care';

    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }
}
