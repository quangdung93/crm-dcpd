<?php

namespace App\Models;

use App\Helpers\ShortcodeHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ProductTemp extends Model
{
    protected $table = 'tbl_product';
    protected $connection = 'mysql2';

    protected $guarded = [];
}
