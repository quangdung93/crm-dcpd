<?php

namespace App\Models;

use App\Scopes\StoreScope;
use App\Traits\LogActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Warehouse extends Model
{
    use SoftDeletes;
    use LogActivity;
    
    protected $table = 'warehouses';

    protected $guarded = [];
    protected $dates = ['deleted_at'];

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new StoreScope);
    }

    public function scopeActive($query){
        return $query->where('status', 1);
    }

    public function store(){
        return $this->belongsTo(Store::class, 'store_id');
    }
}
