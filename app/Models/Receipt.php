<?php

namespace App\Models;

use App\Scopes\StoreScope;
use App\Traits\LogActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Receipt extends Model
{
    use SoftDeletes;
    use LogActivity;

    protected $table = 'receipts';

    protected $guarded = [];
    protected $dates = ['deleted_at'];
    const FINISH = 'finish';
    const CANCEL = 'cancel';
    const TYPE_CUSTOMER = 'customer';
    const TYPE_OTHER = 'other';

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new StoreScope);
    }

    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }
}
