<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductExportDetail extends Model
{
    use HasFactory;

    protected $table = 'product_export_detail';

    protected $guarded = [];

    public $timestamps = false;
}
