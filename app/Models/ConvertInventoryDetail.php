<?php

namespace App\Models;

use App\Helpers\ShortcodeHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ConvertInventoryDetail extends Model
{
    // protected $table = 'tbl_orders';
    // protected $table = 'tbl_points';
    // protected $table = 'tbl_customer';
    // protected $table = 'tbl_product';
    // protected $table = 'tbl_products_manufacture';
    protected $table = 'tbl_kho_code';
    // protected $table = 'tbl_kho';
    // protected $table = 'tbl_users';
    // protected $primaryKey = 'kho_code_id';

    protected $guarded = [];
}
