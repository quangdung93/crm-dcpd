<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerPoint extends Model
{
    protected $table = 'customer_points';

    protected $guarded = [];

    const ADD = 'add';
    const REMOVE = 'remove';

    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }
}
