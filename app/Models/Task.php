<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';

    protected $guarded = [];

    const TYPE_CALL = 'call';
    const TYPE_EMAIL = 'email';
    const TYPE_MESSAGE = 'message';
    const TYPE_METTING = 'meeting';
    const TYPE_ADVISE = 'advise';
    const TYPE_CUSTOMER_CARE = 'customer_care';
    const TYPE_OTHER = 'other';

    const PRIORITY_LOW = 'low';
    const PRIORITY_MEDIUM = 'medium';
    const PRIORITY_HIGH = 'high';

    const STATUS_NEW = 'new';
    const STATUS_PROGRESS = 'progress';
    const STATUS_PENDING = 'pending';
    const STATUS_FINISH = 'finish';
    const STATUS_CANCEL = 'cancel';

    const TYPE = [
        self::TYPE_CALL => 'Gọi điện',
        self::TYPE_EMAIL => 'Gửi mail',
        self::TYPE_MESSAGE => 'Tin nhắn',
        self::TYPE_METTING => 'Họp',
        self::TYPE_ADVISE => 'Tư vấn',
        self::TYPE_CUSTOMER_CARE => 'Chăm sóc khách hàng',
        self::TYPE_OTHER => 'Khác',
    ];

    const STATUS = [
        self::STATUS_NEW => 'Mới',
        self::STATUS_PROGRESS => 'Đang tiến hành',
        self::STATUS_PENDING => 'Tạm ngưng',
        self::STATUS_FINISH => 'Hoàn thành',
        self::STATUS_CANCEL => 'Hủy bỏ',
    ];

    const PRIORITY = [
        self::PRIORITY_LOW => 'Thấp',
        self::PRIORITY_MEDIUM => 'Trung bình',
        self::PRIORITY_HIGH => 'Cao',
    ];

    public function createdBy(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy(){
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function assignBy(){
        return $this->belongsTo(User::class, 'assign_by');
    }

    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id');
    }

}
