<?php

namespace App\Models;

use App\Scopes\StoreScope;
use App\Traits\LogActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Debt extends Model
{
    use SoftDeletes;
    use LogActivity;

    protected $table = 'debts';

    protected $guarded = [];
    protected $dates = ['deleted_at'];
    const UNPAID = 'unpaid';
    const PAID = 'paid';
    const CANCEL = 'cancel';
    const TYPE_CUSTOMER = 'customer';
    const TYPE_SUPPLIER = 'supplier';

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new StoreScope);
    }

    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function order(){
        return $this->belongsTo(Order::class, 'document_id');
    }

    public function customer(){
        return $this->belongsTo(Customer::class, 'object_id');
    }

}
