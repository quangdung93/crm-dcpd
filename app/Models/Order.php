<?php

namespace App\Models;

use App\Scopes\StoreScope;
use App\Traits\LogActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use SoftDeletes;
    use LogActivity;

    protected $table = 'orders';

    protected $guarded = [];
    protected $dates = ['deleted_at'];
    const WAITING = 0;
    const EXPORTED = 5;
    const SHIPPING = 6;
    const FINISH = 1;
    const FAILED = 2;
    const CANCEL = 3;
    const ADMIN_WAITING = 0;
    const ADMIN_APPROVE = 1;
    const INVENTORY_NOT_EXPORT = 0;
    const INVENTORY_EXPORT = 1;

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new StoreScope);
    }

    public function detail(){
        return $this->belongsToMany(Product::class, 'order_detail','order_id', 'product_id')
        ->withPivot('id', 'qty', 'price_old', 'price', 'discount', 'subtotal');
    }

    public function province(){
        return $this->belongsTo(Province::class);
    }

    public function district(){
        return $this->belongsTo(District::class);
    }

    public function ward(){
        return $this->belongsTo(Ward::class);
    }

    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function saler(){
        return $this->belongsTo(User::class, 'saler_id');
    }

    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function scopeType($query, $type){
        return $query->where('order_type', $type);
    }

    public function shipping(){
        return $this->hasOne(Shipping::class, 'order_id');
    }

    public function scopeWithHas($query, $relation, $callback){
        return $query->whereHas($relation, $callback)->with([$relation => $callback]);
    }
}
