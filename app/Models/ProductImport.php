<?php

namespace App\Models;

use App\Scopes\StoreScope;
use App\Traits\LogActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductImport extends Model
{
    use SoftDeletes;
    use LogActivity;

    protected $table = 'product_imports';
    protected $dates = ['deleted_at'];
    protected $guarded = [];

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new StoreScope);
    }

    public function detail(){
        return $this->belongsToMany(Product::class, 'product_import_detail','import_id', 'product_id')
        ->withPivot('qty', 'price', 'subtotal', 'expired_date');
    }

    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function warehouse(){
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }

    public function supplier(){
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }
}
