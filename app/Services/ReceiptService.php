<?php 

namespace App\Services;

use Log;
use Auth;
use Carbon\Carbon;
use App\Models\Brand;
use App\Models\Order;
use App\Models\Receipt;
use App\Models\Category;
use App\Models\Supplier;
use App\Models\Inventory;
use App\Models\Warehouse;
use App\Scopes\StoreScope;
use App\Models\ProductExport;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Models\ProductExportDetail;

class ReceiptService
{
    public function getCode(){
        $order = DB::table('receipts')->orderBy('id', 'DESC')->first();
        $prefix = 'PT';

        if(!$order){
            return $prefix.'0000001';
        }

        $orderCode = $order->code;
        $maxCode = (int)(str_replace($prefix, '', $orderCode)) + 1;

        if ($maxCode < 10)
            return $prefix .'000000' . ($maxCode);
        else if ($maxCode < 100)
            return $prefix .'00000' . ($maxCode);
        else if ($maxCode < 1000)
            return $prefix .'0000' . ($maxCode);
        else if ($maxCode < 10000)
            return $prefix .'000' . ($maxCode);
        else if ($maxCode < 100000)
            return $prefix .'00' . ($maxCode);
        else if ($maxCode < 1000000)
            return $prefix .'0' . ($maxCode);
        else if ($maxCode < 10000000)
            return $prefix . ($maxCode);
    }

    public function renderDatatable($table){
        $data = Datatables::of($table)
            ->editColumn('code', function ($row) {
                return '<a href="#" class="text-info">'.$row->code.'</a>';
            })
            ->editColumn('order_code', function ($row) {
                return '<a href="#" class="text-info">'.optional($row->order)->code.'</a>';
            })
            ->editColumn('customer', function ($row) {
                return '<a href="'.url('admin/customers/detail/'.$row->customer_id).'" class="text-info">'.optional($row->customer)->name.'</a>';
            })
            ->editColumn('value', function ($row) {
                return format_price($row->value) . ' đ';
            })
            ->editColumn('type', function ($row) {
                return 'Thu công nợ';
            })
            ->editColumn('created_by', function ($row) {
                return optional($row->user)->name;
            })
            ->editColumn('created_at', function ($row) {
                return format_datetime($row->created_at);
            })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";

                $action .= '<a href="'.url('admin/receipts/print/'.$row->id).'" target="_blank" class="btn btn-success" title="In phiếu thu">
                    <i class="feather icon-printer"></i>
                </a>';

                return $action;
            })
            ->rawColumns(['code', 'customer', 'order_code', 'status', 'action'])
            ->make(true);
        return $data;
    }

}