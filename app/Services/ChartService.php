<?php 

namespace App\Services;

use Carbon\Carbon;
use App\Models\Order;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ChartService
{
    public function buildQueryFilter($query, $filters){

        if($filters['revenue_type'] == 1){
            $query->where('admin_status', Order::ADMIN_APPROVE);
        }

        if($filters['revenue_type'] == 2){
            $query->where('admin_status', Order::ADMIN_WAITING);
        }

        if($filters['date_from']){
            $query->where('sell_date', '>=', format_date($filters['date_from'], 'Y-m-d 00:00:00'));
        }

        if($filters['date_to']){
            $query->where('sell_date', '<=', format_date($filters['date_to'], 'Y-m-d 23:59:59'));
        }

        return $query;
    }

    public function getOrderSource($filters){
        $query = Order::selectRaw("source_id,
                SUM(total_money) as total_money, 
                count(id) as count")
                ->where('status', Order::FINISH);

        $query = $this->buildQueryFilter($query, $filters);

        $data = $query->groupBy('source_id')
                ->pluck('total_money', 'source_id')
                ->toArray();

        return $data;
    }

    public function getOrderRevenue($filters, $type){
        $query = Order::query();
        
        if($type == 'day'){
            $query->selectRaw("DATE(sell_date) as date, 
                DAY(sell_date) as day,
                SUM(total_money) as total_money");
        }
        elseif($type == 'month'){
            $query->selectRaw("MONTH(sell_date) as date, 
                MONTH(sell_date) as day,
                SUM(total_money) as total_money");
        }
        elseif($type == 'year'){
            $query->selectRaw("YEAR(sell_date) as date, 
                YEAR(sell_date) as day,
                SUM(total_money) as total_money");
        }

        $query->where('status', Order::FINISH);

        $query = $this->buildQueryFilter($query, $filters);

        $data = $query->groupBy('date', 'day')->get()
                ->pluck('total_money', 'day')
                ->toArray();

        return $data;
    }

    public function getOrderTopCustomer($filters){
        $query = Order::selectRaw("customers.name as customer_name,
                SUM(orders.total_money) as total_money")
                ->leftJoin('customers', 'customers.id', '=', 'orders.customer_id')
                ->where('orders.status', Order::FINISH);

        $query = $this->buildQueryFilter($query, $filters);

        $data = $query->groupBy('customer_name')
                ->orderBy('total_money', 'DESC')
                ->limit(10)
                ->pluck('total_money', 'customer_name')
                ->toArray();

        return $data;
    }

    public function getOrderEmployee($filters){
        $query = Order::selectRaw("users.name as sale_name,
                SUM(orders.total_money) as total_money")
                ->leftJoin('users', 'users.id', '=', 'orders.saler_id')
                ->where('orders.status', Order::FINISH);

        $query = $this->buildQueryFilter($query, $filters);

        $data = $query->groupBy('sale_name')
                ->pluck('total_money', 'sale_name')
                ->toArray();

        return $data;
    }
}