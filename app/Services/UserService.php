<?php 

namespace App\Services;

use Auth;
use Carbon\Carbon;
use App\Models\User;

class UserService
{
    public function getListUserByPermission(){
        $user = Auth::user();
        $query = User::query();

        if(!isAdmin()){
            $userIds = explode(',', $user->managers);
            if($userIds){
                $query->whereIn('id', array_merge(array_map('intval', $userIds), [$user->id]));
            }
            else{
                $query->where('id', $user->id);
            }
        }

        $users = $query->active()->get();

        return $users;
    }
    
    /**
     * Lấy danh sách id người dùng dưới quyền
     *
     * @return array
     */
    public function getUserIdsByPermission(){
        $user = Auth::user();

        if(isAdmin()){
            return [];
        }
        else{
            $userIds = explode(',', $user->managers);
            if($userIds){
                $merge = array_merge(array_map('intval', $userIds), [$user->id]);
                return $merge ? array_values(array_filter($merge)) : [];
            }
        }

        return [$user->id]; //array
    }
}