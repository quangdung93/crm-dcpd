<?php 

namespace App\Services;

use Auth;
use Carbon\Carbon;
use App\Models\Inventory;
use App\Models\InventoryDetail;
use App\Models\Store;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class InventoryService
{
    public function renderDatatable($table){
        $data = Datatables::of($table)
            ->editColumn('product_code', function ($row) {
                return '<a href="inventories/detail/'.optional($row->product)->id.'" class="text-warning" target="_blank">'.optional($row->product)->code.'</a>';
            })
            ->editColumn('product_name', function ($row) {
                return '<a href="products/edit/'.optional($row->product)->id.'" class="text-primary" target="_blank">'.optional($row->product)->name.'</a>';
            })
            ->editColumn('price', function ($row) {
                return format_price($row->price) . ' đ';
            })
            ->editColumn('on_hand', function ($row) {
                $html = '<div>Tồn kho: '.$row->on_hand.' tại <span class="font-weight-bold text-danger">'.optional($row->warehouse)->name.'</span></div>';
                $html .= '<div>Đã nhập: '.$row->amount_import.'</div>';
                $html .= '<div>Đã bán: '.$row->amount_sold.'</div>';
                return $html;
            })
            ->editColumn('import_date', function ($row) {
                return $row->import_date ? format_date($row->import_date) : '';
            })
            ->editColumn('expires_date', function ($row) {
                return $row->expires_date ? format_date($row->expires_date) : '';
            })
            ->rawColumns(['product_code', 'product_name', 'on_hand'])
            ->make(true);
        return $data;
    }

    public function renderDatatableDetail($table){
        $data = Datatables::of($table)
            ->editColumn('product_code', function ($row) {
                return '<a href="/admin/products/edit/'.optional($row->product)->id.'" class="text-info btn-detail-order">'.optional($row->product)->code.'</a>';
            })
            ->editColumn('inventory_code', function ($row) {
                return '<div><img width="50" src="'.asset_image('inventory_code/'.$row->code_file).'"/><div>'.$row->product_code.'</div></div>';
            })
            ->editColumn('warehouse', function ($row) {
                return  optional($row->warehouse)->name;
            })
            ->editColumn('import_date', function ($row) {
                return $row->import_date ? format_date($row->import_date) : '';
            })
            ->editColumn('export_date', function ($row) {
                return $row->export_date ? format_date($row->export_date) : '';
            })
            ->editColumn('order_code', function ($row) {
                return  $row->order_code;
            })
            ->editColumn('export_by', function ($row) {
                return  optional($row->user)->name;
            })
            ->editColumn('expried_date', function ($row) {
                return $row->expried_date ? format_date($row->expried_date) : '';
            })
            ->editColumn('is_export', function ($row) {
                return $row->is_export == 1 ? '<label class="label label-danger">Đã xuất kho</label>' : '<label class="label label-success">Chưa xuất kho</label>';
            })
            ->rawColumns(['inventory_code', 'product_code', 'is_export'])
            ->make(true);
        return $data;
    }
    
    /**
     * Nhập kho
     *
     * @param  mixed $product
     * @param  mixed $warehouseId
     * @param  mixed $importDate
     * @return void
     */
    public function importInventory($product, $productImportDetailId, $warehouseId, $storeId, $importDate){
        try {
            DB::beginTransaction();
        
            $inventoryData = [
                'product_id' => $product['id'],
                'warehouse_id' => $warehouseId,
                'store_id' => $storeId ?: Store::DEFAULT,
                'price' => (int)str_replace(',', '', $product['price']),
                'on_hand' => (int)$product['qty'],
                'amount_import' => (int)$product['qty'],
                'import_date' => format_date($importDate, 'Y-m-d'),
                'expires_date' => format_date($product['expired_date'], 'Y-m-d'),
            ];

            $inventory = Inventory::where([
                'product_id' => $product['id'], 
                'warehouse_id' => $warehouseId
            ])->first();

            if($inventory){
                $inventory->update([
                    'on_hand' => (int)$inventory->on_hand + (int)$product['qty'], 
                    'amount_import' => $inventory->amount_import + (int)$product['qty'], 
                    'price' => (int)str_replace(',', '', $product['price'])
                ]);
            }
            else{
                Inventory::create($inventoryData);
            }

            //Nhập mã code kho cho từng sản phẩm 
            $dataInventoryDetail = [];
            for ($i = 1; $i <= (int)$product['qty']; $i++) {
                $productInventoryCode = $product['code'].'-'.$productImportDetailId.'-'.$i;

                //QR code
                $codesDir = Storage::disk('local')->path('public/inventory_code/');
                $codeFileName = $productInventoryCode.'-'.date('d-m-Y-h-i-s').'.svg';
                $codePath = $codesDir.$codeFileName;
                qrCodeGenerate($productInventoryCode, $codePath);

                $dataInventoryDetail[] = [
                    'product_import_detail_id' => $productImportDetailId,
                    'product_id' => $product['id'],
                    'product_code' => $productInventoryCode,
                    'code_file' => $codeFileName,
                    'warehouse_id' => $warehouseId,
                    'price' => (int)str_replace(',', '', $product['price']),
                    'import_date' => format_date($importDate, 'Y-m-d'),
                    'expried_date' => format_date($product['expired_date'], 'Y-m-d')
                ];
            }

            InventoryDetail::insert($dataInventoryDetail);  
            DB::commit();

        } catch (\Throwable $th) {
            DB::rollback();
        }
    }
    
    /**
     * Xuất kho
     *
     * @param  mixed $product
     * @param  mixed $warehouseId
     * @return void
     */
    public function exportInventory($product, $warehouseId, $orderCode = null, $productExportDetailId = null){
        $quantity = (int)$product['pivot']['qty'];

        $inventory = Inventory::where([
            'product_id' => $product['id'], 
            'warehouse_id' => $warehouseId
        ])->first();

        if($inventory){
            $inventory->update([
                'on_hand' => (int)$inventory->on_hand - $quantity, 
                'amount_sold' => $inventory->amount_sold + $quantity
            ]);
        }
        else{
            $inventoryData = [
                'product_id' => $product['id'],
                'warehouse_id' => $warehouseId,
                'price' => $product['price'],
                'on_hand' => 0 - $quantity,
                'amount_import' => 0,
                'amount_sold' => $quantity
            ];

            Inventory::create($inventoryData);
        }

        //Xuất kho cho từng sản phẩm (Nhập trước xuất trước)
        $inventoryDetail = InventoryDetail::where([
            'product_id' => $product['id'],
            'is_export' => 0,
        ])->orderBy('id', 'ASC');

        if($inventoryDetail->get()->count() < $quantity){
            return false;
        }

        $inventoryDetail->skip(0)->take($quantity)->update([
            'is_export' => 1,
            'export_user_id' => Auth::id(),
            'product_export_detail_id' => $productExportDetailId,
            'export_date' => format_date(Carbon::now(), 'Y-m-d'),
            'order_code' => $orderCode
        ]);

        return true;
    }

    public function buildQueryInventory($request){
        $query = Inventory::query();

        if($request->name){
            $query->whereHas('product', function($q) use($request){
                $q->where('name', 'like', '%'.$request->name.'%');
                $q->orWhere('code', $request->name);
            });
        }

        if($request->warehouse_id){
            $query->where('warehouse_id', $request->warehouse_id);
        }
        
        if($request->brand_id){
            $query->whereHas('product', function($q) use($request){
                $q->where('brand_id', $request->brand_id);
            });
        }

        $inventories = $query->with('product', 'warehouse')->select('*');

        return $inventories;
    }


}