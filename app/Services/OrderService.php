<?php 

namespace App\Services;

use Log;
use Auth;
use Carbon\Carbon;
use App\Models\Brand;
use App\Models\Order;
use App\Models\Product;
use App\Models\Category;
use App\Models\Supplier;
use App\Models\Inventory;
use App\Models\Warehouse;
use App\Scopes\StoreScope;
use App\Models\ProductExport;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Models\ProductExportDetail;

class OrderService
{
    public function getOrderCode(){
        $order = DB::table('orders')->orderBy('id', 'DESC')->first();
        $prefix = 'DH';
        $orderCode = $order->code;
        $maxCode = (int)(str_replace($prefix, '', $orderCode)) + 1;

        if ($maxCode < 10)
            return $prefix .'000000' . ($maxCode);
        else if ($maxCode < 100)
            return $prefix .'00000' . ($maxCode);
        else if ($maxCode < 1000)
            return $prefix .'0000' . ($maxCode);
        else if ($maxCode < 10000)
            return $prefix .'000' . ($maxCode);
        else if ($maxCode < 100000)
            return $prefix .'00' . ($maxCode);
        else if ($maxCode < 1000000)
            return $prefix .'0' . ($maxCode);
        else if ($maxCode < 10000000)
            return $prefix . ($maxCode);
    }

    public function renderOrderDetail($order, $type = 'customer'){
        $htmlSupplier = '';
        $htmlSupplierHeader = '';

        $htmlInventoryHeader = '';

        if($type == 'order' && isAdmin()){ // Nhà phân phối
            $htmlSupplierHeader = '<td>Nhà phân phối</td>';
        }
        
        $request = request()->all();

        if($type == 'export'){
            $htmlInventoryHeader = '<td>Tồn kho</td><td>Kho xuất</td>';
        }

        $exchangePointHtml = '';
        if($order->exchange_points){
            $exchangePoints = $order->exchange_points;
            $exchangePointHtml = "(Đổi $exchangePoints điểm tích lũy)";
        }

        $statusOrder = renderOrderStatusLabel($order->status);
        $adminStatusOrder = renderOrderAdminStatusLabel($order->admin_status);
        $inventoryStatus = renderOrderInventoryStatusLabel($order->inventory_status);

        $html = '
        <form id="frm-order-detail" action="">
        <div class="row">
        <div class="col-sm-6">
            <input type="hidden" class="form-control" id="store" name="store_id"/>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Khách hàng:</label>
                <div class="col-sm-8 col-form-label">
                    <a class="text-primary" href="'.url('admin/customers/detail/'.$order->customer_id).'">'.optional($order->customer)->name.'</a>
                </div>
            </div>
            <div class="form-group row">
                <input type="hidden" value="'.$order->id.'" name="order_id"/>
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Tiền hàng:</label>
                <div class="col-sm-8 col-form-label text-primary">'.number_format($order->total_price).' đ</div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Giảm giá tổng đơn:</label>
                <div class="col-sm-8 col-form-label text-success">'.number_format($order->coupon).' đ '.$exchangePointHtml.'</div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Tổng đơn hàng:</label>
                <div class="col-sm-8 col-form-label text-danger">'.number_format($order->total_money).' đ</div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Còn nợ:</label>
                <div class="col-sm-8 col-form-label text-warning">'.number_format($order->total_money - $order->customer_pay).' đ</div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Người bán:</label>
                <div class="col-sm-8 col-form-label">
                    '.optional($order->saler)->name.'
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Tổng sản phẩm:</label>
                <div class="col-sm-8 col-form-label text-primary">'.$order->total_quantity.'</div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Trạng thái:</label>
                <div class="col-sm-4 col-form-label">
                    <label class="label label-'.$statusOrder[1].'">'.$statusOrder[0].'</label>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Trạng thái kho:</label>
                <div class="col-sm-4 col-form-label">
                    <label class="label label-'.$inventoryStatus[1].'">'.$inventoryStatus[0].'</label>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Admin xác nhận:</label>
                <div class="col-sm-4 col-form-label">
                    <label class="label label-'.$adminStatusOrder[1].'"><i class="feather icon-'.$adminStatusOrder[2].'"></i> '.$adminStatusOrder[0].'</label>
                </div>
            </div>
        </div>
        </div>
        <div class="card-datatable table-responsive"><table class="table-order-detail table" width="100%">
        <thead class="thead-light">
            <tr class="bg-primary text-white">
            <td>Mã sản phẩm</td>
            <td>Tên sản phẩm</td>
            '.$htmlSupplierHeader.'
            '.$htmlInventoryHeader.'
            <td>Số lượng</td>
            <td>Giá bán</td>
            <td>Chiết khấu</td>
            <td>Thành tiền</td>
            </tr>
        </thead><tbody>';

        if($order && $order->detail){
            foreach ($order->detail as $key => $value) {
                $htmlOnHand = '';
                $htmlInventory = '';
                if($type == 'order'){ // Nhà phân phối

                    if(isAdmin()){
                        $suppliers = Supplier::active()->get();
                        $htmlSupplier = '<td><select class="form-control select2-modal"><option value="0">Chọn nhà phân phối</option>';
            
                        foreach ($suppliers as $supplier) {
                            $htmlSupplier .= '<option value="'.$supplier->id.'">'.$supplier->name.'</option>';
                        }
                    }
                }
                elseif($type == 'export'){ //Xuất kho
                    $defaultText = 'Không xuất kho';
                    $classPrevent = '';
                    //Export
                    $exports = ProductExport::where('order_id', $order->id)->with('detail')->get();
                    if($exports){
                        $exports = $exports->toArray();
                        $productIds = data_get($exports, '*.detail.*.pivot.product_id');
                        if(in_array($value->pivot->product_id, $productIds)){
                            $defaultText = 'Đã xuất kho';
                            $classPrevent = BLOCKED;
                        }
                    }

                    //Warehouse
                    $warehouses = Warehouse::all();
                    $htmlInventory .= '<td><select class="form-control '.$classPrevent.'" name="inventory_ids['.$value->id.']"><option value="0">'.$defaultText.'</option>';
                    $temp = '';
                    foreach ($warehouses as $warehouse) {
                        $inventory = Inventory::where(['product_id' => $value->id, 'warehouse_id' => $warehouse->id])->first();
                        if($inventory){
                            $onHand = $inventory->on_hand;
                        }
                        else{
                            $onHand = 0;
                        }

                        $temp .= $warehouse->name . ': ' .$onHand . '</br>';

                        if($onHand >= $value->pivot->qty){ //Chỉ hiển thị kho khi có đủ số lượng
                            $htmlInventory .= '<option value="'.$warehouse->id.'">'.$warehouse->name.'</option>';
                        }
                    }

                    $htmlOnHand .= '<td>'.$temp.'</td>';
        
                    $htmlInventory .= '</select></td>';
                }

                $exportDetail = ProductExportDetail::where('order_detail_id', $value->pivot->id)->first();
                $checkExportStatus = $exportDetail ? 1 : 0;
                $detailInventoryStatus = renderOrderInventoryStatusLabel($checkExportStatus);

                $discountText = $value->pivot->discount > 0 ? $value->pivot->discount.'%' : '';

                $html .= '<tr>
                    <td class="text-center">'.$value->code.'<br><label class="label label-'.$detailInventoryStatus[1].'">'.$detailInventoryStatus[0].'</label></td>
                    <td style="width:30%">'.$value->name.'</td>
                    '.$htmlSupplier.'
                    '.$htmlOnHand.'
                    '.$htmlInventory.'
                    <td>'.$value->pivot->qty.'</td>
                    <td>'.number_format($value->pivot->price).'</td>
                    <td>'.$discountText.'</td>
                    <td>'.number_format($value->pivot->subtotal).'</td>
                </tr>';
            }
        }

        $html .= '</tbody></table></div>';
        $html .= '<div class="form-group row"><div class="col-sm-12 text-center">';
        $html .= '<a class="btn btn-info" target="_blank" href="'.url(route('order.print', ['id' => $order->id])).'">
                        <span><i class="feather icon-printer"></i></span>
                        In đơn hàng
                    </a>';

        if($type == 'export'){
            $html .= '<a class="btn btn-danger" href="'.url(route('export.index')).'">
                        <span><i class="feather icon-chevrons-left"></i></span>
                        Hủy
                    </a>
                    <button type="submit" id="btn-export" class="btn btn-primary btn-label-left">
                        <span><i class="feather icon-save"></i></span>
                        Xuất kho
                    </button>
                ';
        }

        $html .= '</div></div></form>';

        return $html;
    }

    public function renderDatatable($table, $exportOrderId = ''){
        $data = Datatables::of($table)
            ->editColumn('code', function ($row) {
                $html = '<p><a href="#" class="text-info btn-detail-order" data-id="'.$row->id.'" data-code="'.$row->code.'" data-toggle="modal" data-target="#modal-order-detail">'.$row->code.'</a></p>';
                if($row->status == 6){
                    $html .= '<p><span class="label label-warning">'.optional($row->shipping)->code.'</span></p>';
                    $html .= '<p><span class="label label-success">Giao hàng tiết kiệm</span></p>';
                }

                return $html;
            })
            ->editColumn('info', function ($row) {
                $name = '<div>';
                $name .= '<div class="text-left">Nhân viên: '.optional($row->user)->name.'</div>';
                $name .= '<div class="text-left">Khách hàng: <a href="customers/detail/'.optional($row->customer)->id.'">'.optional($row->customer)->name.'</a></div>';
                $name .= '<div class="text-left">Ngày tạo: '.Carbon::parse($row->created_at)->format('d/m/Y h:i').'</div>';
                // $name .= '<div class="text-left">Ngày bán: '.Carbon::parse($row->sell_date)->format('d/m/Y').'</div>';
                $name .= '<div class="text-left">Ngày chăm sóc: '.Carbon::parse($row->customer_date)->format('d/m/Y').'</div>';
                $name .= '<div class="text-left">Nguồn: '.renderOrderSource($row->source_id).'</div>';
                $name .= '<div class="text-left">Thanh toán: <label class="label label-info">'.renderPaymentMethod($row->payment_method).'</label</div>';
                $name .= '</div>';

                return $name;

            })
            ->editColumn('status', function ($row) {
                $html = '<div>'.$this->renderOptionStatus($row).'</div>';
                
                if(isLeader() && $row->status == Order::FINISH){
                    $html .= '<hr>';
                    $html .= '<div class="mt-2"><span class="text-danger">Admin xác nhận</span>'.$this->renderOptionAdminStatus($row).'</div>';
                }

                return $html;
            })
            ->editColumn('total_qty', function ($row) {
                $inventoryStatus = renderOrderInventoryStatusLabel($row->inventory_status);
                $html = '<div>'.$row->total_quantity.'</div>';
                $html .= '<div class="mb-3"><label class="label label-'.$inventoryStatus[1].'">'.$inventoryStatus[0].'</label></div>';
                return $html;
            })
            ->editColumn('total_money', function ($row) {
                return format_price($row->total_money) . ' đ';
            })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";
                if($row->status == 0 && $user->can('add_product_exports')){ // Quyền xuất đơn hàng
                    $action .= '<a class="btn btn-primary" href="'.url('admin/exports/create?order_id='.$row->id).'" title="Xuất hàng">
                                <i class="feather icon-log-out"></i></a>';
                }
                if($row->status == 0 && $user->can('add_shippings')){
                    $action .= '<a href="'.url('admin/shippings/order/'.$row->id).'" class="btn btn-info" title="Vận chuyển">
                        <i class="feather icon-navigation"></i>
                    </a>';
                }
                if($user->can('delete_orders')){
                    $action .= '<a href="'.url('admin/orders/delete/'.$row->id).'" class="btn btn-danger notify-confirm" title="Xóa">
                        <i class="feather icon-trash-2"></i>
                    </a>';
                }
                $action .= '<a class="btn btn-warning" target="_blank" href="'.url(route('order.print', ['id' => $row->id])).'"><i class="feather icon-printer" title="In đơn hàng"></i></a>';
                $action .= '<a class="btn btn-success btn-detail-order" href="#" data-id="'.$row->id.'" data-code="'.$row->code.'" data-toggle="modal" data-target="#modal-order-detail"><i class="feather icon-eye" title="Xem"></i></a>';

                return $action;
            })
            ->rawColumns(['code', 'info', 'total_qty', 'sale', 'status', 'action'])
            ->make(true);
        return $data;
    }

    public function renderDatatableRevenue($table){
        $data = Datatables::of($table)
            ->editColumn('code', function ($row) {
                $html = '<p><a href="#" class="text-info btn-detail-order" data-id="'.$row->id.'" data-code="'.$row->code.'" data-toggle="modal" data-target="#modal-order-detail">'.$row->code.'</a></p>';
                return $html;
            })
            ->editColumn('sell_date', function ($row) {
                return Carbon::parse($row->sell_date)->format('d/m/Y');
            })
            ->editColumn('saler', function ($row) {
                return optional($row->saler)->name;
            })
            ->editColumn('customer', function ($row) {
                return '<a class="text-info" target="_blank" href="'.url('admin/customers/detail/'.$row->customer_id).'">'.optional($row->customer)->name.'</a>';
            })
            ->editColumn('total_qty', function ($row) {
                return $row->total_quantity;
            })
            ->editColumn('discount', function ($row) {
                return format_price($row->discount).' đ';
            })
            ->editColumn('total_money', function ($row) {
                return format_price($row->total_money) . ' đ';
            })
            ->editColumn('lack', function ($row) {
                return format_price($row->lack) . ' đ';
            })
            ->editColumn('status', function ($row) {
                $status = renderOrderStatusLabel($row->status);
                return '<label class="label label-'.$status[1].'">'.$status[0].'</label>';
            })
            ->rawColumns(['code', 'customer', 'status'])
            ->make(true);
        return $data;
    }

    public function renderOptionStatus($order){
        $user = Auth::user();
        $disableStatus = [];

        $listStatus = [
            Order::WAITING => 'Chờ xác nhận',
            Order::EXPORTED => 'Đã xuất hàng',
            Order::SHIPPING => 'Đang vận chuyển',
            Order::FINISH => 'Hoàn thành',
            Order::FAILED => 'Thất bại',
            Order::CANCEL => 'Hủy'
        ];

        $classPrevent = '';
        if(!$user->can('edit_orders')){
            $classPrevent = BLOCKED;
        }

        //Nếu ko phải admin thì ẩn xuất hàng/đang giao hàng
        if(!isAdmin()){
            $disableStatus = [Order::EXPORTED, Order::SHIPPING];
        }

        //Nếu trạng thái đơn hàng là Hủy/Thất bại thì ko cho chỉnh sửa
        // if(in_array($order->status, [Order::FAILED, Order::CANCEL])){
        //     $classPrevent = BLOCKED;
        // }

        // Nếu trạng thái đơn hàng là Đã xuất kho/hoàn thành thì chỉ admin mới được đổi trạng thái
        if(in_array($order->status, [Order::EXPORTED, Order::FINISH])){
            if(!isAdmin()){
                $classPrevent = BLOCKED;
            }
        }

        $html = '<select class="form-control populate select2 order-status '.$classPrevent.'" data-id="'.$order->id.'" style="height:unset">';
        foreach ($listStatus as $key => $value) {
            $selected = $order->status == $key ? 'selected' : '';
            $disabled = in_array($key, $disableStatus) ? 'disabled' : '';
            $html .= '<option value="'.$key.'" '.$selected.' '.$disabled.'>'.$value.'</option>';
        }

        $html .= '</select>';

        return $html;
    }

    public function renderOptionAdminStatus($order){
        $user = Auth::user();

        $listStatus = [
            Order::ADMIN_WAITING => 'Chưa xác nhận',
            Order::ADMIN_APPROVE => 'Đã xác nhận',
        ];

        $classPrevent = '';
        if(!isLeader()){
            $classPrevent = BLOCKED;
        }

        $html = '<select class="form-control populate select2 order-admin-status '.$classPrevent.'" data-id="'.$order->id.'" style="height:unset">';
        foreach ($listStatus as $key => $value) {
            $selected = $order->admin_status == $key ? 'selected' : '';
            $html .= '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
        }

        $html .= '</select>';

        return $html;
    }

    public function calculatorAddPoints($products){
        $settingCategories = explode(',', setting('config_not_points_categories'));
        $settingBrands = explode(',', setting('config_not_points_brand'));
        $settingProducts = explode(',', setting('config_not_points_products'));

        $listProductNonPoints = [];
        if($settingCategories){
            $productLists = Category::whereIn('id', $settingCategories)->with('products')->get()->toArray();
            $productIds = data_get($productLists, '*.products.*.id');
            if($productIds){
                $listProductNonPoints = $productIds;
            }
        }

        if($settingBrands){
            $productLists = Brand::whereIn('id', $settingBrands)->with('products')->get()->toArray();
            $BrandProductIds = data_get($productLists, '*.products.*.id');
            if($productIds){
                $listProductNonPoints = array_merge($listProductNonPoints, $BrandProductIds);
            }
        }

        if($settingProducts){
            $productIds = array_map('intval', $settingProducts);
            if($productIds){
                $listProductNonPoints = array_merge($listProductNonPoints, $productIds);
            }
        }
        
        $orderDetail = $products->detail;

        $totalPriceAllowPoints = 0;
        $productAllowPoints = [];
        foreach ($orderDetail as $detail) {
            $productId = $detail->pivot->product_id;
            $productName = $detail->name;
            $productPrice = $detail->pivot->price;
            $productQty = $detail->pivot->qty;
            if(!in_array($productId, $listProductNonPoints)){
                $totalPrice = $productPrice * $productQty;
                $totalPriceAllowPoints += $totalPrice;
                $productAllowPoints[] = [
                    'product_id' => $productId,
                    'product_name' => $productName,
                    'product_price' => $productPrice,
                    'product_qty' => $productQty,
                    'points' => calCustomerPoints($totalPrice),
                ];
            }
        }

        return [
            'totalPrice' => $totalPriceAllowPoints,
            'productAllowPoints' => $productAllowPoints,
        ];
    }

    //Lấy KH mới - là KH chỉ có đơn hàng duy nhất
    public function getNewCustomerIds(){
        return DB::table('orders')
                    ->select('customer_id', DB::raw('count(customer_id) as count'))
                    ->whereNull('deleted_at')
                    ->groupBy('customer_id')
                    ->having(DB::raw('count'), 1)
                    ->get()
                    ->pluck('customer_id')
                    ->toArray();
    }

}