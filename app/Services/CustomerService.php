<?php 

namespace App\Services;

use Auth;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Customer;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class CustomerService
{
    public function getCustomerCode(){
        $customer = DB::table('customers')->orderBy('id', 'DESC')->first();
        $code = $customer->customer_code;
        $maxCode = (int)(str_replace('CX', '', $code)) + 1;

        if ($maxCode < 10)
            return 'CX000000' . ($maxCode);
        else if ($maxCode < 100)
            return 'CX00000' . ($maxCode);
        else if ($maxCode < 1000)
            return 'CX0000' . ($maxCode);
        else if ($maxCode < 10000)
            return 'CX000' . ($maxCode);
        else if ($maxCode < 100000)
            return 'CX00' . ($maxCode);
        else if ($maxCode < 1000000)
            return 'CX0' . ($maxCode);
        else if ($maxCode < 10000000)
            return 'CX' . ($maxCode);
    }

    public function renderDatatable($table){
        $data = Datatables::of($table)
            ->editColumn('customer_code', function ($row) {
                return '<a class="text-primary" href="'.url('/admin/customers/detail/'.$row->id).'">'.$row->customer_code.'</a>';
            })
            ->editColumn('name', function ($row) {
                $level = calCustomerLevel($row->total_money);
                $groupHtml = getCustomerGroup($row->customer_group);

                $name = '<div>';
                $name .= '<a class="text-primary" href="'.url('/admin/customers/detail/'.$row->id).'">'.$row->name.'</a>';
                $name .= '<div><span>Điểm tích lũy: '.$row->points.'</span></div>';
                $name .= '<div><span>Tổng đơn hàng: '.$row->total_order.'</span></div>';
                $name .= '<div><span>Loại khách hàng: '.$groupHtml.'</span></div>';
                $name .= '<div><span>Loại thành viên: <span class="label '.$level['class'].'">'.$level['level'].'</span></span></div>';
                $name .= '</div>';

                return $name;

            })
            ->editColumn('total_money', function ($row) {
                return number_format($row->total_money);
            })
            ->editColumn('customer_date', function ($row) {
                $html = '<div>'.Carbon::parse($row->joindate)->format('d/m/Y').'</div>';
                $html .= '<div>'.Carbon::parse($row->customer_date)->format('H:i - d/m/Y').'</div>';
                return $html;
            })
            ->editColumn('saler_id', function ($row) {
                return optional($row->saler)->name;
            })
            ->addColumn('sale', function ($row) {
                $action = "";
                $action .= '<a class="btn btn-primary text-nowrap" href="'.url('admin/orders/create?customer_id='.$row->id).'" title="Đặt hàng"><i class="feather icon-shopping-cart"></i> Đặt hàng</a>';

                return $action;
            })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";
                if($user->can('edit_customers')){
                    $action .= '<a class="btn btn-primary" href="'.url('admin/customers/edit/'.$row->id).'" title="Chỉnh sửa">
                                <i class="feather icon-edit-1"></i></a>';
                }
                if($user->can('delete_customers')){
                    $action .= '<a href="'.url('admin/customers/delete/'.$row->id).'" class="btn btn-danger notify-confirm" title="Xóa">
                        <i class="feather icon-trash-2"></i>
                    </a>';
                }
                $action .= '<a class="btn btn-success" href="'.url('admin/customers/detail/'.$row->id).'" target="_blank"><i class="feather icon-eye" title="Xem"></i></a>';

                return $action;
            })
            ->rawColumns(['customer_code', 'customer_date', 'name', 'sale', 'action'])
            ->make(true);
        return $data;
    }

    public function buildQueryCheckRole($query){
        //Kiểm tra quyền Đại lý/Khách lẻ
        if(!isRoleAgency()){ //Nếu ko có quyền QL đại lý
            $query->withHas('customer', function($q){
                $q->where('customer_group', '!=', Customer::GROUP_AGENCY);
            });
        }

        if(!isRoleCustomer()){ //Nếu ko có quyền QL KH lẻ
            $query->withHas('customer', function($q){
                $q->where('customer_group', '!=', Customer::GROUP_SINGLE);
            });
        }

        if(!isRoleCustomerSale()){ //Nếu ko có quyền QL KH nhân viên sale
            $query->withHas('customer', function($q){
                $q->where('customer_group', '!=', Customer::GROUP_SALE);
            });
        }

        return $query;
    }

    public function buildQueryFilterCustomerLevel($query, $level){
        if($level == 'non-member'){
            $query->where('total_money', '<', [Customer::TYPE_NON_MEMBER]);
        }
        elseif($level == 'member'){
            $query->where('total_money', '>=', [Customer::TYPE_NON_MEMBER]);
            $query->where('total_money', '<', [Customer::TYPE_MEMBER]);
        }
        elseif($level == 'silver'){
            $query->where('total_money', '>=', [Customer::TYPE_MEMBER]);
            $query->where('total_money', '<', [Customer::TYPE_SILVER]);
        }
        elseif($level == 'golden'){
            $query->where('total_money', '>=', [Customer::TYPE_SILVER]);
            $query->where('total_money', '<', [Customer::TYPE_GOLDEN]);
        }
        elseif($level == 'platinum'){
            $query->where('total_money', '>=', [Customer::TYPE_GOLDEN]);
            $query->where('total_money', '<', [Customer::TYPE_PLATINUM]);
        }
        elseif($level == 'diamond'){
            $query->where('total_money', '>=', [Customer::TYPE_PLATINUM]);
        }

        return $query;
    }

    public function buildQuerySortCustomer($query, $level){
        if($level == 'money_min'){
            $query->orderBy('total_money', 'ASC');
        }
        elseif($level == 'money_max'){
            $query->orderBy('total_money', 'DESC');
        }
        elseif($level == 'total_min'){
            $query->orderBy('total_order', 'ASC');
        }
        elseif($level == 'total_max'){
            $query->orderBy('total_order', 'DESC');
        }

        return $query;
    }

    public function buildQueryFilterCustomerTotalOrderMoney($query, $priceFrom = 0, $priceTo = 0){
        $priceFromTemp = $priceFrom ?: 0;
        $priceToTemp = $priceTo ?: 0;
        $priceFromFinal = str_replace(',', '', $priceFromTemp);
        $priceToFinal = str_replace(',', '', $priceToTemp);

        return $query->where('total_money', '>=', [$priceFromFinal])
                    ->where('total_money', '<=', [$priceToFinal]);
    }

    public function cronJobCalculateOrder(){
        $result = Order::selectRaw("count(store_id) as quantity, count(customer_id) as count, customer_id,
                SUM(total_money) as total_money, 
                SUM(lack) as lack, 
                SUM(total_quantity) as total_quantity")
                ->where('status', 1)
                // ->where('admin_status', 1)
                ->groupBy('customer_id')
                ->get();

        $count = 0;
        foreach ($result as $value) {
            if($value->total_money == 0){
                continue;
            }
            
            Customer::where('id', $value['customer_id'])->update([
                'total_money' => $value->total_money,
                'total_order' => $value->count,
                'total_lack' => $value->lack,
            ]);

            $count++;
        }

        return $count;
    }

    public function getPrefix($customerGroup){
        $prefix = '';
        if($customerGroup == Customer::GROUP_AGENCY){
            $prefix = 'agency';
        }
        elseif($customerGroup == Customer::GROUP_SALE){
            $prefix = 'saler';
        }

        return $prefix;
    }
}