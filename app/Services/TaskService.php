<?php 

namespace App\Services;
use Carbon\Carbon;
use App\Models\Task;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TaskService
{
    public function getTaskCode(){
        $prefix = 'CV';
        $task = DB::table('tasks')->orderBy('id', 'DESC')->first();
        $code = $task->task_code;
        $maxCode = (int)(str_replace($prefix, '', $code)) + 1;

        if ($maxCode < 10)
            return $prefix.'000000' . ($maxCode);
        else if ($maxCode < 100)
            return $prefix.'00000' . ($maxCode);
        else if ($maxCode < 1000)
            return $prefix.'0000' . ($maxCode);
        else if ($maxCode < 10000)
            return $prefix.'000' . ($maxCode);
        else if ($maxCode < 100000)
            return $prefix.'00' . ($maxCode);
        else if ($maxCode < 1000000)
            return $prefix.'0' . ($maxCode);
        else if ($maxCode < 10000000)
            return $prefix . ($maxCode);
    }

    public function renderDatatable($table){
        $data = Datatables::of($table)
            ->editColumn('task_name', function ($row) {
                $taskType = renderTaskType($row->task_type);
                $name = '<div>';
                $name .= '<div class="mt-1"><span class="label label-'.$taskType[1].'">'.$taskType[0].'</span>' .$row->task_name.'</div>';
                $name .= '<div class="mt-1">'.Carbon::parse($row->date_start)->format('d/m/Y').' - '.Carbon::parse($row->date_end)->format('d/m/Y').'</div>';
                $name .= '</div>';

                return $name;

            })
            ->editColumn('priority', function ($row) {
                $taskPriority = renderTaskPriority($row->priority);
                return '<span class="label label-'.$taskPriority[1].'">'.$taskPriority[0].'</span>';
            })
            ->editColumn('assign_by', function ($row) {
                return optional($row->assignBy)->name;
            })
            ->editColumn('created_by', function ($row) {
                return optional($row->createdBy)->name;
            })
            ->editColumn('status', function ($row) {
                $status = renderTaskStatus($row->status);
                return '<span class="label label-'.$status[1].'">'.$status[0].'</span>';
            })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";
                if($user->can('edit_tasks')){
                    $action .= '<a class="btn btn-primary show-edit-task" data-id="'.$row->id.'" href="#" title="Chỉnh sửa">
                                <i class="feather icon-edit-1"></i></a>';
                }
                if($user->can('delete_tasks')){
                    $action .= '<a href="'.url('admin/tasks/delete/'.$row->id).'" class="btn btn-danger notify-confirm" title="Xóa">
                        <i class="feather icon-trash-2"></i>
                    </a>';
                }

                // $action .= '<a class="btn btn-success" href="'.url('admin/tasks/detail/'.$row->id).'" target="_blank"><i class="feather icon-eye" title="Xem"></i></a>';

                return $action;
            })
            ->rawColumns(['task_name', 'priority', 'status', 'action'])
            ->make(true);
        return $data;
    }
}