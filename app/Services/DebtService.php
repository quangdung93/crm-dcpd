<?php 

namespace App\Services;

use Auth;
use Cache;
use Carbon\Carbon;
use App\Models\Debt;
use App\Models\Order;
use App\Models\Store;
use App\Models\Customer;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class DebtService
{

    public function getCode(){
        $order = DB::table('debts')->orderBy('id', 'DESC')->first();
        $prefix = 'N';

        if(!$order){
            return $prefix.'0000001';
        }

        $orderCode = $order->code;
        $maxCode = (int)(str_replace($prefix, '', $orderCode)) + 1;

        if ($maxCode < 10)
            return $prefix .'000000' . ($maxCode);
        else if ($maxCode < 100)
            return $prefix .'00000' . ($maxCode);
        else if ($maxCode < 1000)
            return $prefix .'0000' . ($maxCode);
        else if ($maxCode < 10000)
            return $prefix .'000' . ($maxCode);
        else if ($maxCode < 100000)
            return $prefix .'00' . ($maxCode);
        else if ($maxCode < 1000000)
            return $prefix .'0' . ($maxCode);
        else if ($maxCode < 10000000)
            return $prefix . ($maxCode);
    }

    public function buildQueryCustomerDebt($request){
        $query = DB::table('debts')->select(
            'debts.id',
            'customer_code',
            'customers.id as customer_id',
            'customers.name as customer_name',
            'customer_group',
            'customers.total_lack',
            'orders.code as order_code',
            'debt',
            'debts.created_at',
            'debts.status'
        );

        $query->leftJoin('customers', 'customers.id', '=', 'debts.object_id');
        $query->leftJoin('orders', 'orders.id', '=', 'debts.document_id');

        if(!isRoleAgency()){ //Nếu ko có quyền QL đại lý
            $query->where('customers.customer_group', '!=', Customer::GROUP_AGENCY);
        }

        if(!isRoleCustomer()){ //Nếu ko có quyền QL KH lẻ
            $query->where('customers.customer_group', '!=', Customer::GROUP_SINGLE);
        }

        if(!isRoleCustomerSale()){ //Nếu ko có quyển QL KH là nhân viên sale
            $query->where('customers.customer_group', '!=', Customer::GROUP_SALE);
        }

        if($request->date_from){
            $query->where('debts.created_at', '>=', format_date($request->date_from, 'Y-m-d 00:00:00'));
        }
        else{
            $query->where('debts.created_at', '>=', format_date(Carbon::now()->startOfMonth(), 'Y-m-d 00:00:00'));
        }

        if($request->date_to){
            $query->where('debts.created_at', '<=', format_date($request->date_to, 'Y-m-d 23:59:59'));
        }
        else{
            $query->where('debts.created_at', '<=', format_date(Carbon::now()->endOfMonth(), 'Y-m-d 23:59:59'));
        }

        if($request->customer_id && $request->agency_id){
            $query->where('customers.id', $request->customer_id);
            $query->orWhere('customers.id', $request->agency_id);
        }
        else{
            if($request->customer_id){
                $query->where('customers.id', $request->customer_id);
            }
    
            if($request->agency_id){
                $query->where('customers.id', $request->agency_id);
            }

            if($request->customer_sale_id){
                $query->where('customers.id', $request->customer_sale_id);
            }
        }

        //Lấy DSKH theo quyền user
        $userIds = (new UserService)->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('debts.created_by', $userIds);
        }

        if($request->status){
            $query->where('debts.status', $request->status);
        }

        $user = Auth::user();
        $activeStoreId = $user->store_active;
        if($activeStoreId){
            $query->where('debts.store_id', (int)$activeStoreId);
        }
        else{
            $query->where('debts.store_id', Store::getDefaultStore());
        }

        $query->where('debts.type', 'customer')->orderBy('debts.id', 'DESC');

        return $query;
    }

    public function renderDatatableCustomerDebt($table){
        $data = Datatables::of($table)
            ->editColumn('code', function ($row) {
                return '<a href="'.url('admin/customers/detail/'.$row->customer_id).'" class="text-info">'.$row->customer_code.'</a>';
            })
            ->editColumn('order_code', function ($row) {
                return '<a href="#" class="text-info">'.$row->order_code.'</a>';
            })
            ->editColumn('customer', function ($row) {
                return '<a href="'.url('admin/customers/detail/'.$row->customer_id).'" class="text-info">'.$row->customer_name.'</a>';
            })
            // ->editColumn('customer_group', function ($row) {
            //     return getCustomerGroup($row->customer_group);
            // })
            ->editColumn('debt', function ($row) {
                return format_price($row->debt) . ' đ';
            })
            ->editColumn('created_at', function ($row) {
                return format_datetime($row->created_at);
            })
            ->editColumn('status', function ($row) {
                $status = renderDebtStatus($row->status);
                return '<label class="label label-'.$status[1].'">'.$status[0].'</label>';
            })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";

                if($row->status == Debt::UNPAID && $user->can('add_receipts')){
                    $action .= '<a href="'.url('admin/receipts/create/'.$row->id).'" class="btn btn-info" title="Tạo phiếu thu">
                        <i class="feather icon-navigation"></i>
                    </a>';
                }

                if($row->status == Debt::UNPAID){
                    $action .= '<a href="'.url('admin/debts/print/'.$row->customer_id).'" target="_blank" class="btn btn-warning" title="In Công nợ">
                        <i class="feather icon-printer"></i>
                    </a>';
                }
                else{
                    $action .= '<a href="'.url('admin/debts/print/'.$row->customer_id).'" target="_blank" class="btn btn-success" title="In phiếu thu">
                        <i class="feather icon-printer"></i>
                    </a>';
                }

                return $action;
            })
            ->rawColumns(['code', 'customer', 'order_code', 'total_lack', 'status', 'action'])
            ->make(true);
        return $data;
    }
}