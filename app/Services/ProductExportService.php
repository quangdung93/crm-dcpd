<?php 

namespace App\Services;

use Auth;
use Carbon\Carbon;
use App\Models\Customer;
use App\Models\Warehouse;
use App\Scopes\StoreScope;
use App\Models\ProductExport;
use App\Models\InventoryDetail;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class ProductExportService
{
    public function getExportCode(){
        $prefix = 'PX';
        $import = DB::table('product_exports')->orderBy('id', 'DESC')->first();

        if(!$import){
            return $prefix.'0000001';
        }

        $importCode = $import->code;
        $maxCode = (int)(str_replace($prefix, '', $importCode)) + 1;

        if ($maxCode < 10)
            return $prefix.'000000' . ($maxCode);
        else if ($maxCode < 100)
            return $prefix.'00000' . ($maxCode);
        else if ($maxCode < 1000)
            return $prefix.'0000' . ($maxCode);
        else if ($maxCode < 10000)
            return $prefix.'000' . ($maxCode);
        else if ($maxCode < 100000)
            return $prefix.'00' . ($maxCode);
        else if ($maxCode < 1000000)
            return $prefix.'0' . ($maxCode);
        else if ($maxCode < 10000000)
            return $prefix.'' . ($maxCode);
    }

    public function renderExportDetail($export){
        $orderCode = optional($export->order)->code;
        $status = $export->status == ProductExport::FINISH ? '<label class="label label-success">Hoàn thành</label>' : '<label class="label label-danger">Đã hủy</label>';

        $html = '
        <div class="row">
        <div class="col-sm-6">
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Đơn hàng:</label>
                <div class="col-sm-8 col-form-label text-primary">'.$orderCode.'</div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Khách hàng:</label>
                <div class="col-sm-8 col-form-label text-primary">'.optional(optional($export->order)->customer)->name.'</div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Ghi chú:</label>
                <div class="col-sm-8 col-form-label">'.$export->notes.'</div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Ngày xuất:</label>
                <div class="col-sm-8 col-form-label">'.format_date($export->import_date).'</div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Ngày tạo:</label>
                <div class="col-sm-8 col-form-label">'.format_date($export->created_at).'</div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Trạng thái:</label>
                <div class="col-sm-8 col-form-label text-primary">'.$status.'</div>
            </div>
        </div>
        <div class="card-datatable table-responsive"><table class="table-order-detail table" width="100%">
        <thead class="thead-light">
            <tr class="bg-primary text-white">
            <td>Mã sản phẩm</td>
            <td>Mã code kho</td>
            <td>Tên sản phẩm</td>
            <td>Kho xuất</td>
            <td>Số lượng</td>
            <td>Giá bán</td>
            <td>Thành tiền</td>
            </tr>
        </thead><tbody>';

        if($export && $export->detail){
            foreach ($export->detail as $key => $value) {
                $inventoryDetail = InventoryDetail::where([
                    'product_id' => $value->pivot->product_id,
                    'order_code' => $orderCode,
                ])->get();

                if($inventoryDetail){
                    $inventoryDetail = $inventoryDetail->toArray();
                    $htmlInventory = '';
                    foreach ($inventoryDetail as $key => $inventory) {
                        $htmlInventory .= '<div class="text-center"><img width="50" src="'.asset_image('inventory_code/'.$inventory['code_file']).'"/><div>'.$inventory['product_code'].'</div></div>';
                    }
                }

                $warehouse = Warehouse::where('id', $value->pivot->warehouse_id)->first();
                $warehouseName = $warehouse ? $warehouse->name : '';

                $html .= '<tr>
                    <td>'.$value->code.'</td>
                    <td>'.$htmlInventory.'</td>
                    <td style="width:30%">'.$value->name.'</td>
                    <td>'.$warehouseName.'</td>
                    <td>'.$value->pivot->qty.'</td>
                    <td>'.number_format($value->pivot->price).'</td>
                    <td>'.number_format($value->pivot->subtotal).'</td>
                </tr>';
            }
        }

        $html .= '</tbody></table></div>';

        return $html;
    }

    public function renderDatatable($table){
        $data = Datatables::of($table)
            ->editColumn('code', function ($row) {
                return '<a href="#" class="text-warning btn-detail-export" data-id="'.$row->id.'" data-code="'.$row->code.'" data-toggle="modal" data-target="#modal-export-detail">'.$row->code.'</a>';
            })
            ->editColumn('order_id', function ($row) {
                $orderId = optional($row->order)->id;
                $orderCode = optional($row->order)->code;
                return $orderId ? '<a href="#" class="text-info btn-detail-order" data-id="'.$orderId.'" data-code="'.$orderCode.'" data-toggle="modal" data-target="#modal-order-detail">'.$orderCode.'</a>' : '';
            })
            ->editColumn('total', function ($row) {
                return format_price($row->total) . ' đ';
            })
            ->editColumn('export_date', function ($row) {
                return format_date($row->export_date);
            })
            ->editColumn('created_at', function ($row) {
                return format_date($row->created_at);
            })
            ->editColumn('status', function ($row) {
                return $row->status == ProductExport::FINISH ? '<label class="label label-success">Hoàn thành</label>' : '<label class="label label-danger">Đã hủy</label>';
            })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";
                if(isDeveloper()){
                    $action .= '<a href="'.url('admin/exports/delete/'.$row->id).'" class="btn btn-danger notify-confirm" title="Xóa">
                        <i class="feather icon-trash-2"></i>
                    </a>';
                }

                return $action;
            })
            ->rawColumns(['code', 'order_id', 'info', 'status', 'action'])
            ->make(true);
        return $data;
    }

    public function renderDatatableReport($table){
        $data = Datatables::of($table)
            ->editColumn('code', function ($row) {
                return '<a href="#" class="text-info" data-id="'.$row->id.'" data-code="'.$row->code.'">'.$row->code.'</a>';
            })
            ->editColumn('order_code', function ($row) {
                return '<a href="#" class="text-info">'.$row->order_code.'</a>';
            })
            ->editColumn('product_name', function ($row) {
                return '<a href="#" class="text-info">'.$row->product_name.'</a>';
            })
            ->editColumn('customer_name', function ($row) {
                $name = '<div>';
                $name .= '<div class="text-left">Kho xuất: '.$row->warehouse_name.'</div>';
                $text = getCustomerGroup($row->customer_group);
                $name .= '<div class="text-left">'.$text.': '.$row->customer_name.'</div>';
                $name .= '</div>';

                return $name;

            })
            ->editColumn('qty', function ($row) {
                return $row->qty;
            })
            ->editColumn('price', function ($row) {
                return format_price($row->price) . ' đ';
            })
            ->editColumn('export_date', function ($row) {
                return format_date($row->export_date);
            })
            ->editColumn('created_by', function ($row) {
                return $row->created_by;
            })
            ->rawColumns(['code', 'order_code', 'info', 'product_name', 'customer_name'])
            ->make(true);
        return $data;
    }
}