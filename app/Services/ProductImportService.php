<?php 

namespace App\Services;

use Auth;
use Carbon\Carbon;
use App\Scopes\StoreScope;
use App\Models\ProductImport;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class ProductImportService
{
    public function getImportCode(){
        $prefix = 'PN';
        $import = DB::table('product_imports')->orderBy('id', 'DESC')->first();

        if(!$import){
            return $prefix.'0000001';
        }

        $importCode = $import->code;
        $maxCode = (int)(str_replace($prefix, '', $importCode)) + 1;

        if ($maxCode < 10)
            return $prefix.'000000' . ($maxCode);
        else if ($maxCode < 100)
            return $prefix.'00000' . ($maxCode);
        else if ($maxCode < 1000)
            return $prefix.'0000' . ($maxCode);
        else if ($maxCode < 10000)
            return $prefix.'000' . ($maxCode);
        else if ($maxCode < 100000)
            return $prefix.'00' . ($maxCode);
        else if ($maxCode < 1000000)
            return $prefix.'0' . ($maxCode);
        else if ($maxCode < 10000000)
            return $prefix.'' . ($maxCode);
    }

    public function renderImportDetail($import){
        $html = '
        <div class="row">
        <div class="col-sm-6">
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Kho nhập:</label>
                <div class="col-sm-8 col-form-label text-primary">'.$import->warehouse->name.'</div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Nhà phân phối:</label>
                <div class="col-sm-8 col-form-label text-primary">'.$import->supplier->name.'</div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Tổng tiền:</label>
                <div class="col-sm-8 col-form-label text-primary">'.number_format($import->total).' đ</div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Ngày nhập:</label>
                <div class="col-sm-8 col-form-label text-primary">'.format_date($import->import_date).'</div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Ngày tạo:</label>
                <div class="col-sm-8 col-form-label text-primary">'.format_date($import->created_at).'</div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Ghi chú:</label>
                <div class="col-sm-8 col-form-label text-primary">'.$import->notes.'</div>
            </div>
        </div>
        <div class="card-datatable table-responsive"><table class="table-order-detail table" width="100%">
        <thead class="thead-light">
            <tr class="bg-primary text-white">
            <td>Mã sản phẩm</td>
            <td>Tên sản phẩm</td>
            <td>Số lượng</td>
            <td>Giá bán</td>
            <td>Thành tiền</td>
            </tr>
        </thead><tbody>';

        if($import && $import->detail){
            foreach ($import->detail as $key => $value) {
                $html .= '<tr>
                    <td>'.$value->code.'</td>
                    <td style="width:30%">'.$value->name.'</td>
                    <td>'.$value->pivot->qty.'</td>
                    <td>'.number_format($value->pivot->price).'</td>
                    <td>'.number_format($value->pivot->subtotal).'</td>
                </tr>';
            }
        }

        $html .= '</tbody></table></div>';

        return $html;
    }

    public function renderDatatable($table){
        $data = Datatables::of($table)
            ->editColumn('code', function ($row) {
                return '<a href="#" class="text-info btn-detail-order" data-id="'.$row->id.'" data-code="'.$row->code.'" data-toggle="modal" data-target="#modal-order-detail">'.$row->code.'</a>';
            })
            ->editColumn('info', function ($row) {
                $name = '<div>';
                $name .= '<div class="text-left">Kho nhập: '.optional($row->warehouse)->name.'</div>';
                $name .= '<div class="text-left">Nhà phân phối: '.optional($row->supplier)->name.'</div>';
                $name .= '</div>';

                return $name;

            })
            ->editColumn('total', function ($row) {
                return format_price($row->total) . ' đ';
            })
            ->editColumn('import_date', function ($row) {
                return format_date($row->import_date);
            })
            ->editColumn('created_at', function ($row) {
                return format_date($row->created_at);
            })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";
                // if($user->can('edit_customers')){
                //     $action .= '<a class="btn btn-primary" href="'.url('admin/customers/edit/'.$row->id).'" title="Chỉnh sửa">
                //                 <i class="feather icon-edit-1"></i></a>';
                // }
                if($row->id != 1 && isDeveloper()){
                    $action .= '<a href="'.url('admin/imports/delete/'.$row->id).'" class="btn btn-danger notify-confirm" title="Xóa">
                        <i class="feather icon-trash-2"></i>
                    </a>';
                }
                $action .= '<a class="btn btn-success btn-detail-order" href="#" data-id="'.$row->id.'" data-code="'.$row->code.'" data-toggle="modal" data-target="#modal-order-detail"><i class="feather icon-eye" title="Xem"></i></a>';

                return $action;
            })
            ->rawColumns(['code', 'info', 'action'])
            ->make(true);
        return $data;
    }

    public function renderDatatableReport($table){
        $data = Datatables::of($table)
            ->editColumn('code', function ($row) {
                return '<a href="#" class="text-info" data-id="'.$row->id.'" data-code="'.$row->code.'">'.$row->code.'</a>';
            })
            ->editColumn('product_name', function ($row) {
                return '<a href="#" class="text-info">'.$row->product_name.'</a>';
            })
            ->editColumn('info', function ($row) {
                $name = '<div>';
                $name .= '<div class="text-left">Kho nhập: '.$row->warehouse_name.'</div>';
                $name .= '<div class="text-left">Nhà phân phối: '.$row->supplier_name.'</div>';
                $name .= '</div>';

                return $name;

            })
            ->editColumn('qty', function ($row) {
                return $row->qty;
            })
            ->editColumn('price', function ($row) {
                return format_price($row->price) . ' đ';
            })
            ->editColumn('import_date', function ($row) {
                return format_date($row->import_date);
            })
            ->editColumn('created_by', function ($row) {
                return $row->created_by;
            })
            ->rawColumns(['code', 'info', 'product_name'])
            ->make(true);
        return $data;
    }
}