<?php

namespace App\Providers;

use App\Models\Page;
use App\Models\Post;
use App\Models\Brand;
use App\Models\Order;
use App\Models\Store;
use App\Models\Product;
use App\Models\Category;
use App\Models\Supplier;
use App\Models\Warehouse;
use App\Models\PostCategory;
use App\Models\ProductExport;
use App\Models\ProductImport;
use App\Observers\OrderObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Order::observe(OrderObserver::class);

        Relation::morphMap([
            'post' => Post::class,
            'product' => Product::class,
            'order' => Order::class,
            'page' => Page::class,
            'category' => Category::class,
            'brand' => Brand::class,
            'postcategory' => PostCategory::class,
            'productimport' => ProductImport::class,
            'productexport' => ProductExport::class,
            'warehouse' => Warehouse::class,
            'store' => Store::class,
            'supplier' => Supplier::class,
        ]);
    }
}
