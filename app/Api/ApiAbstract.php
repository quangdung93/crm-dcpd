<?php
namespace App\Api;

use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Models\ZaloApp;
use Illuminate\Support\Facades\Log;
use App\Exceptions\ApiHandleException;

abstract class ApiAbstract
{
    const LOG_SUCCESS = true;

    protected function callApi($method = "POST", $url, $data = []){
        try {
            $params['headers'] = [
                'Token' => env('GHTK_API_KEY'),
                'Content-Type' => 'application/json',
            ];
    
            if(!empty($data)){
                $params['json'] = $data;
            }
    
            $client = $this->_getClient();
    
            $response = $client->request($method, $url, $params);
        }
        catch (GuzzleException $ex) {
            // Guzzle Error
            logger('[API] GuzzleException Failed:', ['message' => $ex->getMessage()], 'error');
            throw new ApiHandleException(ApiHandleException::GUZZLE_ERROR_MESSAGE, $ex->getMessage(), ApiHandleException::GUZZLE_ERROR);
        }

        $result = json_decode($response->getBody(), true);

        if ($response->getStatusCode() == 200){
            // Success
            if ($result && $result['success']) {

                //Log success
                if(self::LOG_SUCCESS){
                    logger('[API] Success:', ['api' => $url,'request' => $data,'response' => $result]);
                }

                return $result;
            }

            // Error API
            logger('[API] Failed:', ['request' => $data,'response' => $result], 'warning');
            return $result;
        }

        // HTTP Error
        logger('[API] Failed:', ['api' => $url,'request' => $data,'response' => $result], 'error');
        throw new ApiHandleException($result['message'], "", ApiHandleException::ERROR_HTTP);
    }

    protected function _getClient(){
        return new Client([
            'http_errors' => false,
            'base_uri' => env('GHTK_API_URL')
        ]);
    }
}
