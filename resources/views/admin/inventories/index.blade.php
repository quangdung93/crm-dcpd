@extends('admin.body')
@php
    $pageName = 'Tồn kho';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="col-sm-12">
            @can('add_inventories')
                <div class="text-right mb-3">
                    <a href="{{ url('admin/imports/create') }}" class="btn btn-primary"><i
                            class="feather icon-shopping-cart"></i> Nhập hàng</a>
                </div>
            @endcan
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Tìm kiếm</h4>
                        <form id="search-inventory">
                        <div class="row">
                                <div class="col-sm-4">
                                    <input class="form-control" name="name" id="name" placeholder="Nhập tên, mã sản phẩm"/>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control populate select2" id="brand-id" name="brand_id">
                                        <option value="0">Chọn thương hiệu</option>
                                        @if($brands)
                                            @foreach($brands as $brand)
                                                <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control populate select2" id="warehouse-id" name="warehouse_id">
                                        <option value="0">Chọn kho</option>
                                        @if($warehouses)
                                            @foreach($warehouses as $warehouse)
                                                <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-success mt-1">
                                        <span><i class="feather icon-search"></i></span>
                                        Tìm kiếm
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="dashbroad mb-4" id="report-total"></div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            <table id="datatable" class="table stableweb-table center w100">
                                <thead>
                                    <tr>
                                        <th>Mã SP</th>
                                        <th>Sản phẩm</th>
                                        <th>Đơn giá</th>
                                        <th>Tồn kho</th>
                                        <th>Ngày nhập</th>
                                        <th>Ngày hết hạn</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        const ajax_url = "{!! route('inventory.view') !!}";
        const REPORT_INVENTORY_URL = "{!! route('inventory.analytic') !!}";
        var columns = [
            { data: 'product_code',name: 'product_code',orderable: false, searchable: false},
            { data: 'product_name',name: 'product_name', width: '30%'},
            { data: 'price',name: 'price'},
            { data: 'on_hand',name: 'on_hand', width: '20%', className: "text-left"},
            { data: 'import_date',name: 'import_date'},
            { data: 'expires_date',name: 'expires_date'},
        ];

        loadReport();

        showDataTableServerSide($('#datatable'), ajax_url, columns);

        const URL_LIST = "{!! route('inventory.list') !!}";
        $(document).on('submit', '#search-inventory', function(e){
            e.preventDefault();

            let self = $(this);
            let url = `${URL_LIST}?${$(this).serialize()}`;
            loadReport();
            showDataTableServerSide($('#datatable'), url, columns);
        });

        function loadReport(){
            const productName = $('#name').val();
            const brandId = $('#brand-id').find(':selected').val();
            const warehouseId = $('#warehouse-id').find(':selected').val();
            
            $.get(`${REPORT_INVENTORY_URL}?name=${productName}&brand_id=${brandId}&warehouse_id=${warehouseId}`, {}, function(response){
                if(response.error == 0){
                    $('#report-total').html(response.data);
                }
            });
        }
    });
</script>
@endsection