@extends('admin.body')
@php
    $pageName = 'Đơn hàng';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="panel-body">
            <form class="form-horizontal" action="{{url($routeName)}}" method="POST" role="form"
                enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Chọn sản phẩm</h4>
                                @if ($errors->has('products'))
                                    <div class="text-danger col-sm-12">{{ $errors->first('products') }}</div>
                                @endif
                                <div class="card-block">
                                    @include('admin.components.product-search', 
                                    [
                                        'inputName' => 'products[]'
                                    ])
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Thông tin {{ $pageName }}</h4>
                                <div class="form-group row mb-0">
                                    <label class="col-sm-12 col-form-label">Khách hàng:</label>
                                    <div class="col-sm-10 pr-0">
                                        @include('admin.components.customer-search', 
                                        [
                                            'inputName' => 'customer_id',
                                            'customer_id' => request()->get('customer_id')
                                        ])
                                    </div>
                                    <div class="col-sm-2 pr-0">
                                        <a href="#" data-toggle="modal" data-target="#modal-add-customer" class="btn btn-success"><i class="feather icon-plus"></i></a>
                                    </div>
                                    @if ($errors->has('customer_id'))
                                        <div class="text-danger col-sm-12">{{ $errors->first('customer_id') }}</div>
                                    @endif
                                </div>
                                {{-- <div class="form-group row">
                                    <label class="col-sm-12 col-form-label">Kho xuất:</label>
                                    <div class="col-sm-12">
                                        <select class="form-control populate select2" name="warehouse_id">
                                            @if($warehouses)
                                                @foreach($warehouses as $warehouse)
                                                    <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('warehouse_id'))
                                            <div class="text-danger mt-2">{{ $errors->first('warehouse_id') }}</div>
                                        @endif
                                    </div>
                                </div> --}}
                                <div class="form-group row">
                                    <label class="col-sm-12 col-form-label">Nhân viên bán hàng:</label>
                                    <div class="col-sm-12">
                                        <input type="hidden" class="form-control" id="store" name="store_id"/>
                                        <div class="checkout-select">
                                            <select name="saler_id" class="form-control select2">
                                                @foreach($users as $key => $value)
                                                    <option value="{{ $value->id }}" 
                                                        {{ Auth::id() == $value->id  ? 'selected' : '' }}
                                                    >
                                                        {{ $value->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <div class="error text-danger mt-2"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row {{ $customer ? '' : 'hidden' }} " id="box-customer-points">
                                    <label class="col-sm-12 col-form-label">Đổi điểm tích lũy: <span class="customer-points">{{ $customer->points ?? 0 }}</span></label>
                                    <div class="col-sm-12">
                                        <div class="checkout-select">
                                            <select id="points" name="points" class="form-control select2">
                                                <option value="0">Chọn điểm đổi</option>
                                                <option id="points-100" value="100">100 điểm</option>
                                                <option id="points-200" value="200">200 điểm</option>
                                                <option id="points-300" value="300">300 điểm</option>
                                            </select>
                                            @if ($errors->has('points'))
                                                <div class="text-danger mt-2">{{ $errors->first('points') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-12 col-form-label">Nguồn khách hàng:</label>
                                    <div class="col-sm-12">
                                        <select class="form-control populate select2" name="source_id">
                                            <option value="0">Không xác định</option>
                                            <option value="1">Website</option>
                                            <option value="2">Facebook</option>
                                            <option value="3">Hotline</option>
                                            <option value="4">Chăm sóc khách hàng</option>
                                            <option value="5">Lazada</option>
                                            <option value="6">Shopee</option>
                                            <option value="7">Tiki</option>
                                        </select>
                                        @if ($errors->has('source_id'))
                                            <div class="text-danger mt-2">{{ $errors->first('source_id') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-12 col-form-label">Ngày hẹn lại chăm sóc:</label>
                                    <div class="col-sm-12">
                                        <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' class="form-control" name="customer_date" value="{{ format_date(\Carbon\Carbon::now()) }}"/>
                                            <span class="input-group-addon bg-primary">
                                            <span class="feather icon-calendar"></span>
                                            </span>
                                        </div>
                                        @if ($errors->has('customer_date'))
                                            <div class="text-danger mt-2">{{ $errors->first('customer_date') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <x-textarea type="" title="Ghi chú" name="notes" value=""/>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Thông tin Thanh toán</h4>
                                <div class="form-group row">
                                    <label class="col-sm-12 col-form-label">Hình thức thanh toán</label>
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <div class="payment-method">
                                                <input type="radio" id="money" name="payment_method" value="1" checked> <label for="money">Tiền mặt</label>
                                            </div>
                                            <div class="payment-method">
                                                <input type="radio" id="cod" name="payment_method" value="2"> <label for="cod">COD</label>
                                            </div>
                                            <div class="payment-method">
                                                <input type="radio" id="transfer" name="payment_method" value="3"> <label for="transfer">Chuyển khoản</label>
                                            </div>
                                        </div>
                                        <div class="input-group">
                                            <div class="payment-method">
                                                <input type="radio" id="atm" name="payment_method" value="5"> <label for="atm">Cà thẻ</label>
                                            </div>
                                            <div class="payment-method">
                                                <input type="radio" id="website" name="payment_method" value="6"> <label for="website">Website</label>
                                            </div>
                                            <div class="payment-method">
                                                <input type="radio" id="has-debt" name="payment_method" value="7"> <label for="has-debt" style="color: red">Công nợ</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right">Tiền hàng</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="total-money" name="total_price" class="form-control prevent-event text-right" value="0"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right">Giảm giá</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="discount" name="coupon" class="form-control txtMoney text-right" value="0"/>
                                        <div class="alert-success mt-2 text-right" id="discount-note" role="alert"></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right">Tổng cộng</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="total-final" name="total_money" class="form-control prevent-event text-right text-danger" value="0"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right">Khách đưa</label>
                                    <div class="col-sm-8">
                                        <input id="customer-pay" type="text" name="customer_pay" class="form-control txtMoney text-right text-success" value="0"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right">Nợ</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="debt" name="lack" class="form-control prevent-event text-right" value="0"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6 text-right">
                        <a class="btn btn-danger btn-label-left f-right" href="{{url($routeName)}}">
                            <span><i class="feather icon-chevrons-left"></i></span>
                            Hủy
                        </a>
                        <button type="submit" name="submit" value="print" class="btn btn-info btn-label-left">
                            <span><i class="feather icon-save"></i></span>
                            In Đơn Hàng
                        </button>
                        <button type="submit" name="submit" value="create" class="btn btn-primary btn-label-left">
                            <span><i class="feather icon-save"></i></span>
                            Lưu
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<!-- Page-body end -->

@include('admin.modal.add-customer')

@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $('#datetimepicker1').datepicker({format: 'dd/mm/yyyy'});
        $('#has-debt').attr({disabled: true});

        $(document).on('submit', '#frm-add-customer', function(e){
            e.preventDefault();

            let self = $(this);

            if($('input[name="name"]').val() === ''){
                pushNotify('Bạn chưa nhập họ tên!', text = '', type = 'danger');
                return;
            }

            if($('input[name="phone"]').val() === ''){
                pushNotify('Bạn chưa nhập số điện thoại!', text = '', type = 'danger');
                return;
            }
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: $(this).data('action'),
                data: $(this).serialize(),
                success: function (response) {
                    if (response.error == 0) {
                        pushNotify('Tạo khách hàng thành công!');
                        $('#modal-add-customer').modal('hide');
                        self[0].reset();
                        $('.customer-search-input').attr('placeholder', response.data.name);
                        $('#customer_id').val(response.data.id);
                    }
                }
            });
        });

        setTimeout(() => {
            $('#store').val($("#store-id option:selected").val());
        }, 500);

        let isCheck = true;
        $(document).on('change', '#points', function () {
            let points = this.value;
            let customerPoints = $('.customer-points').text();
            let totalPrice = $('#total-money').val();

            if (!customerPoints) {
                return false;
            }

            if(totalPrice == 0){
                pushNotify('Bạn chưa chọn sản phẩm!', text = '', type = 'danger');

                if(isCheck){
                    isCheck = false;
                    $('#points').val(0).trigger('change');
                }

                return;
            }

            if(points > customerPoints){
                pushNotify('Điểm tích lũy không đủ!', text = '', type = 'danger');
                $('#points').val(0).trigger('change');
                return;
            }

            const exchange = {
                0: 0,
                100: 300000,
                200: 600000,
                300: 1000000
            };

            const discount = exchange[points];
            totalPrice = totalPrice.replaceAll(',', '');
            let total = totalPrice - discount;

            $('#discount').val(encodeCurrencyFormat(discount));
            $('#total-final').val(encodeCurrencyFormat(total));
            $('#customer-pay').val(encodeCurrencyFormat(total));
        });

        $('input[name="payment_method"]').on('change', function(e) {
            let totalFinal = $('#total-final').val();
            let total = totalFinal.replaceAll(',' , '');
            console.log({total});
            if($(this).val() == 7){
                if(total > 0){
                    $('#debt').val(encodeCurrencyFormat(total));
                    $('#customer-pay').val(0);
                }
            }
            else{
                $('#debt').val(0);
                $('#customer-pay').val(encodeCurrencyFormat(total));
            }
        });
    });
</script>
@endsection