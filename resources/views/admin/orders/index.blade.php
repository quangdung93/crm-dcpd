@extends('admin.body')
@php
    $pageName = 'Đơn hàng';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="col-sm-12">
            @can('add_orders')
                <div class="text-right mb-3">
                    <a href="{{url($routeName.'/create')}}" class="btn btn-primary"><i
                            class="feather icon-shopping-cart"></i> Bán hàng</a>
                    <a href="{{url($routeName.'/export')}}" class="btn btn-success"><i
                        class="feather icon-download"></i> Xuất Excel</a>
                </div>
            @endcan
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Tìm kiếm</h4>
                        <form id="ajax-search">
                        <div class="row">
                                <div class="col-sm-3">
                                    <input class="form-control" name="code" placeholder="Nhập mã đơn hàng"/>
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control populate select2" name="status">
                                        <option value="-1">Chọn trạng thái</option>
                                        <option value="0">Chờ xác nhận</option>
                                        <option value="1">Hoàn thành</option>
                                        <option value="2">Thất bại</option>
                                        <option value="3">Hủy</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control populate select2" name="source">
                                        <option value="-1">Chọn nguồn KH</option>
                                        <option value="0">Không xác định</option>
                                        <option value="1">Website</option>
                                        <option value="2">Facebook</option>
                                        <option value="3">Hotline</option>
                                        <option value="4">Chăm sóc khách hàng</option>
                                        <option value="5">Lazada</option>
                                        <option value="6">Shopee</option>
                                        <option value="7">Tiki</option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control populate select2" name="payment">
                                        <option value="-1">Chọn hình thức TT</option>
                                        <option value="0">Không xác định</option>
                                        <option value="1">Tiền mặt</option>
                                        <option value="2">COD</option>
                                        <option value="3">Chuyển khoản</option>
                                        <option value="5">Cà thẻ</option>
                                        <option value="6">Website</option>
                                        <option value="7">Công nợ</option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-success mt-1">
                                        <span><i class="feather icon-search"></i></span>
                                        Tìm kiếm
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            <table id="datatable" class="table stableweb-table center w100">
                                <thead>
                                    <tr>
                                        <th>Mã đơn</th>
                                        <th>Thông tin</th>
                                        <th>Trạng thái</th>
                                        <th>Tổng SL</th>
                                        <th>Tổng tiền</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-body end -->

    <div class="modal fade" id="modal-order-detail">
        <div class="modal-dialog" role="document" style="max-width: 80%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Chi tiết đơn hàng <span id="order-code"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        const ajax_url = "{!! route('orders.view') !!}";
        var columns = [
            { data: 'code', name: 'code', orderable: false, searchable: false},
            { data: 'info',name: 'info',width: '25%'},
            { data: 'status',name: 'status', width: '16%'},
            { data: 'total_qty',name: 'total_qty', className: 'nowrap'},
            { data: 'total_money',name: 'total_money'},
            { data: 'action',orderable: false, searchable: false, className: 'nowrap'}
        ];

        showDataTableServerSide($('#datatable'), ajax_url, columns);

        // Cập nhật trạng thái đơn hàng
        $(document).on('change', '.order-status', function(){
            let orderId = $(this).data('id');
            let statusId = $(this).find(':selected').val();

            if(!orderId || !statusId){
                return;
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: '{{ route('order.api.update') }}',
                data: {order_id: orderId, status: statusId},
                success: function (response) {
                    if (response.error == 0) {
                        pushNotify('Cập nhật đơn hàng thành công!');
                        setTimeout(() => {
                            window.location.reload(); 
                        }, 500);
                    }
                }
            });
        });

        //Cập nhật xác nhận admin
        $(document).on('change', '.order-admin-status', function(){
            let orderId = $(this).data('id');
            let statusId = $(this).find(':selected').val();

            if(!orderId || !statusId){
                return;
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: '{{ route('order.api.update') }}',
                data: {order_id: orderId, admin_status: statusId},
                success: function (response) {
                    if (response.error == 0) {
                        pushNotify('Cập nhật đơn hàng thành công!');
                        setTimeout(() => {
                            window.location.reload(); 
                        }, 500);
                    }
                }
            });
        });

        $(document).on('click', '.btn-detail-order', function(){
            let order_id = $(this).data('id');
            let order_code = $(this).data('code');

            //Reset modal
            $('#modal-order-detail .modal-body').html('');

            if(!order_id){
                return;
            }

            $.get(`${URL_MAIN}admin/orders/detail/${order_id}?type=order`, {}, function(response){
                if(response.error == 0){
                    $('#order-code').text(order_code);
                    $('#modal-order-detail .modal-body').html(response.data);
                    $('.select2-modal').each(function() { 
                        $(this).select2({ dropdownParent: $(this).parent()});
                    });
                }
            });
        });

        const URL_LIST = "{!! route('order.list') !!}";
        $(document).on('submit', '#ajax-search', function(e){
            e.preventDefault();

            let self = $(this);
            let url = `${URL_LIST}?${$(this).serialize()}`;

            showDataTableServerSide($('#datatable'), url, columns);
        });
    });
</script>
@endsection