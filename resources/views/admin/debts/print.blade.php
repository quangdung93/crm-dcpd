<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="charset=utf-8" />
    <meta charset="UTF-8">
    <title>Công nợ khách hàng</title>
    <style>
        body {
            font-family: DejaVu Sans, sans-serif;
            font-size: 13px
        }

        table, td, th {
            border: 1px solid;
        }

        table {
            border-collapse: collapse;
        }

        p{
            margin: 0;
        }

        /* tr:nth-child(odd){background-color: #f1f1f1;} */

        /* tr:nth-child(even){background-color: #e0e0e0;} */

        .logo{
            float: left;
            width: 200px;
            min-width: 200px;
        }

        .logo img{
            max-width: 100%;
            width: 200px
        }

        .info{
            float: left;
            margin-left: 40px;
            width: 100%;
        }

        .info .name{
        }
    </style>
</head>
<body>
    @php
        $companyLogo = $store->logo ?? '';
        $imageData = '';
        if($companyLogo){
            $avatarUrl = asset($companyLogo);
            $arrContextOptions = array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
            $type = @pathinfo($avatarUrl, PATHINFO_EXTENSION);
            $avatarData = @file_get_contents($avatarUrl, false, stream_context_create($arrContextOptions));
            $avatarBase64Data = @base64_encode($avatarData);
            $imageData = 'data:image/' . $type . ';base64,' . $avatarBase64Data;
        }
    @endphp
    <div class="content">
        <div class="header">
            <div class="logo">
                <img src="{{ $imageData }}" alt="" />
            </div>
            <div class="info">
                @php
                    $admin_company_name = $store->company_name ?? '';
                    $admin_address = $store->address ?? '';
                    $admin_phone = $store->phone ?? '';
                    $admin_website = $store->website ?? '';
                    $admin_email = $store->email ?? '';
                    $admin_facebook = $store->facebook ?? '';
                    $admin_zalo = $store->zalo ?? '';
                    $admin_note = $store->note ?? '';
                @endphp
                @if($admin_company_name)
                <h4 style="margin: 0; text-transform: uppercase">{{ $admin_company_name }}</h4>
                @endif

                @if($admin_address)
                <p class="name">Địa chỉ: {{ $admin_address }}</p>
                @endif

                @if($admin_phone)
                <p class="name">Điện thoại: {{ $admin_phone }}</p>
                @endif

                @if($admin_website)
                <p class="name">Website: {{ $admin_website }}</p>
                @endif

                @if($admin_email)
                <p class="name">Email: {{ $admin_email }}</p>
                @endif

                @if($admin_facebook)
                <p class="name">Facebook: {{ $admin_facebook }}</p>
                @endif
            </div>
        </div>
        <div style="clear:both"></div>
        <h2 style="text-align: center;margin: 0">Hóa Đơn Chi Tiết Khách Hàng</h2>
        <div>
            <div style="float:left; width: 40%">Tên khách hàng: {{ $customer->name }}</div>
            <div style="float:left; width: 30%">Điện thoại: {{ $customer->phone }}</div>
            <div style="float:right">Ngày: {{ format_datetime(\Carbon\Carbon::now()) }}</div>
            <div style="clear:both"></div>
            <div>Mã khách hàng: {{ $customer->customer_code }}</div>
            <div>Địa chỉ: {{ $customer->address }}</div>
        </div>

        <div class="table" style="margin-top: 20px">
            <table style="width: 100%">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Thời gian</th>
                        <th>Mã đơn hàng</th>
                        <th>Diễn giải</th>
                        <th>SL</th>
                        <th>Đơn giá</th>
                        <th>Giảm giá</th>
                        <th>Thành tiền</th>
                        <th>Ghi nợ</th>
                        <th>Ghi có</th>
                    </tr>
                </thead>
                <tbody>
                    @if($debts)
                        @php
                            $count = 0;
                            $totalPay = 0;
                        @endphp
                        @foreach($debts as $row)
                            @if($row->order)
                                @php
                                    $countSub = 0;
                                    $count++;
                                    $totalPay = $totalPay + $row->pay;
                                @endphp
                                @foreach($row->order->detail as $product)
                                    @if($countSub == 0)
                                    <tr style="border:1px solid #e0e0e0">
                                        <td style="text-align: center; width: 50px">{{ $count }}</td>
                                        <td style="text-align: left">{{ format_datetime($row->created_at) }}</td>
                                        <td style="text-align: left">{{ $row->order->code }}</td>
                                        <td style="text-align: left">Bán hàng</td>
                                        <td style="text-align: left"></td>
                                        <td style="text-align: left"></td>
                                        <td style="text-align: left"></td>
                                        <td style="text-align: center"></td>
                                        <td style="text-align: center"> {{ number_format($row->debt) }}</td>
                                        <td style="text-align: center">{{ number_format($row->pay) }}</td>
                                    </tr>
                                    @endif
                                    <tr style="border:1px solid #e0e0e0">
                                        <td style="text-align: center; width: 50px"></td>
                                        <td style="text-align: left"></td>
                                        <td style="text-align: left"></td>
                                        <td style="text-align: left">{{ $product->name }}</td>
                                        <td style="text-align: center">{{ $product->pivot->qty }}</td>
                                        <td style="text-align: center">{{ number_format($product->pivot->price) }}</td>
                                        <td style="text-align: center">{{ number_format($product->pivot->discount) }}%</td>
                                        <td style="text-align: center">{{ number_format($product->pivot->subtotal) }}</td>
                                        <td style="text-align: center"></td>
                                        <td style="text-align: center"></td>
                                    </tr>
                                    @php
                                        $countSub++;
                                    @endphp
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                </tbody>
            </table>
            <div style="text-align: right; padding: 5px">
                <span class="font-weight-bold">Tổng hóa đơn: </span>
                <span class="font-weight-bold" style="color :red;">{{ number_format($totalPay) }} đ</span>
            </div>
            {{-- <h4 style="text-align: right; text-transform: capitalize; padding: 5px;margin: 0">Bằng chữ: {{ $moneyToVietnamese }}</h4> --}}
            {{-- @if($admin_note)
                <p style="font-style: italic; margin: 0">{{ $admin_note }}</p>
            @endif --}}
            <div style="margin-top: 15px">
                <div style="float:left; text-align: center; width: 50%">Khách hàng <br> (Ký, ghi rõ họ tên)</div>
                <div style="float:left; text-align: center; width: 50%">Nhân viên <br> (Ký, ghi rõ họ tên)</div>
            </div>
        </div>
    </div>
</body>
</html>