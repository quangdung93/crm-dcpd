@extends('admin.body')
@php
    $pageName = 'Công nợ khách hàng';
    $routeName = getCurrentSlug();
@endphp
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets\icon\icofont\css\icofont.css')}}">
@endsection
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                @can('read_receipts')
                    <div class="text-right mb-3">
                        <a href="{{url(route('receipt.index'))}}" class="btn btn-primary"><i
                                class="feather icon-airplay"></i> Phiếu Thu</a>
                    </div>
                @endcan
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Bộ lọc</h4>
                        <form id="ajax-search">
                            <div class="row">
                                <div class="col-sm-3">
                                    <select class="form-control populate select2" id="status" name="status">
                                        <option value="">Chọn trang thái</option>
                                        <option value="unpaid">Chưa thanh toán</option>
                                        <option value="paid">Đã thanh toán</option>
                                        <option value="cancel">Hủy</option>
                                    </select>
                                </div>
                                @can('read_customers')
                                <div class="col-sm-3">
                                    <select class="form-control populate select2" id="customer-id" name="customer_id">
                                        <option value="0">Chọn khách hàng</option>
                                        @if($customers)
                                            @foreach ($customers as $supplier)
                                                <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                @endcan

                                @can('read_agents')
                                <div class="col-sm-3">
                                    <select class="form-control populate select2" id="agency-id" name="agency_id">
                                        <option value="0">Chọn đại lý</option>
                                        @if($agency)
                                            @foreach ($agency as $supplier)
                                                <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                @endcan

                                @can('read_customer_salers')
                                <div class="col-sm-3">
                                    <select class="form-control populate select2" id="customer-sale-id" name="customer_sale_id">
                                        <option value="0">Chọn nhân viên sale</option>
                                        @if($customerSale)
                                            @foreach ($customerSale as $sale)
                                                <option value="{{ $sale->id }}">{{ $sale->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                @endcan
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-3">
                                    <div class='input-group date datetime-pick' id="date-from-pick">
                                        <input type='text' class="form-control" id="date_from" name="date_from" value="{{ format_date(\Carbon\Carbon::now()->startOfMonth()) }}"/>
                                        <span class="input-group-addon bg-primary">
                                        <span class="feather icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class='input-group date datetime-pick' id="date-to-pick">
                                        <input type='text' class="form-control" id="date_to" name="date_to" value="{{ format_date(\Carbon\Carbon::now()->endOfMonth()) }}"/>
                                        <span class="input-group-addon bg-primary">
                                        <span class="feather icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-3 text-center mt-1">
                                    <div class="btn-group order-btn-calendar">
                                        <button type="button" id="current-week" class="btn btn-default">Tuần</button>
                                        <button type="button" id="current-month" class="btn btn-primary">Tháng</button>
                                        <button type="button" id="current-quarter" class="btn btn-default">Quý</button>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <button type="submit" class="btn btn-success mt-1">
                                        <span><i class="feather icon-search"></i></span>
                                        Xem thống kê
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="dashbroad mb-4" id="report-revenue"></div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            <table id="datatable" class="table stableweb-table center w100">
                                <thead>
                                    <tr>
                                        <th>Mã khách hàng</th>
                                        <th>Mã đơn hàng</th>
                                        <th>Tên khách hàng</th>
                                        {{-- <th>Loại khách hàng</th> --}}
                                        <th>Công nợ</th>
                                        <th>Ngày tạo</th>
                                        <th>Trạng thái</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-body end -->
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $('.datetime-pick').datepicker({ firstDay: 1, format: 'dd/mm/yyyy'});

        const ajax_url = "{!! route('debt.list') !!}";
        const REPORT_DEBT_URL = "{!! route('debt.analytic') !!}";
        var columns = [
            { data: 'code', name: 'code', orderable: false, searchable: false},
            { data: 'order_code',name: 'order_code'},
            { data: 'customer',name: 'customer'},
            // { data: 'customer_group',name: 'customer_group'},
            { data: 'debt',name: 'debt'},
            { data: 'created_at',name: 'created_at'},
            { data: 'status',name: 'status'},
            { data: 'action',name: 'action'},
        ];

        showDataTableServerSide($('#datatable'), ajax_url, columns);

        loadReportDebt();

        $(document).on('submit', '#ajax-search', function(e){
            e.preventDefault();

            let self = $(this);
            let url = `${ajax_url}?${$(this).serialize()}`;
            loadReportDebt();
            showDataTableServerSide($('#datatable'), url, columns);
        });

        function loadReportDebt(){
            const status = $('#status').find(':selected').val();
            const salerId = $('#saler-id').find(':selected').val();
            const customerId = $('#customer-id').find(':selected').val();
            const agencyId = $('#agency-id').find(':selected').val();
            const dateFrom = $('#date_from').val();
            const dateTo = $('#date_to').val();
            
            $.get(`${REPORT_DEBT_URL}?saler_id=${salerId}&status=${status}&date_from=${dateFrom}&date_to=${dateTo}&customer_id=${customerId}&agency_id=${agencyId}`, {}, function(response){
                if(response.error == 0){
                    $('#report-revenue').html(response.data);
                }
            });
        }
    });
</script>
@endsection