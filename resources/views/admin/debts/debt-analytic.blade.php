<div class="row">
    <div class="col-sm-3">
        <div class="bg-green-400 panel widget center bgimage" style="border-radius:6px; position:relative; overflow:hidden; background-image: linear-gradient(to right, #6a11cb 0%, #2575fc 100%);">
            <div style="width:100%; height:40px; display:flex; position:relative; z-index:20; justify-content: start; color:#fff;">
                <div class="relative" style="margin-left:14px; text-align:left;">
                    <p style="color:#ffffff; display:block; margin:0px; font-weight:500; font-size:20px; line-height:17px; margin-bottom:13px;">{{ $totalCustomer }}</p>
                    <p style="display:block; color:#dce9fe; text-align:left; line-height:14px; margin:0px;font-size:12px">Tổng khách hàng có công nợ</p>
                </div>
            </div>

        </div>
    </div>
    <div class="col-sm-3">
        <div class="bg-green-400 panel widget center bgimage" style="border-radius:6px; position:relative; overflow:hidden; background-image: linear-gradient(to right, #b8cbb8 0%, #b8cbb8 0%, #b465da 0%, #cf6cc9 33%, #ee609c 66%, #ee609c 100%);">
            <div style="width:100%; height:40px; display:flex; position:relative; z-index:20; justify-content: start; color:#fff;">
                <div class="relative" style="margin-left:14px; text-align:left;">
                    <p style="color:#ffffff; display:block; margin:0px; font-weight:500; font-size:20px; line-height:17px; margin-bottom:13px;">{{ number_format($totalLack)}} đ</p>
                    <p style="display:block; text-align:left; line-height:14px; margin:0px;font-size:12px">Tổng công nợ khách hàng</p>
                </div>
            </div>
        </div>
    </div>
</div>