<div class="modal fade" id="modal-add-customer">
    <div class="modal-dialog" role="document" style="max-width: 900px">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="feather icon-plus-circle text-success"></i> Tạo khách hàng <span id="order-code"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form id="frm-add-customer" class="form-validate" data-action="{{url(route('customer.api.add'))}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label class="col-sm-3 text-right">Loại KH</label>
                                <div class="col-sm-9">
                                    <input type="radio" id="gender-male" name="customer_group" value="1" checked>&nbsp;<label for="gender-male">Khách lẻ</label>&nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="gender-famale" name="customer_group" value="2">&nbsp;<label for="gender-famale">Khách sỉ</label>
                                </div>
                            </div>
                            <x-input type="text" title="Họ tên" name="name" value=""/>
                            <x-input type="text" title="Điện thoại" name="phone" value=""/>
                            <x-input type="text" title="Email" name="email" value=""/>
                            <x-input type="text" title="Địa chỉ" name="address" value=""/>
                            <div class="form-group row">
                                <label class="col-sm-3 text-right">Giới tính</label>
                                <div class="col-sm-9">
                                    <input type="radio" id="gender-male" name="gender" value="0" checked>&nbsp;<label for="gender-male">Nam</label>&nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="gender-famale" name="gender" value="1">&nbsp;<label for="gender-famale">Nữ</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Ngày sinh</label>
                                <div class="col-sm-9">
                                    <div class='input-group date datetime-pick'>
                                        <input type='text' class="form-control" name="birthday" value="{{ format_date(\Carbon\Carbon::now()) }}"/>
                                        <span class="input-group-addon bg-primary">
                                        <span class="feather icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <x-textarea type="" title="Ghi chú" name="note" value="" />
                            <x-textarea type="" title="Tình trạng da" name="note_skin" value="" />
                            <x-textarea type="" title="Tính cách" name="note_genitive" value="" />
                        </div>
                        <div class="col-12 d-flex flex-sm-row justify-content-center flex-column mt-2">
                            <button type="reset" data-dismiss="modal" class="btn btn-outline-secondary mr-sm-1" aria-label="Close">Hủy bỏ</button>
                            <button type="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0">Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>