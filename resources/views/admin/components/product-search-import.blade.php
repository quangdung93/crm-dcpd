<div class="product-searchbox">
    <div class="form-group row mb-0">
        <div class="col-sm-12">
            <div class="input-group search-tool">
                <span class="input-group-addon bg-primary"><i class="feather icon-search"></i></span>
                <input class="form-control searchbox-input" 
                data-name="{{$inputName}}" onfocus="productSearchBox.call(this)" 
                placeholder="Nhập mã sản phẩm hoặc tên sản phẩm"/>
            </div>
        </div>  
    </div>
    <div class="form-group row mb-0">
        <div class="col-sm-12">
            <table class="table table-products table-bordered w-100" @if (empty($products)) style="display: none" @endif>
                <thead>
                    <tr>
                        <th>Mã</th>
                        <th>Tên sản phẩm</th>
                        @if($mode == 'import')
                            <th>Số lượng</th>
                            <th>Giá bán</th>
                            <th>Thành tiền</th>
                            <th>Ngày hết hạn</th>
                            <th>Xóa</th>
                        @endif
                    </tr>
                </thead>
                    @if($products)
                    @foreach($products as $key => $product)
                    <tr>
                        <input class="id" value="{{ $product->id }}" name="products[{{ $key }}][id]" type="hidden">
                        <input value="{{ $product->code }}" name="products[{{ $key }}][code]" type="hidden">
                        <td><a href="#" class="label label-info" target="_blank">{{ $product->code }}</a></td>
                        <td><a href="#" target="_blank">{{ $product->name }}</a></td>
                        @if($mode == 'import')
                            <td class="product-qty">
                                <input class="form-control text-center" name="products[{{ $key }}][qty]" style="width: 50px" type="number" min="1" oninput="this.value=Math.abs(this.value)" value="1">
                            </td>
                            <td class="product-price">
                                <input type="text" class="form-control" name="products[{{ $key }}][price]" value="{{ number_format($product->price) }}">
                            </td>
                            <td class="product-total">
                                <input type="hidden" value="{{ $product->price }}">
                                <span>{{ number_format($product->price) }}</span>
                            </td>
                            <td class="product-expired">
                                <div class="input-group date expires-time">
                                    <input type="text" class="form-control" name="products[{{ $key }}][expired_date]" value="{{ format_date(\Carbon\Carbon::now()) }}">
                                    <span class="input-group-addon bg-primary">
                                    <span class="feather icon-calendar"></span>
                                    </span>
                                </div>
                            </td>
                        @endif
                        <td><i class="feather icon-trash-2 remove-product-searchbox text-danger"></i></td>
                    </tr>
                    @endforeach
                    @endif
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
@push('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        //Search product into table
        const countProductInit = '{{ isset($products) ? $products->count() : 0 }}';
        let count = parseInt(countProductInit) > 0 ? parseInt(countProductInit) : 0;
        productSearchBox = function(){
            var self = $(this);
            var nameValue = $(this).data('name');
            var parent = $(this).closest('.product-searchbox');
            $(this).autocomplete({
                minLength: 3,
                delay: 500,
                source: function (request, response) {
                    var listIds = parent.find('.id').map(function(){
                        return $(this).val();
                    }).get();
        
                    var listIdsParam = '';
                    if (listIds.length) {
                        listIdsParam = '&ids=' + listIds.join(',');
                    }
        
                    let url = "/admin/products/search?key=" + request.term + listIdsParam;
        
                    $.getJSON(url, function (data) {
                        if(data.error == 0){
                            let products = data.data;
                            var itemList = $.map(products, function (value, key) {
                                return {
                                    product_id: value.id,
                                    product_name: value.name,
                                    product_code: value.code,
                                    product_price: value.price,
                                    class: 'product-item-search'
                                };
                            });
                        }
        
                        response(itemList);
                    }).fail(function() {
                        response([]);
                    });
                },
                select: function( event, ui ) {
                    var html = renderProduct(ui.item, count);
                    parent.find('table').prepend(html);
                    self.val('');
                    if (parent.find("table").css('display') == 'none') {
                        parent.find("table").css('display', 'table');
                    }

                    count++;
                    @if($mode == 'import')
                        $('.expires-time').datepicker({format: 'dd/mm/yyyy'});
                        handlePrice();
                    @endif
                }
            }).autocomplete("instance")._renderItem = function( ul, item ) {
                    return $("<li class='search-item'>")
                        .append(renderProductSearchItem(item))
                        .appendTo(ul);
            };
        }

        function renderProductSearchItem(item){
            return `<a class='product-search-box'>
                    <span class='label label-info'> ${item.product_code}</span>
                    <span class='product-name'> ${item.product_name}</span>
                </a>`;
        }
        
        function renderProduct(product, count) {
            let now = '{{ format_date(\Carbon\Carbon::now()) }}';
            return `<tr>
                <input class="id" value="${ product.product_id }" name="products[${count}][id]" type="hidden"/>
                <input value="${ product.product_code }" name="products[${count}][code]" type="hidden"/>
                <td><a href="#" class="label label-info" target="_blank">${ product.product_code }</a></td>
                <td><a href="#" target="_blank">${ product.product_name }</a></td>
                @if($mode == 'import')
                    <td class="product-qty">
                        <input class="form-control text-center" name="products[${count}][qty]" style="width: 50px" type="number" min="1" oninput="this.value=Math.abs(this.value)" value="1">
                    </td>
                    <td class="product-price">
                        <input type="text" class="form-control" name="products[${count}][price]" value="${ parseInt(product.product_price).toLocaleString('en-US') }">
                    </td>
                    <td class="product-total">
                        <input type="hidden" value="${product.product_price * 1}">
                        <span>${ parseInt(product.product_price * 1).toLocaleString('en-US') }</span>
                    </td>
                    <td class="product-expired">
                        <div class='input-group date expires-time'>
                            <input type='text' class="form-control" name="products[${count}][expired_date]" value="${now}"/>
                            <span class="input-group-addon bg-primary">
                            <span class="feather icon-calendar"></span>
                            </span>
                        </div>
                    </td>
                @endif
                <td><i class="feather icon-trash-2 remove-product-searchbox text-danger"></i></td>
            </tr>`;
        }

        $(document).on("click", ".remove-product-searchbox", function () {
            if(confirm('Bạn có muốn xóa dòng này?')){
                $(this).closest("tr").remove();
                handlePrice();
            }
        });

        function format_price(price){
            return `${parseInt(price).toLocaleString('en-US')}`;
        }

        function handlePrice(){
            let qty = price = total = totalMoney = totalFinal = discount = 0;
            $('.table-products tbody tr').each(function(index, item){
                qty = $(this).find('.product-qty input').val();
                price = $(this).find('.product-price input').val();

                if(price == ''){
                    $(this).find('.product-price input').val(0);
                }

                price = price ? price.replaceAll(',', '') : 0;
                total = parseInt(qty) * parseInt(price);
                $(this).find('.product-total span').text(encodeCurrencyFormat(total));
                totalMoney += total;
            });

            $('#total-money').val(encodeCurrencyFormat(totalMoney));
        }


        $(document).on('keyup', ".product-qty input", function () {
            handlePrice();
        });

        $(document).on('keyup', ".product-price input", function () {
            handlePrice();
        });

    //End search product into table
    });
</script>
@endpush