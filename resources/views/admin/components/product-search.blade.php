<div class="product-searchbox">
    <div class="form-group row mb-0">
        <div class="col-sm-12">
            <div class="input-group search-tool">
                <span class="input-group-addon bg-primary"><i class="feather icon-search"></i></span>
                <input class="form-control searchbox-input" 
                data-name="{{$inputName}}" onfocus="productSearchBox.call(this)" 
                placeholder="Nhập mã sản phẩm hoặc tên sản phẩm"/>
            </div>
        </div>  
    </div>
    <div class="form-group row mb-0">
        <div class="col-sm-12">
            <table class="table table-products table-bordered w-100" @if (empty($products)) style="display: none" @endif>
                <thead>
                    <tr>
                        <th>Sản phẩm</th>
                        <th>Số lượng</th>
                        <th>Giá bán</th>
                        <th>CK(%)</th>
                        <th>Thành tiền</th>
                        <th>Xóa</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <div class="alert-success mt-3" role="alert">
        (*) Gõ mã hoặc tên sản phẩm vào hộp tìm kiếm để thêm hàng vào đơn hàng<br>
        Ô giảm giá: Nếu gõ nhỏ hơn 100 sẽ tương đương với số % nhập giảm, Nếu lớn hơn 100 tương đương với số tiền giảm.
    </div>
</div>
@push('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        //Search product into table
        let count = 0;
        productSearchBox = function(){
            var self = $(this);
            var nameValue = $(this).data('name');
            var parent = $(this).closest('.product-searchbox');
            $(this).autocomplete({
                minLength: 3,
                delay: 500,
                source: function (request, response) {
                    var listIds = parent.find('.id').map(function(){
                        return $(this).val();
                    }).get();
        
                    var listIdsParam = '';
                    if (listIds.length) {
                        listIdsParam = '&ids=' + listIds.join(',');
                    }
        
                    let url = "/admin/products/search?key=" + request.term + listIdsParam;
        
                    $.getJSON(url, function (data) {
                        if(data.error == 0){
                            let products = data.data;
                            var itemList = $.map(products, function (value, key) {
                                return {
                                    product_id: value.id,
                                    product_name: value.name,
                                    product_code: value.code,
                                    product_price: value.price,
                                    class: 'product-item-search'
                                };
                            });
                        }
        
                        response(itemList);
                    }).fail(function() {
                        response([]);
                    });
                },
                select: function( event, ui ) {
                    var html = renderProduct(ui.item, count);
                    parent.find('table').prepend(html);
                    self.val('');
                    if (parent.find("table").css('display') == 'none') {
                        parent.find("table").css('display', 'table');
                    }

                    $('#has-debt').attr({disabled: false});
                    
                    count++;
                    handlePrice();
                }
            }).autocomplete("instance")._renderItem = function( ul, item ) {
                    return $("<li class='search-item'>")
                        .append(renderProductSearchItem(item))
                        .appendTo(ul);
            };
        }

        function renderProductSearchItem(item){
            return `<a class='product-search-box'>
                    <span class='label label-info'> ${item.product_code}</span>
                    <span class='product-name'> ${item.product_name}</span>
                </a>`;
        }
        
        function renderProduct(product, count) {
            return `<tr>
                <input class="id" value="${ product.product_id }" name="products[${count}][id]" type="hidden"/>
                <td>
                    ${ product.product_name }<br>
                    <a href="#" class="label label-info" target="_blank">${ product.product_code }</a>
                <td class="product-qty">
                    <input class="form-control text-center" name="products[${count}][qty]" style="width: 50px" type="number" min="1" oninput="this.value=Math.abs(this.value)" value="1">
                </td>
                <td class="product-price">
                    <input type="text" class="form-control" name="products[${count}][price]" value="${ parseInt(product.product_price).toLocaleString('en-US') }">
                </td>
                <td class="product-discount">
                    <input type="text" class="form-control" name="products[${count}][discount]" oninput="this.value=Math.abs(this.value)" style="width: 50px" value="0">
                </td>
                <td class="product-total">
                    <input type="hidden" value="${product.product_price * 1}">
                    <span>${ parseInt(product.product_price * 1).toLocaleString('en-US') }</span>
                </td>
                <td><i class="feather icon-trash-2 remove-product-searchbox text-danger"></i></td>
            </tr>`;
        }

        $(document).on("click", ".remove-product-searchbox", function () {
            if(confirm('Bạn có muốn xóa dòng này?')){
                $(this).closest("tr").remove();
                handlePrice();
            }
        });

        function format_price(price){
            return `${parseInt(price).toLocaleString('en-US')}`;
        }

        function handlePrice(){
            let qty = price = total = totalMoney = totalFinal = discount = 0;
            $('.table-products tbody tr').each(function(index, item){
                qty = $(this).find('.product-qty input').val();
                discountInput = $(this).find('.product-discount input').val();
                price = $(this).find('.product-price input').val();

                if(price == ''){
                    $(this).find('.product-price input').val(0);
                }

                if(discountInput == ''){
                    $(this).find('.product-discount input').val(0);
                }

                price = price ? price.replaceAll(',', '') : 0;
                let itemPrice = parseInt(price) - (parseInt(price) * discountInput/100);
                total = parseInt(qty) * itemPrice;
                $(this).find('.product-total span').text(encodeCurrencyFormat(total));
                totalMoney += total;
            });

            let customerPay = $('#customer-pay').val();

            if($('#discount').val() == ''){
                discount = 0;
            }
            else{
                discount = $('#discount').val();
            }

            if(discount > totalMoney){
                $('#discount').val(totalMoney);
                discount = totalMoney;
            }

            if(discount > 0 && discount <= 100){
                totalFinal = totalMoney - (totalMoney * discount/100);
                $('#discount-note').text(`Giảm giá ${discount}%`);
            }
            else{
                totalFinal = totalMoney - discount;
                if(discount > 0) $('#discount-note').text(`Giảm giá ${encodeCurrencyFormat(discount)} đ`);
            }

            $('#total-money').val(encodeCurrencyFormat(totalMoney));
            $('#total-final').val(encodeCurrencyFormat(totalFinal));
            $('#customer-pay').val(encodeCurrencyFormat(totalFinal));

            if($('#has-debt').is(':checked')){
                $('#debt').val(encodeCurrencyFormat(totalFinal));
                $('#customer-pay').val(0);
            }
        }

        $(document).on('keyup', 'input#discount',function () {
            handlePrice();
        });

        $(document).on('keyup', 'input#customer-pay', function () {
            let totalFinal = $('#total-final').val();
            let value = $(this).val();

            let debt = totalFinal.replaceAll(',', '') - value.replaceAll(',', '');
            $('#debt').val(encodeCurrencyFormat(debt));
        });

        $(document).on('keyup', ".product-qty input", function () {
            handlePrice();
        });

        $(document).on('keyup', ".product-price input", function () {
            handlePrice();
        });

        $(document).on('keyup', ".product-discount input", function () {
            handlePrice();
        });

    //End search product into table
    });
</script>
@endpush