@extends('admin.body')
@php
    $pageName = 'Khách hàng';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row ">
            <div class="col-sm-12">
                @can('add_customers')
                    <div class="text-right mb-3">
                        <a href="{{url($routeName.'/create')}}" class="btn btn-primary"><i
                                class="feather icon-plus"></i> Thêm mới</a>
                    </div>
                @endcan
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Bộ lọc</h4>
                        <form id="search-customer">
                        <div class="row">
                                <div class="col-sm-4">
                                    <input class="form-control" name="name" placeholder="Nhập tên, mã hoặc SDT khách hàng"/>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control populate select2" id="saler-id" name="saler_id">
                                        <option value="0">Chọn nhân viên</option>
                                        @if($users)
                                            @foreach ($users as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control populate select2" name="customer_level">
                                        <option value="">Hạng khách hàng</option>
                                        <option value="non-member">Chưa đạt hạng</option>
                                        <option value="member">Thành viên</option>
                                        <option value="silver">Bạc</option>
                                        <option value="golden">Vàng</option>
                                        <option value="platinum">Bạch kim</option>
                                        <option value="diamond">Kim cương</option>
                                    </select>
                                </div>
                                <div class="col-sm-4 mt-3">
                                    <input type="text" 
                                        name="price_from" 
                                        class="form-control input-price text-right" 
                                        onkeyup="this.value=formatMoney(this.value)" 
                                        onclick="this.select()"
                                        autocomplete="off" 
                                        placeholder="Từ số tiền"/>
                                </div>
                                <div class="col-sm-4 mt-3">
                                    <input type="text" 
                                        name="price_to" 
                                        class="form-control input-price text-right" 
                                        onkeyup="this.value=formatMoney(this.value)" 
                                        onclick="this.select()"
                                        autocomplete="off" 
                                        placeholder="Đến số tiền"/>
                                </div>
                                <div class="col-sm-4 mt-3">
                                    <select class="form-control populate select2" name="birtday_month">
                                        <option value="0">Tháng sinh</option>
                                        @for($i = 1; $i <= 12; $i++)
                                            @if($i < 10)
                                                <option value="0{{ $i }}">Tháng 0{{ $i }}</option>
                                            @else
                                                <option value="{{ $i }}">Tháng {{ $i }}</option>
                                            @endif
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-sm-4 mt-3">
                                    <select class="form-control populate select2" name="customer_care">
                                        <option value="0">Chọn thời gian chăm sóc</option>
                                        <option value="1">KH chưa chăm sóc</option>
                                        <option value="2">KH 60 ngày chưa mua hàng</option>
                                    </select>
                                </div>
                                <div class="col-sm-4 mt-3">
                                    <select class="form-control populate select2" name="sort_by">
                                        <option value="">Sắp xếp theo</option>
                                        <option value="money_min">Tổng tiền thấp đến cao</option>
                                        <option value="money_max">Tổng tiền cao đến thấp</option>
                                        <option value="total_min">Số lượng đơn thấp đến cao</option>
                                        <option value="total_max">Số lượng đơn cao đến thấp</option>
                                    </select>
                                </div>
                                <div class="col-sm-4 mt-3">
                                    <button type="submit" class="btn btn-success mt-1">
                                        <span><i class="feather icon-search"></i></span>
                                        Tìm kiếm
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            <table id="datatable" class="table stableweb-table center w100">
                                <thead>
                                    <tr>
                                        <th>Mã KH</th>
                                        <th>Tên KH</th>
                                        <th>Tổng tiền</th>
                                        <th>Ngày bán/chăm sóc</th>
                                        <th>Nhân viên</th>
                                        <th>Bán hàng</th>
                                        <th>Chỉnh sửa</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-body end -->
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        const ajax_url = "{!! route('customers.view') !!}";
        const URL_ORDER_LIST = "{!! route('customers.list') !!}";
        const columns = [
            { data: 'customer_code',name: 'image',orderable: false, searchable: false},
            { data: 'name',name: 'name',width: '25%'},
            { data: 'total_money',name: 'total_money'},
            { data: 'customer_date',name: 'customer_date'},
            { data: 'saler_id',name: 'saler_id', className: 'nowrap'},
            { data: 'sale',name: 'discount'},
            { data: 'action',orderable: false, searchable: false, className: 'nowrap'}
        ];

        showDataTableServerSide($('#datatable'), ajax_url, columns);

        $(document).on('submit', '#search-customer', function(e){
            e.preventDefault();

            let self = $(this);
            let url = `${URL_ORDER_LIST}?${$(this).serialize()}`;

            showDataTableServerSide($('#datatable'), url, columns);
            
            // $.ajax({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            //     method: 'GET',
            //     url: url,
            //     success: function (response) {
            //         if (response.error == 0) {
            //             showDataTableServerSide($('#datatable'), ajax_url, columns);
            //         }
            //     }
            // });
        });

    });
</script>
@endsection