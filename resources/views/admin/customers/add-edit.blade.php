@extends('admin.body')
@php
    $type = request()->get('type');
    $pageName = getCustomerGroup($type);
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="panel-body">
            <form class="form-horizontal" action="{{url($routeName)}}" method="POST" role="form"
                enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Thông tin {{ $pageName }}</h4>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Loại khách hàng</label>
                                    @php
                                        $categories = [
                                            [
                                                'id' => App\Models\Customer::GROUP_SINGLE,
                                                'name' => 'Khách lẻ'
                                            ],
                                            [
                                                'id' => App\Models\Customer::GROUP_AGENCY,
                                                'name' => 'Đại lý'
                                            ],
                                            [
                                                'id' => App\Models\Customer::GROUP_SALE,
                                                'name' => 'Nhân viên sale'
                                            ]
                                        ];
                                    @endphp
                                    <div class="col-sm-9">
                                        <select class="form-control populate select2" name="customer_group">
                                            @foreach($categories as $item) 
                                            @php
                                                $selected = '';
                                                if(isset($customer)){ //edit
                                                    if($customer->customer_group == $item['id']){
                                                        $selected = 'selected';
                                                    }
                                                }
                                                else{ //create
                                                    if(isset($type)){
                                                        if($type == App\Models\Customer::GROUP_AGENCY 
                                                            && $item['id'] == App\Models\Customer::GROUP_AGENCY){
                                                            $selected = 'selected';
                                                        }
                                                        elseif($type == App\Models\Customer::GROUP_SALE 
                                                            && $item['id'] == App\Models\Customer::GROUP_SALE){
                                                            $selected = 'selected';
                                                        }
                                                    }
                                                    elseif($item['id'] == App\Models\Customer::GROUP_SINGLE){
                                                        $selected = 'selected';
                                                    }
                                                }
                                            @endphp
                                                <option value="{{$item['id']}}" {{ $selected }}>{{$item['name']}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('customer_group'))
                                            <div class="text-danger mt-2">{{ $errors->first('customer_group') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <x-input type="text" :title="'Tên '.$pageName" name="name" value="{{ $customer->name ?? ''  }}"/>
                                {{-- <x-input type="text" title="Mã khách hàng" name="customer_code" value="{{ $customer->customer_code ?? ''  }}"/> --}}
                                <x-input type="text" title="Điện thoại" name="phone" value="{{ $customer->phone ?? ''  }}"/>
                                <x-input type="text" title="Email" name="email" value="{{ $customer->email ?? ''  }}"/>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Ngày sinh</label>
                                    <div class="col-sm-9">
                                        <div class='input-group date datetime-pick'>
                                            <input type='text' class="form-control" name="birthday" value="{{ isset($customer->birthday) ? format_date($customer->birthday) : format_date(\Carbon\Carbon::now()) }}"/>
                                            <span class="input-group-addon bg-primary">
                                            <span class="feather icon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 text-right">Giới tính</label>
                                    <div class="col-sm-9">
                                        <input type="radio" id="gender-male" name="gender" value="0" {{ isset($customer) ? ($customer->gender == 0 ? 'checked' : '') : 'checked' }}>&nbsp;<label for="gender-male">Nam</label>&nbsp;&nbsp;&nbsp;
                                        <input type="radio" id="gender-famale" name="gender" value="1" {{(isset($customer) && $customer->gender == 1) ? 'checked' : '' }}>&nbsp;<label for="gender-famale">Nữ</label>
                                    </div>
                                </div>
                                @if(isLeader())
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label text-right">Nhân viên chăm sóc</label>
                                        <div class="col-sm-9">
                                            <div class="checkout-select">
                                                <select name="saler_id" class="form-control select2">
                                                    @foreach($users as $key => $value)
                                                        <option value="{{ $value->id }}" {{ isset($customer) ? ($customer->saler_id == $value->id ? 'selected' : '') : (\Auth::id() == $value->id ? 'selected' : '') }}>{{ $value->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="error text-danger mt-2"></div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Tỉnh/thành phố</label>
                                    <div class="col-sm-9">
                                        <div class="checkout-select">
                                            <select id="provinces" name="province_id" data-id="{{ isset($customer) ? $customer->province_id : '' }}" class="form-control select2">
                                                @foreach($provinces as $key => $value)
                                                    <option value="{{ $value->id }}" {{ (isset($customer) && $customer->province_id == $value->id) ? 'selected' : '' }}>{{ $value->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="error text-danger mt-2"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Quận/huyện</label>
                                    <div class="col-sm-9">
                                        <div class="checkout-select">
                                            <select id="districts" name="district_id" data-id="{{ isset($customer) ? $customer->district_id : '' }}" class="form-control select2">
                                                <option value="0">Chọn quận/huyện</option>
                                            </select>
                                            <div class="error text-danger mt-2"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Phường/xã</label>
                                    <div class="col-sm-9">
                                        <div class="checkout-select">
                                            <select id="wards" name="ward_id" data-id="{{ isset($customer) ? $customer->ward_id : '' }}" class="form-control select2">
                                                <option value="0">Chọn phường/xã</option>
                                            </select>
                                            <div class="error text-danger mt-2"></div>
                                        </div>
                                    </div>
                                </div>
                                <x-textarea type="" title="Địa chỉ" name="address" value="{{ $customer->address ?? ''  }}" />
                                <x-textarea type="" title="Tình trạng da" name="note_skin" value="{{ $customer->note_skin ?? ''  }}" />
                                <x-textarea type="" title="Tính cách" name="note_genitive" value="{{ $customer->note_genitive ?? ''  }}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Trạng thái</h4>
                                <x-switch-box 
                                type="short" 
                                title="Trạng thái" 
                                name="status" 
                                checked="{{ !isset($product) ? 'true' : ($product->status ? 'true' : '') }}"/>
                            </div>
                        </div>
                    </div>
                </div>
                <x-submit-button :route="$routeName"/>
            </form>
        </div>
    </div>
<!-- Page-body end -->
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){

        $('.datetime-pick').datepicker({format: 'dd/mm/yyyy'});

        //Handle Address
        let activeProvincesId = $('#provinces').data('id');

        if(activeProvincesId){
            loadDistrict(activeProvincesId);
        }

        $(document).on('change', '#provinces', function () {
            let province_id = this.value;
            if (!province_id) {
                return false;
            }

            loadDistrict(province_id);
        });

        $(document).on('change', '#districts', function () {
            let district_id = this.value;
            if (!district_id) {
                return false;
            }

            loadWards(district_id);
        });

        function loadDistrict(province_id){
            let activeDistrictId = $('#districts').data('id');
            $.get('/province/' + province_id, function (result) {
                if (result.error == 0) {
                    $('#districts').empty();
                    $('#districts').append(result.data);
                    $("#districts").trigger("chosen:updated");

                    if(activeDistrictId){
                        $('#districts').val(activeDistrictId).trigger('change');
                        $('#districts').data('id', "");
                    }

                } else {
                    alert(result.message.title);
                }
            });
        }

        function loadWards(district_id){
            let activeWardId = $('#wards').data('id');
            $.get('/district/' + district_id, function (result) {
                if (result.error == 0) {
                    $('#wards').empty();
                    $('#wards').append(result.data);
                    $("#wards").trigger("chosen:updated");

                    if(activeWardId){
                        $('#wards').val(activeWardId).trigger('change');
                        $('#wards').data('id', "");
                    }
                } else {
                    alert(result.message.title);
                }
            });
        }

        //End Handle Address
    });
</script>
@endsection