<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="charset=utf-8" />
    <meta charset="UTF-8">
    <title>Đơn hàng</title>
    <style>
        body {
            font-family: DejaVu Sans, sans-serif;
            font-size: 13px
        }

        p{
            margin: 0;
        }

        th {
            /* background-color: #f1f1f1; */
            color: #333;
            padding: 5px;
            border: 1px solid #333
        }

        td{
            border-bottom: 1px solid #333;
        }

        /* tr:nth-child(odd){background-color: #f1f1f1;} */

        /* tr:nth-child(even){background-color: #e0e0e0;} */

        .logo{
            float: left;
            width: 200px;
            min-width: 200px;
        }

        .logo img{
            max-width: 100%;
            width: 200px
        }

        .info{
            float: left;
            margin-left: 40px;
            width: 100%;
        }

        .info .name{
        }
    </style>
</head>
<body>
    @php
        $companyLogo = $store->logo ?? '';
        $imageData = '';
        if($companyLogo){
            $avatarUrl = asset($companyLogo);
            $arrContextOptions = array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
            $type = @pathinfo($avatarUrl, PATHINFO_EXTENSION);
            $avatarData = @file_get_contents($avatarUrl, false, stream_context_create($arrContextOptions));
            $avatarBase64Data = @base64_encode($avatarData);
            $imageData = 'data:image/' . $type . ';base64,' . $avatarBase64Data;
        }
    @endphp
    <div class="content">
        <div class="header">
            <div class="logo">
                <img src="{{ $imageData }}" alt="" />
            </div>
            <div class="info">
                @php
                    $admin_company_name = $store->company_name ?? '';
                    $admin_address = $store->address ?? '';
                    $admin_phone = $store->phone ?? '';
                    $admin_website = $store->website ?? '';
                    $admin_email = $store->email ?? '';
                    $admin_facebook = $store->facebook ?? '';
                    $admin_zalo = $store->zalo ?? '';
                    $admin_note = $store->note ?? '';
                @endphp
                @if($admin_company_name)
                <h4 style="margin: 0; text-transform: uppercase">{{ $admin_company_name }}</h4>
                @endif

                @if($admin_address)
                <p class="name">Địa chỉ: {{ $admin_address }}</p>
                @endif

                @if($admin_phone)
                <p class="name">Điện thoại: {{ $admin_phone }}</p>
                @endif

                @if($admin_website)
                <p class="name">Website: {{ $admin_website }}</p>
                @endif

                @if($admin_email)
                <p class="name">Email: {{ $admin_email }}</p>
                @endif

                @if($admin_facebook)
                <p class="name">Facebook: {{ $admin_facebook }}</p>
                @endif
            </div>
        </div>
        <div style="clear:both"></div>
        <h2 style="text-align: center;margin: 0">PHIẾU GIAO HÀNG</h2>
        <div>
            <div style="float:left; width: 40%">Tên khách hàng: {{ optional($order->customer)->name }}</div>
            <div style="float:left; width: 30%">Điện thoại: {{ optional($order->customer)->phone }}</div>
            <div style="float:right">Ngày: {{ format_datetime($order->created_at) }}</div>
            <div style="clear:both"></div>
            <div>Địa chỉ: {{ optional($order->customer)->address }}</div>
        </div>

        <div class="table" style="margin-top: 20px">
            <table style="width: 100%">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th style="width: 30%">Sản phẩm</th>
                        <th class="text-right">Đơn giá</th>
                        <th>Số lượng</th>
                        <th>Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    @if($order->detail)
                        @foreach($order->detail as $row)
                            <tr style="border:1px solid #e0e0e0">
                                <td style="text-align: center; width: 50px">{{ $loop->iteration }}</td>
                                <td style="text-align: left; width: 40%">{{$row->name}}</td>
                                <td style="text-align: center"> {{ number_format($row->pivot->price) }} đ</td>
                                <td style="text-align: center"> {{$row->pivot->qty}} </td>
                                <td style="text-align: center">{{ number_format($row->pivot->subtotal) }} đ</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            @if($order->coupon > 0)
                <div style="text-align: right; padding: 5px">
                    <span class="font-weight-bold">Tiền hàng: </span>
                    <span class="font-weight-bold text-danger">{{ number_format($order->total_price) }} đ</span>
                </div>
                <div style="text-align: right; padding: 5px">
                    <span class="font-weight-bold">Giảm giá: </span>
                    <span class="font-weight-bold text-danger">-{{ number_format($order->coupon) }} đ</span>
                </div>
            @endif
            <div style="text-align: right; padding: 5px">
                <span class="font-weight-bold">Tổng tiền: </span>
                <span class="font-weight-bold text-danger">{{ number_format($order->total_money) }} đ</span>
            </div>
            <h4 style="text-align: right; text-transform: capitalize; padding: 5px;margin: 0">Bằng chữ: {{ $moneyToVietnamese }}</h4>
            @if($admin_note)
                <p style="font-style: italic; margin: 0">{{ $admin_note }}</p>
            @endif
            <div>
                <div style="float:left; text-align: center; width: 50%">Người nhận <br> (Ký, ghi rõ họ tên)</div>
                <div style="float:left; text-align: center; width: 50%">Người giao <br> (Ký, ghi rõ họ tên)</div>
            </div>
        </div>
    </div>
</body>
</html>