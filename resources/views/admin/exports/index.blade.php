@extends('admin.body')
@php
    $pageName = 'Xuất hàng';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="col-sm-12">
            @can('add_product_exports')
                <div class="text-right mb-3">
                    <a href="{{url($routeName.'/create')}}" class="btn btn-primary"><i
                            class="feather icon-shopping-cart"></i> Xuất hàng</a>
                </div>
            @endcan
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            <table id="datatable" class="table stableweb-table center w100">
                                <thead>
                                    <tr>
                                        <th>Mã đơn xuất</th>
                                        <th>Nguồn</th>
                                        <th>Ngày xuất hàng</th>
                                        <th>Ngày tạo</th>
                                        <th>Trạng thái</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-body end -->

    <div class="modal fade" id="modal-order-detail">
        <div class="modal-dialog" role="document" style="max-width: 80%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Chi tiết đơn hàng <span id="order-code"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-export-detail">
        <div class="modal-dialog" role="document" style="max-width: 80%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Chi tiết phiếu xuất <span id="export-code"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        const ajax_url = "{!! route('export.view') !!}";
        var columns = [
            { data: 'code',name: 'code',orderable: false, searchable: false},
            { data: 'order_id',name: 'order_id',orderable: false, searchable: false},
            { data: 'export_date',name: 'export_date'},
            { data: 'created_at',name: 'created_at'},
            { data: 'status',name: 'status'},
            { data: 'action',orderable: false, searchable: false, className: 'nowrap'}
        ];

        showDataTableServerSide($('#datatable'), ajax_url, columns);

        $(document).on('click', '.btn-detail-order', function(){
            let order_id = $(this).data('id');
            let order_code = $(this).data('code');

            if(!order_id){
                return;
            }

            $.get(`${URL_MAIN}admin/orders/detail/${order_id}?type=order`, {}, function(response){
                if(response.error == 0){
                    $('#order-code').text(order_code);
                    $('#modal-order-detail .modal-body').html(response.data);
                }
            });
        });

        $(document).on('click', '.btn-detail-export', function(){
            let export_id = $(this).data('id');
            let export_code = $(this).data('code');

            if(!export_id){
                return;
            }

            $.get(`${URL_MAIN}admin/exports/detail/${export_id}`, {}, function(response){
                if(response.error == 0){
                    $('#export-code').text(export_code);
                    $('#modal-export-detail .modal-body').html(response.data);
                }
            });
        });
    });
</script>
@endsection