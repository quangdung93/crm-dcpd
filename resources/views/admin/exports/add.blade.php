@extends('admin.body')
@php
    $pageName = 'Xuất hàng';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="panel-body">
            <form class="form-horizontal" action="{{url($routeName)}}" method="POST" role="form"
                enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Đơn hàng</h4>
                                @if ($errors->has('order_id'))
                                    <div class="text-danger mb-3">{{ $errors->first('order_id') }}</div>
                                @endif
                                <div class="dt-responsive table-responsive">
                                    <table id="datatable" class="table stableweb-table center w100">
                                        <thead>
                                            <tr>
                                                <th>Mã đơn</th>
                                                <th>Thông tin</th>
                                                <th>Trạng thái</th>
                                                <th>Tổng SL</th>
                                                <th>Tổng tiền</th>
                                                {{-- <th>Thao tác</th> --}}
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Thông tin {{ $pageName }}</h4>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Xuất từ:</label>
                                    <div class="col-sm-3">
                                        <select class="form-control populate select2" name="warehouse_id">
                                            @if($warehouses)
                                                @foreach($warehouses as $warehouse)
                                                    <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('warehouse_id'))
                                            <div class="text-danger mt-2">{{ $errors->first('warehouse_id') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Ngày xuất hàng:</label>
                                    <div class="col-sm-3">
                                        <div class='input-group date datetime-pick'>
                                            <input type='text' class="form-control" name="export_date" value="{{ format_date(\Carbon\Carbon::now()) }}"/>
                                            <span class="input-group-addon bg-primary">
                                            <span class="feather icon-calendar"></span>
                                            </span>
                                        </div>
                                        @if ($errors->has('export_date'))
                                            <div class="text-danger mt-2">{{ $errors->first('export_date') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <x-textarea type="" title="Ghi chú" name="notes" value=""/>
                            </div>
                        </div>
                    </div>
                </div> --}}
                {{-- <div class="form-group row">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6 text-right">
                        <a class="btn btn-danger btn-label-left f-right" href="{{url($routeName)}}">
                            <span><i class="feather icon-chevrons-left"></i></span>
                            Hủy
                        </a>
                        <button type="submit" class="btn btn-primary btn-label-left">
                            <span><i class="feather icon-save"></i></span>
                            Lưu
                        </button>
                    </div>
                </div> --}}
            </form>
        </div>
    </div>
<!-- Page-body end -->

<div class="modal fade" id="modal-order-detail">
    <div class="modal-dialog" role="document" style="max-width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Chi tiết đơn hàng <span id="order-code"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        const URL_ORDERS_LIST = "{!! route('order.list') !!}";
        const URL_EXPORT = "{!! route('export.add') !!}";
        const exportOrderId = "{{ request()->get('order_id') }}";
        var columns = [
            { data: 'code', name: 'code', orderable: false, searchable: false},
            { data: 'info',name: 'info',width: '25%'},
            { data: 'status',name: 'status', width: '250px', className: 'prevent-event'},
            { data: 'total_qty',name: 'total_qty', className: 'nowrap'},
            { data: 'total_money',name: 'total_money'},
            // { data: 'action',orderable: false, searchable: false, className: 'nowrap'}
        ];
        let ajax_url = `${URL_ORDERS_LIST}?status=0&export_order_id=${exportOrderId}`;
        showDataTableServerSide($('#datatable'), ajax_url, columns);

        $('.datetime-pick').datepicker({format: 'dd/mm/yyyy'});

        setTimeout(() => {
            $('#store').val($("#store-id option:selected").val());
        }, 500);

        $(document).on('submit', '#frm-order-detail', function(e){
            e.preventDefault();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: `${URL_EXPORT}`,
                data: $(this).serialize(),
                success: function (response) {
                    if (response.error == 0) {
                        pushNotify(response.message.title);
                        window.location.href = "{!! route('export.index') !!}";
                    }
                    else{
                        pushNotify(response.message.title, text = '', type = 'danger');
                    }
                }
            });
        });

        $(document).on('click', '.btn-detail-order', function(){
            let order_id = $(this).data('id');
            let order_code = $(this).data('code');

            //Reset modal
            $('#modal-order-detail .modal-body').html('');

            const storeId = $("#store-id option:selected").val();

            if(!order_id){
                return;
            }

            $.get(`${URL_MAIN}admin/orders/detail/${order_id}?type=export&store_id=${storeId}`, {}, function(response){
                if(response.error == 0){
                    $('#order-code').text(order_code);
                    $('#modal-order-detail .modal-body').html(response.data);
                    $('.select2-modal').each(function() { 
                        $(this).select2({ dropdownParent: $(this).parent()});
                    });
                }
            });
        });
    });
</script>
@endsection