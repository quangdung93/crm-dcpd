@extends('admin.body')
@section('title','Tổng quan')
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/chart-apex.css') }}">
@endsection
@section('content')
<div class="page-body dashbroad">
    <div class="row">
        <!-- task, page, download counter  start -->
        <div class="col-sm-4">
            <div class="bg-green-400 panel widget center bgimage" style="border-radius:6px; position:relative; overflow:hidden; background-image: linear-gradient(to right, #6a11cb 0%, #2575fc 100%);">
                <div style="width:100%; height:40px; display:flex; position:relative; z-index:1; justify-content: start; color:#fff;">
                    <img src="{{ asset('assets/images/deploy-to-do.png') }}" class="hidden-md" style="width:40px; height:40px;">
                    <div class="relative" style="margin-left:14px; text-align:left;">
                        <p style="color:#ffffff; display:block; margin:0px; font-weight:500; font-size:17px; line-height:17px; margin-top:3px; margin-bottom:4px;">Sản phẩm</p>
                        <p style="display:block; color:#dce9fe; text-align:left; line-height:14px; margin:0px;font-size:12px">{{ App\Models\Product::active()->count() }} sản phẩm</p>
                    </div>
                    <a href="{{ route('products.index') }}" style="cursor:pointer; flex-shrink:0; justify-self:end; margin-left:auto; background:#fff; color:#4801FF; font-weight:500; padding:10px 20px; border-radius:4px;">
                        Xem ngay
                    </a>
                </div>
                
                <img src="{{ asset('assets/images/digital-ocean.png') }}" style="width:130px; z-index:1; height:auto; top:10px; right:140px; opacity:30%; position:absolute;">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="bg-green-400 panel widget center bgimage" style="border-radius:6px; position:relative; overflow:hidden; background-image: linear-gradient(to right, #b8cbb8 0%, #b8cbb8 0%, #b465da 0%, #cf6cc9 33%, #ee609c 66%, #ee609c 100%);">
                <div style="width:100%; height:40px; display:flex; position:relative; z-index:1; justify-content: start; color:#fff;">
                    <img src="{{ asset('assets/images/play-icon.png') }}" class="hidden-md" style="width:40px; height:40px;">
                    <div class="relative" style="margin-left:14px; text-align:left;">
                        <p style="color:#ffffff; display:block; margin:0px; font-weight:500; font-size:17px; line-height:17px; margin-top:3px; margin-bottom:4px;">Đơn hàng</p>
                        <p style="display:block; text-align:left; line-height:14px; margin:0px;font-size:12px">{{ App\Models\Order::count() }} đơn hàng</p>
                    </div>
                    <a href="{{ route('orders.index') }}" style="cursor:pointer; flex-shrink:0;  justify-self:end; margin-left:auto; background:#fff; color:#ee445d; font-weight:500; padding:10px 20px; border-radius:4px;">
                        Xem ngay
                    </a>
                </div>
                <img src="{{ asset('assets/images/popcorn-soda-icon.png') }}" style="width:80px; z-index:10; height:auto; bottom:-30px; right:5px; position:absolute;">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="bg-green-400 panel widget center bgimage" style="border-radius:6px; position:relative; overflow:hidden; background-image: url('{{ asset('assets/images/tails-bg.png') }}'); background-size:cover;">
                <div style="width:100%; height:40px; display:flex; position:relative; z-index:20; justify-content: start; color:#fff;">
                    <img src="{{ asset('assets/images/tails-icon.png') }}" class="hidden-md" style="width:40px; height:40px;">
                    <div class="relative" style="margin-left:14px; text-align:left;">
                        <p style="color:#ffffff; display:block; margin:0px; font-weight:500; font-size:17px; line-height:17px; margin-top:3px; margin-bottom:4px;">Khách hàng</p>
                        <p style="display:block; text-align:left; color:#dce9fe; line-height:14px; margin:0px;font-size:12px">{{ App\Models\Customer::count() }} khách hàng</p>
                    </div>
                    <a href="{{ route('customers.index') }}" style="cursor:pointer; flex-shrink:0;  justify-self:end; margin-left:auto; background:#fff; color:#4801FF; font-weight:500; padding:10px 20px; border-radius:4px;">
                        Xem ngay
                    </a>
                </div>
            </div>
        </div>
        <!-- task, page, download counter  end -->
    </div>
    @can('read_statics')
    <div class="row mt-3">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="sub-title">Thống Kê Doanh Số (<span id="label-from">{{ format_date(\Carbon\Carbon::now()->subMonths(6)) }}</span> - <span id="label-to">{{ format_date(\Carbon\Carbon::now()) }}</span>)</h4>
                    <form id="ajax-search">
                        <div class="row">
                            <div class="col-sm-12 pr-0">
                                <div class="form-group">
                                    @can('read_orders')
                                        <input type="radio" class="revenue-checked" name="revenue_type" value="1" id="revenue-finish" checked> <label for="revenue-finish" class="font-weight-bold mr-3">Doanh số đã xác nhận</label>
                                        <input type="radio" class="revenue-checked" name="revenue_type" value="2" id="revenue-shipping"> <label for="revenue-shipping" class="font-weight-bold mr-3">Doanh số chưa xác nhận</label>
                                    @endcan
                                </div>
                            </div>
                        </div>
                        {{-- <div class="row mb-4">
                            <div class="col-sm-3">
                                <select class="form-control populate select2" id="saler-id" name="saler_id">
                                    <option value="0">Chọn nhân viên bán hàng</option>
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="col-sm-2">
                                <div class='input-group date datetime-pick' id="date-from-pick">
                                    <input type='text' class="form-control" id="date_from" name="date_from" value="{{ format_date(\Carbon\Carbon::now()->subMonths(6)) }}"/>
                                    <span class="input-group-addon bg-primary">
                                    <span class="feather icon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class='input-group date datetime-pick' id="date-to-pick">
                                    <input type='text' class="form-control" id="date_to" name="date_to" value="{{ format_date(\Carbon\Carbon::now()) }}"/>
                                    <span class="input-group-addon bg-primary">
                                    <span class="feather icon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-3 text-center mt-1">
                                <div class="btn-group order-btn-calendar">
                                    <button type="button" id="current-week" class="btn btn-default">Tuần</button>
                                    <button type="button" id="current-month" class="btn btn-default">Tháng</button>
                                    <button type="button" id="current-quarter" class="btn btn-default">Quý</button>
                                    <button type="button" id="current-year" class="btn btn-default">Năm</button>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-success mt-1">
                                    <span><i class="feather icon-search"></i></span>
                                    Xem thống kê
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-block">
                    <h4 class="sub-title m-0">Doanh số theo nguồn khách hàng</h4>
                </div>
                <div class="card-body">
                    <canvas class="chart-pie-source-order chartjs" data-height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-block">
                    <h4 class="sub-title m-0">Doanh số theo khách hàng</h4>
                </div>
                <div class="card-body">
                    <canvas class="bar-chart-customer-order chartjs" data-height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="sub-title m-0">Doanh số bán hàng</h4>
                </div>
                <div class="card-body">
                    <canvas class="bar-chart-revenue chartjs" data-height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="sub-title m-0">Doanh số bán hàng theo nhân viên</h4>
                </div>
                <div class="card-body">
                    <canvas class="bar-chart-ex chartjs" data-height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
    @endcan
</div>
@endsection

@section('javascript')
    @can('read_statics')
    {{-- Charts --}}
    <script src="{{ asset('admin/assets/js/apexcharts.min.js') }}"></script>
    <script src="{{ asset('admin/assets/js/chart.min.js') }}"></script>

    <script type="text/javascript">
        window.chartPieSourceOrder = null;
        window.barChartCustomerTop = null;
        window.barChartRevenue = null;
        window.barChartEmployee = null;

        const chartWrapper = $('.chartjs');
        const flatPicker = $('.flat-picker');
        const barChartEx = $('.bar-chart-ex');

        const CHART_COLORS = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(201, 203, 207)'
        };

        // Color Variables
        const primaryColorShade = '#836AF9',
            yellowColor = '#ffe800',
            successColorShade = '#28dac6',
            warningColorShade = '#ffe802',
            warningLightColor = '#FDAC34',
            infoColorShade = '#299AFF',
            greyColor = '#4F5D70',
            blueColor = '#2c9aff',
            blueLightColor = '#84D0FF',
            greyLightColor = '#EDF1F4',
            tooltipShadow = 'rgba(0, 0, 0, 0.25)',
            lineChartPrimary = '#666ee8',
            lineChartDanger = '#ff4961',
            labelColor = '#6e6b7b',
            grid_line_color = 'rgba(200, 200, 200, 0.2)'; // RGBA color helps in dark layout

        $(window).on('load', function () {
            loadChartOrderSource();
            loadChartOrderCustomer();
            loadChartOrderRevenue();
            loadChartOrderEmployee();

            // Detect Dark Layout
            if ($('html').hasClass('dark-layout')) {
                labelColor = '#b4b7bd';
            }

            // Wrap charts with div of height according to their data-height
            if (chartWrapper.length) {
                chartWrapper.each(function () {
                    $(this).wrap($('<div style="height:' + this.getAttribute('data-height') + 'px"></div>'));
                });
            }

            $(document).on('submit', '#ajax-search', function(e){
                e.preventDefault();
                const dateFrom = $('#date_from').val();
                const dateTo = $('#date_to').val();
                $('#label-from').html(dateFrom);
                $('#label-to').html(dateTo);

                loadChartOrderSource();
                loadChartOrderCustomer();
                loadChartOrderRevenue();
                loadChartOrderEmployee();
            });
        });

        function loadChartOrderSource(){
            if(chartPieSourceOrder){
                chartPieSourceOrder.destroy();
            }
            
            const dateFrom = $('#date_from').val();
            const dateTo = $('#date_to').val();
            const revenueType = $('input[name=revenue_type]:checked').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: '{{ route('api.chart.order.source') }}',
                data: {revenue_type: revenueType, date_from: dateFrom, date_to: dateTo},
                success: function (response) {
                    if(response.error == 0){
                        let dataChart = response.data;
                        initChartOrderSource(dataChart);
                    }
                }
            });
        }

        function loadChartOrderCustomer(){
            if(barChartCustomerTop){
                barChartCustomerTop.destroy();
            }

            const dateFrom = $('#date_from').val();
            const dateTo = $('#date_to').val();
            const revenueType = $('input[name=revenue_type]:checked').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: '{{ route('api.chart.order.customer') }}',
                data: {revenue_type: revenueType, date_from: dateFrom, date_to: dateTo},
                success: function (response) {
                    if(response.error == 0){
                        let dataChart = response.data;
                        initChartOrderTopCustomer(dataChart);
                    }
                }
            });
        }

        function loadChartOrderRevenue(){
            if(barChartRevenue){
                barChartRevenue.destroy();
            }

            const dateFrom = $('#date_from').val();
            const dateTo = $('#date_to').val();
            const revenueType = $('input[name=revenue_type]:checked').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: '{{ route('api.chart.order.revenue') }}',
                data: {revenue_type: revenueType, date_from: dateFrom, date_to: dateTo},
                success: function (response) {
                    if(response.error == 0){
                        let result = response.data;
                        let dataChart = result.data;
                        let days = result.days;
                        let type = result.type;

                        let count = days.length;

                        let new_array = [];
                        for (let i in days) {
                            if(dataChart[days[i]]){
                                new_array[` ${days[i]}`] = dataChart[days[i]];
                            }
                        }

                        let dataFinal = {};
                        dataFinal.label = Object.keys(new_array);
                        dataFinal.data = Object.values(new_array);
                        initChartOrderRevenue(dataFinal, type, dateFrom, dateTo);
                    }
                }
            });
        }

        function loadChartOrderEmployee(){
            if(barChartEmployee){
                barChartEmployee.destroy();
            }

            const dateFrom = $('#date_from').val();
            const dateTo = $('#date_to').val();
            const revenueType = $('input[name=revenue_type]:checked').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: '{{ route('api.chart.order.employee') }}',
                data: {revenue_type: revenueType, date_from: dateFrom, date_to: dateTo},
                success: function (response) {
                    if(response.error == 0){
                        let dataChart = response.data;
                        initChartOrderEmployee(dataChart, dateFrom, dateTo);
                    }
                }
            });
        }

        function initChartOrderSource(dataChart){
            const chartPieSourceOrderClass = $('.chart-pie-source-order');
            window.chartPieSourceOrder = new Chart(chartPieSourceOrderClass, {
                type: 'pie',
                options: {
                    maintainAspectRatio: false,
                    hover: {
                        animationDuration: 0
                    },
                    elements: {
                        rectangle: {
                            borderWidth: 2,
                            borderSkipped: 'bottom'
                        }
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    responsiveAnimationDuration: 500,
                    legend: {
                        display: true,
                        position: 'left'
                    },
                    title: {
                        display: true,
                        text: 'Thống kê doanh số theo nguồn khách hàng'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: true,
                        callbacks: {
                            title: function(tooltipItems, data) {
                                return tooltipItems[0].yLabel;
                            },
                            label: function (tooltipItem, data) {
                                let label = data.labels[tooltipItem.index];
                                let value = data.datasets[0].data[tooltipItem.index];
                                return ' ' + label + ': ' + encodeCurrencyFormat(value) + ' đ';
                            }
                        }
                    },
                },
                data: {
                    labels: dataChart.label,
                    datasets: [
                        {
                            barThickness: 15,
                            data: dataChart.data,
                            backgroundColor: Object.values(CHART_COLORS),
                            borderColor: '#fff'
                        }
                    ]
                }
            });

            chartPieSourceOrder.update();
        }

        function initChartOrderTopCustomer(dataChart){
            const barChartCustomerTopClass = $('.bar-chart-customer-order');
            window.barChartCustomerTop = new Chart(barChartCustomerTopClass, {
                type: 'horizontalBar',
                options: {
                    maintainAspectRatio: false,
                    hover: {
                        animationDuration: 0
                    },
                    elements: {
                        rectangle: {
                            borderWidth: 2,
                            borderSkipped: 'bottom'
                        }
                    },
                    responsive: true,
                    responsiveAnimationDuration: 500,
                    legend: {
                        display: false,
                        position: 'left'
                    },
                    title: {
                        display: true,
                        text: 'Top 10 khách hàng mua hàng nhiều nhất'
                    },
                    scales: {
                        yAxes: [{
                            scaleLabel: {
                                display: false,
                                labelString: 'Khách hàng'
                            }
                        }],
                        xAxes: [{
                            scaleLabel: {
                                display: false,
                                labelString: 'Triệu'
                            },
                            ticks: {
                                callback: function(value, index, ticks) {
                                    return `${encodeCurrencyFormat(value)} đ`;
                                }
                            }
                        }]
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: true,
                        callbacks: {
                            title: function(tooltipItems, data) {
                                return tooltipItems[0].yLabel;
                            },
                            label: function(tooltipItem, data) {
                                let label = data.labels[tooltipItem.index];
                                let value = data.datasets[0].data[tooltipItem.index];
                                return ' ' + label + ': ' + encodeCurrencyFormat(value) + ' đ';
                            }
                        }
                    },
                },
                data: {
                    labels: dataChart.label,
                    datasets: [
                        {
                            barThickness: 15,
                            data: dataChart.data,
                            backgroundColor: Object.values(CHART_COLORS),
                            borderColor: '#fff'
                        }
                    ]
                }
            });

            barChartCustomerTop.update();
        }

        function initChartOrderRevenue(dataChart, type, dateFrom, dateTo){
            const barChartRevenueClass = $('.bar-chart-revenue');

            if(type == 'day'){
                textLabel = 'Ngày';
            }
            else if(type == 'month'){
                textLabel = 'Tháng';
            }
            else if(type == 'year'){
                textLabel = 'Năm';
            }

            window.barChartRevenue = new Chart(barChartRevenueClass, {
                type: 'line',
                options: {
                    maintainAspectRatio: false,
                    hover: {
                        animationDuration: 0
                    },
                    elements: {
                        rectangle: {
                            borderWidth: 2,
                            borderSkipped: 'bottom'
                        }
                    },
                    responsive: true,
                    responsiveAnimationDuration: 500,
                    legend: {
                        display: false,
                        position: 'left'
                    },
                    title: {
                        display: true,
                        text: `Thống kê doanh số theo ${textLabel} (từ ${dateFrom} đến ${dateTo})`
                    },
                    scales: {
                        yAxes: [{
                            scaleLabel: {
                                display: false,
                                labelString: 'đ'
                            },
                            ticks: {
                                callback: function(value, index, ticks) {
                                    return `${encodeCurrencyFormat(value)} đ`;
                                }
                            }
                        }],
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: textLabel
                            },
                            ticks: {
                                // callback: function(value, index, ticks) {
                                //     return `Ngày ${value}`;
                                // }
                            }
                        }]
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: true,
                        callbacks: {
                            title: function(tooltipItems, data) {
                                return `${encodeCurrencyFormat(tooltipItems[0].yLabel)} đ`;
                            },
                            label: function(tooltipItem, data) {
                                let label = data.labels[tooltipItem.index];
                                let value = data.datasets[0].data[tooltipItem.index];
                                return `${textLabel} ${label}`;
                            }
                        }
                    },
                },
                data: {
                    labels: dataChart.label,
                    datasets: [
                        {
                            data: dataChart.data,
                            // backgroundColor: infoColorShade,
                            backgroundColor: 'transparent',
                            borderColor: blueColor
                        }
                    ]
                }
            });

            barChartRevenue.update();
        }

        function initChartOrderEmployee(dataChart, dateFrom, dateTo){
            const barChartEmployeeClass = $('.bar-chart-ex');

            window.barChartEmployee = new Chart(barChartEmployeeClass, {
                type: 'bar',
                options: {
                    maintainAspectRatio: false,
                    hover: {
                        animationDuration: 0
                    },
                    elements: {
                        rectangle: {
                            borderWidth: 2,
                            borderSkipped: 'bottom'
                        }
                    },
                    responsive: true,
                    responsiveAnimationDuration: 500,
                    legend: {
                        display: false,
                        position: 'left'
                    },
                    title: {
                        display: true,
                        text: `Thống kê doanh số theo nhân viên (từ ${dateFrom} đến ${dateTo})`
                    },
                    scales: {
                        yAxes: [{
                            scaleLabel: {
                                display: false,
                                labelString: 'đ'
                            },
                            ticks: {
                                callback: function(value, index, ticks) {
                                    return `${encodeCurrencyFormat(value)} đ`;
                                }
                            }
                        }],
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'Nhân viên'
                            },
                            ticks: {
                                // callback: function(value, index, ticks) {
                                //     return `Ngày ${value}`;
                                // }
                            }
                        }]
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: true,
                        callbacks: {
                            title: function(tooltipItems, data) {
                                return `${encodeCurrencyFormat(tooltipItems[0].yLabel)} đ`;
                            },
                            label: function(tooltipItem, data) {
                                let label = data.labels[tooltipItem.index];
                                let value = data.datasets[0].data[tooltipItem.index];
                                return label;
                            }
                        }
                    },
                },
                data: {
                    labels: dataChart.label,
                    datasets: [
                        {
                            data: dataChart.data,
                            backgroundColor: infoColorShade,
                            borderColor: blueColor
                        }
                    ]
                }
            });

            barChartEmployee.update();
        }
    </script>
    @endcan
@endsection