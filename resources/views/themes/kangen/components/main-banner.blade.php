<div class="main-banner">
    <div class="container">
        <div class="row">
            <div class="content-wrap">
                <div class="item left">
                    <div class="title">{{ theme('home_banner.title') }}</div>
                    <div class="content">{{ theme('home_banner.content') }}</div>
                    <a href="{{ theme('home_banner.link') }}" class="btn read-more">Xem thêm <i class="feather icon-chevrons-right"></i></a>
                </div>
                <div class="item right">
                    <img class="img-fluid" src="{{ asset(theme('home_banner.image')) }}" alt="Banner main"/>
                </div>
            </div>
        </div>
    </div>
</div>