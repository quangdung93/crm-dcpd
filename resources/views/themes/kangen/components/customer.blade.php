<section class="section-customer">
    <div class="container">
        <div class="section-title">
            <div class="icon">
                <i class="feather icon-users"></i>
            </div>
            <h3><a href="#">KHÁCH HÀNG NÓI GÌ VỀ CHÚNG TÔI</a></h3>
            <p class="mt-4">Đừng nghe chúng tôi nói, hãy nhìn vào những nhận xét của khách hàng đã sử dụng dịch vụ của chúng tôi.</p>
        </div>
        <div class="content">
            <div class="cusomter-slider">
                <div class="item">
                    <div class="image">
                        <img class="img-fuild lazy" data-src="https://kangenvietnam.vn/uploads/2018/02/ts-ngo-duc-vuong-150x150.jpg" alt="" />
                    </div>
                    <div class="info">Nước kiềm Kangen của tập toàn Enagic là nguồn nước hỗ trợ chữa bệnh cho tôi. Nguồn nước Kangen có tính dương nên khi uống sẽ quân bình tính âm trong cơ thể bạn. Tôi khuyên dùng trong việc điều trị chữa bệnh và dưỡng sinh.</div>
                    <div class="customer-name">
                        <h5>TS. NGÔ ĐỨC VƯỢNG</h5>
                        <p>Lương Y Quốc Gia - ĐH Quốc Gia HN</p>
                    </div>
                </div>
                <div class="item">
                    <div class="image">
                        <img class="img-fuild lazy" data-src="https://kangenvietnam.vn/uploads/2018/07/bac-si-tien-si-HIROMI-SHINYA-150x150.jpg" alt="" />
                    </div>
                    <div class="info">Với 6 - 10 ly nước Kangen 9.5 mỗi ngày sẽ giúp cơ thể loại bỏ được một lượng axit gây hại trong cơ thể. Nước Kangen giúp tăng lượng hồng huyết cầu lên não giúp phòng chống được bệnh tai biến mạch mãu não và đột quỵ.</div>
                    <div class="customer-name">
                        <h5>GS-BS. HIROMI SHINYA</h5>
                        <p>Giáo Sư - Bác Sĩ phẫu thuật ĐH Y Khoa Albert Einstein</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>