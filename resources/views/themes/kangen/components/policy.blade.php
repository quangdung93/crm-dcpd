<section class="section-policy" style="background-image:url('{{ asset('assets/images/bg-policy.jpg') }}')">
    <div class="container">
        <div class="policy-wrapper">
            <div class="title">Chính sách bảo hành và những quy định của tập đoàn</div>
            <div class="content">
                <div class="policy-item">
                    <img class="image lazy" data-src="{{ asset('assets/images/policy-icon-1.png') }}" alt="">
                    <div class="policy-item-title">Sản phẩm chất lượng</div>
                    <div class="policy-item-des text-center">Công ty trực tiếp phân phối dòng máy điện giải kangen từ tập đoàn Enagic Nhật Bản đứng top đầu thế giới.</div>
                    <a href="{{ url('/may-loc-nuoc-kangen') }}" class="btn btn-kangen">Xem</a>
                </div>
                <div class="policy-item">
                    <img class="image lazy" data-src="{{ asset('assets/images/policy-icon-2.png') }}" alt="">
                    <div class="policy-item-title">Giá cả cạnh tranh</div>
                    <div class="policy-item-des text-center">Đưa ra nhiều sự lựa chọn cho khách hàng với giá thành ưu đãi nhất, chất lượng nhất.</div>
                    <a href="{{ url('/kinh-nghiem-hay/gia-may-loc-nuoc-kangen') }}" class="btn btn-kangen">Xem</a>
                </div>
                <div class="policy-item">
                    <img class="image lazy" data-src="{{ asset('assets/images/policy-icon-3.png') }}" alt="">
                    <div class="policy-item-title">Vận chuyển an toàn</div>
                    <div class="policy-item-des text-center">Vận chuyển miễn phí ở khu vực thành phố HCM, Hà Nội và các tỉnh lân cận trên toàn quốc.</div>
                    <a href="{{ url('/dich-vu-kangen') }}" class="btn btn-kangen">Xem</a>
                </div>
            </div>
        </div>
    </div>
</section>