$(function () {
    // MENU
    //Localstorage Menu State
    if(localStorage.getItem('menu_type') == 'mini'){
        $('.pcoded-wrapper').addClass('mini-menu');
        $('#mobile-collapse i').removeClass('icon-toggle-right');
        $('#mobile-collapse i').addClass('icon-toggle-left');
    }
    else{
        $('.pcoded-wrapper').removeClass('mini-menu');
        $('#mobile-collapse i').removeClass('icon-toggle-left');
        $('#mobile-collapse i').addClass('icon-toggle-right');
    }

    //Active Menu current (Menu Admin)
    $('ul.pcoded-submenu li').each(function(){
        if($(this).hasClass('active')){
            $(this).closest('.pcoded-hasmenu').addClass('pcoded-trigger');
        }
    });

    //END menu

    //Init select input
    $('.select2').select2();

    //Datetime
    $('.datetime-pick').datepicker({format: 'dd/mm/yyyy'});

    //button filter month
    $(document).on('click', '#current-week', function(){
        setCurrentWeek();
        resetClassBtn($(this));
    });

    $(document).on('click', '#current-quarter', function(){
        setCurrentQuarter();
        resetClassBtn($(this));
    });

    $(document).on('click', '#current-month', function(){
        setCurrentMonth();
        resetClassBtn($(this));
    });

    $(document).on('click', '#current-year', function(){
        setCurrentYear();
        resetClassBtn($(this));
    });

    // Multiple swithces
    var elem = Array.prototype.slice.call(document.querySelectorAll('.js-single'));
    elem.forEach(function(html) {
        var switchery = new Switchery(html, { color: '#4680ff', jackColor: '#fff' });
    });

    $(document).ready(function(){
        //Auto hide success & error alert after 3s
        hideSuccessMessage();
        hideErrorMessage();

        //Format Money
        window.formatMoney = function(str) {
            var strTemp = getNumber(str);
            if (strTemp.length <= 3)
                return strTemp;
            strResult = "";
            for (var i = 0; i < strTemp.length; i++)
                strTemp = strTemp.replace(",", "");
            var m = strTemp.lastIndexOf(".");
            if (m == -1) {
                for (var i = strTemp.length; i >= 0; i--) {
                    if (strResult.length > 0 && (strTemp.length - i - 1) % 3 == 0)
                        strResult = "," + strResult;
                    strResult = strTemp.substring(i, i + 1) + strResult;
                }
            } else {
                var strphannguyen = strTemp.substring(0, strTemp.lastIndexOf("."));
                var strphanthapphan = strTemp.substring(strTemp.lastIndexOf("."),
                        strTemp.length);
                var tam = 0;
                for (var i = strphannguyen.length; i >= 0; i--) {

                    if (strResult.length > 0 && tam == 4) {
                        strResult = "," + strResult;
                        tam = 1;
                    }

                    strResult = strphannguyen.substring(i, i + 1) + strResult;
                    tam = tam + 1;
                }
                strResult = strResult + strphanthapphan;
            }
            return strResult;
        }

        function getNumber(str) {
            var count = 0;
            for (var i = 0; i < str.length; i++) {
                var temp = str.substring(i, i + 1);
                if (!(temp == "," || temp == "." || (temp >= 0 && temp <= 9))) {
                    alert('Giá trị nhập vào phải là số');
                    return str.substring(0, i);
                }
                if (temp == " ")
                    return str.substring(0, i);
                if (temp == ".") {
                    if (count > 0)
                        return str.substring(0, ipubl_date);
                    count++;
                }
            }
            return str;
        }

        window.showErrorMessage = function(message) {
            $('.alert.alert-danger .error-message').empty().html(message);
            $('.alert.alert-danger').fadeIn();
            scrollToTop();
        }
        window.clearMessage = function() {
            hideSuccessMessage();
            hideErrorMessage();
        }

        window.scrollToElement = function(element, delay = 20, time = 500, margin = 10) {
            $("html, body").delay(delay).animate({
                scrollTop: element.offset().top - margin
            }, time);
        }
    
    });
    
    timeSince = function(date) {

        var seconds = Math.floor((new Date() - date) / 1000);
      
        var interval = seconds / 31536000;
      
        if (interval > 1) {
          return Math.floor(interval) + " năm trước";
        }
        interval = seconds / 2592000;
        if (interval > 1) {
          return Math.floor(interval) + " tháng trước";
        }
        interval = seconds / 86400;
        if (interval > 1) {
          return Math.floor(interval) + " ngày trước";
        }
        interval = seconds / 3600;
        if (interval > 1) {
          return Math.floor(interval) + " giờ trước";
        }
        interval = seconds / 60;
        if (interval > 1) {
          return Math.floor(interval) + " phút trước";
        }
        return Math.floor(seconds) + " giây trước";
    }

    $('#datatable').DataTable({
        filter:true,
        fixedHeader: {
            headerOffset: 50,
        },
        bStateSave: true,
        "oLanguage": {
            "sInfo":         "Hiển thị _START_ đến _END_ của _TOTAL_ dòng",
            "sInfoEmpty":      "Hiển thị 0 đến 0 của 0 dòng",
            "sLengthMenu":     "Hiển thị _MENU_ dòng",
            "sEmptyTable":     "Không có dữ liệu",
            "sSearch":         "Tìm kiếm:",
            "sProcessing":     "Đang tải...",
            "oPaginate": {
                "sFirst":      "Trang đầu",
                "sLast":       "Trang cuối",
                "sNext":       "Trang kế",
                "sPrevious":   "Trang trước"
            }
        }
    });

    $('.datatable').DataTable({
        filter:true,
        fixedHeader: {
            headerOffset: 50,
        },
        bStateSave: true,
        "oLanguage": {
            "sInfo":         "Hiển thị _START_ đến _END_ của _TOTAL_ dòng",
            "sInfoEmpty":      "Hiển thị 0 đến 0 của 0 dòng",
            "sLengthMenu":     "Hiển thị _MENU_ dòng",
            "sEmptyTable":     "Không có dữ liệu",
            "sSearch":         "Tìm kiếm:",
            "sProcessing":     "Đang tải...",
            "oPaginate": {
                "sFirst":      "Trang đầu",
                "sLast":       "Trang cuối",
                "sNext":       "Trang kế",
                "sPrevious":   "Trang trước"
            }
        }
    });

    formatMoney = function(str) {
        var strTemp = getNumber(str);
        console.log('strTemp', strTemp);
        if (strTemp.length <= 3)
            return strTemp;
        strResult = "";
        for (var i = 0; i < strTemp.length; i++)
            strTemp = strTemp.replace(",", "");
        var m = strTemp.lastIndexOf(".");
        if (m == -1) {
            for (var i = strTemp.length; i >= 0; i--) {
                if (strResult.length > 0 && (strTemp.length - i - 1) % 3 == 0)
                    strResult = "," + strResult;
                strResult = strTemp.substring(i, i + 1) + strResult;
            }
        } else {
            var strphannguyen = strTemp.substring(0, strTemp.lastIndexOf("."));
            var strphanthapphan = strTemp.substring(strTemp.lastIndexOf("."),
                    strTemp.length);
            var tam = 0;
            for (var i = strphannguyen.length; i >= 0; i--) {

                if (strResult.length > 0 && tam == 4) {
                    strResult = "," + strResult;
                    tam = 1;
                }

                strResult = strphannguyen.substring(i, i + 1) + strResult;
                tam = tam + 1;
            }
            strResult = strResult + strphanthapphan;
        }
        return strResult;
    }

    function getNumber(str) {
        var count = 0;
        for (var i = 0; i < str.length; i++) {
            var temp = str.substring(i, i + 1);
            if (!(temp == "," || temp == "." || (temp >= 0 && temp <= 9))) {
                alert('Giá trị nhập vào phải là số');
                return str.substring(0, i);
            }
            if (temp == " ")
                return str.substring(0, i);
            if (temp == ".") {
                if (count > 0)
                    return str.substring(0, ipubl_date);
                count++;
            }
        }
        return str;
    }

    encodeCurrencyFormat = function(obs) {
        return obs.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    $(".txtMoney").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    //Datatable server side
    showDataTableServerSide = function(element, ajax_url, columns){
        let config = {
            // fixedHeader: {
            //     headerOffset: 50,
            // },
            destroy: true,
            processing: true,
            serverSide: true,
            searching: false,
            filter: true,
            // sort:true,
            // aaSorting: [[0, 'desc']],
            bStateSave: true,
            ajax: {
                "url": ajax_url,
                "type" : "GET",
            },
            columns: columns,
            "oLanguage": {
                "sInfo":         "Hiển thị _START_ đến _END_ của _TOTAL_ dòng",
                "sInfoEmpty":      "Hiển thị 0 đến 0 của 0 dòng",
                "sLengthMenu":     "Hiển thị _MENU_ dòng",
                "sEmptyTable":     "Không có dữ liệu",
                "sSearch":         "Tìm kiếm:",
                "sProcessing":     "Đang tải...",
                "oPaginate": {
                    "sFirst":      "Trang đầu",
                    "sLast":       "Trang cuối",
                    "sNext":       "Trang kế",
                    "sPrevious":   "Trang trước"
                }
            }
        };

        element.DataTable(config);
    }

    convert_slug = function(title){
        var slug;
        //Đổi chữ hoa thành chữ thường
        slug = title.toLowerCase();
    
        //Đổi ký tự có dấu thành không dấu
        slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        slug = slug.replace(/đ/gi, 'd');
        //Xóa các ký tự đặt biệt
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        //Đổi khoảng trắng thành ký tự gạch ngang
        slug = slug.replace(/ /gi, "-");
        //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
        //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
        slug = slug.replace(/\-\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-/gi, '-');
        slug = slug.replace(/\-\-/gi, '-');
        //Xóa các ký tự gạch ngang ở đầu và cuối
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');
        //In slug ra textbox có id “slug”

        $('input[name="slug"]').val(slug);
    }

    setCurrentMonth = function() {
        var date = new Date;
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        $("#date-from-pick").datepicker('setDate', firstDay);
        $("#date-to-pick").datepicker('setDate', lastDay);
    }

    setCurrentQuarter = function() {
        var d = new Date();
        var quarter = Math.floor((d.getMonth() / 3));
        var firstDate = new Date(d.getFullYear(), quarter * 3, 1);
        var lastDay = new Date(firstDate.getFullYear(), firstDate.getMonth() + 3, 0);
        $("#date-from-pick").datepicker('setDate', firstDate);
        $("#date-to-pick").datepicker('setDate', lastDay);
    }

    setCurrentWeek = function() {
        var curr = new Date();
        var firstDay = new Date(curr.setDate(curr.getDate() - curr.getDay()));
        var lastDay = new Date(curr.setDate(curr.getDate() - curr.getDay() + 6));
        $("#date-from-pick").datepicker('setDate', firstDay);
        $("#date-to-pick").datepicker('setDate', lastDay);
    }

    setCurrentYear = function() {
        var d = new Date();
        var firstDate = new Date(d.getFullYear(), 0, 1);
        var lastDay = new Date(d.getFullYear(), 11, 31);
        $("#date-from-pick").datepicker('setDate', firstDate);
        $("#date-to-pick").datepicker('setDate', lastDay);
    }

    resetClassBtn = function(self){
        $('.order-btn-calendar button').removeClass('btn-primary');
        self.addClass('btn-primary');
    }

    getWarehouseByStore = function(){
        setTimeout(() => {
            const storeId = $("#store-id option:selected").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: `${URL_MAIN}admin/warehouses/store/${storeId}`,
                data: {store_id: storeId},
                success: function (response) {
                    if (response.error == 0) {
                        $('.warehouse-box').show();
                        $('#warehouses').empty();
                        $('#warehouses').append(response.data);
                        $("#warehouses").trigger("chosen:updated");
                    }
                }
            });
        }, 500);
    }

});