<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\ApiController;
use App\Http\Controllers\Admin\LogController;
use App\Http\Controllers\Site\CartController;
use App\Http\Controllers\Site\HomeController;
use App\Http\Controllers\Admin\DebtController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\TaskController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Site\RouteController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\ImageController;
use App\Http\Controllers\Admin\MediaController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\StoreController;
use App\Http\Controllers\Admin\ThemeController;
use App\Http\Controllers\Admin\ExportController;
use App\Http\Controllers\Admin\ImportController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\Admin\CommentController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ReceiptController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Site\CheckoutController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\RedirectController;
use App\Http\Controllers\Admin\ShippingController;
use App\Http\Controllers\Admin\SupplierController;
use App\Http\Controllers\Site\WordpressController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\InventoryController;
use App\Http\Controllers\Admin\ShortcodeController;
use App\Http\Controllers\Admin\WarehouseController;
use App\Http\Controllers\Admin\ConvertDataController;
use App\Http\Controllers\Admin\PostCategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Login
Route::get('/login', [LoginController::class, 'login']);
Route::post('/login', [LoginController::class, 'postLogin'])->name('login');

//****************/ ADMIN /*********************

Route::group(['prefix' => 'admin','middleware' => 'auth'], function () {
    Route::get('/', function(){
        return redirect('admin/dashbroad', 301);
    });

    Route::get('system-logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);
    
    Route::get('/dashbroad', [DashboardController::class, 'index'])->name('dashbroad');
    Route::get('/logout', [LoginController::class, 'logout']);

    //User
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', [UserController::class, 'index'])->middleware('can:read_users');
        Route::get('/create', [UserController::class, 'create'])->middleware('can:add_users');
        Route::post('/create', [UserController::class, 'store'])->middleware('can:add_users');
        Route::get('/edit/{id}', [UserController::class, 'edit']);
        Route::post('/edit/{id}', [UserController::class, 'update']);
        Route::get('/delete/{id}', [UserController::class, 'destroy'])->middleware('can:delete_users');
    });

    //Role
    Route::group(['prefix' => 'roles', 'middleware' => ['can:read_roles']], function () {
        Route::get('/', [RoleController::class, 'index']);
        Route::get('/create', [RoleController::class, 'create'])->middleware('can:add_roles');
        Route::post('/create', [RoleController::class, 'store'])->middleware('can:add_roles');
        Route::get('/edit/{id}', [RoleController::class, 'edit'])->middleware('can:edit_roles');
        Route::post('/edit/{id}', [RoleController::class, 'update'])->middleware('can:edit_roles');

        //Create permission // url : /admin/roles/create_permission/{permission_group_name}
        Route::get('/create_permission/{permission}', [RoleController::class, 'createPermission'])
        ->middleware('role:'.config('permission.role_dev'));
    });

    //Product
    Route::group(['prefix' => 'products', 'middleware' => ['can:read_products']], function () {
        Route::get('/', [ProductController::class, 'index'])->name('products.index');
        Route::get('datatable', [ProductController::class,'getDatatable'])->name('products.view');
        Route::get('list', [ProductController::class,'getLists'])->name('product.list');
        Route::get('/create', [ProductController::class, 'create'])->middleware('can:add_products');
        Route::get('/search', [ProductController::class, 'search']);
        Route::post('/create', [ProductController::class, 'store'])->middleware('can:add_products');
        Route::get('/edit/{product}', [ProductController::class, 'edit'])->middleware('can:edit_products');
        Route::post('/edit/{id}', [ProductController::class, 'update'])->middleware('can:edit_products');
        Route::get('/delete/{id}', [ProductController::class, 'destroy'])->middleware('can:delete_products');
    });

    //Page
    Route::group(['prefix' => 'pages', 'middleware' => ['can:read_pages']], function () {
        Route::get('/', [PageController::class, 'index']);
        Route::get('/create', [PageController::class, 'create'])->middleware('can:add_pages');
        Route::post('/create', [PageController::class, 'store'])->middleware('can:add_pages');
        Route::get('/edit/{id}', [PageController::class, 'edit'])->middleware('can:edit_pages');
        Route::post('/edit/{id}', [PageController::class, 'update'])->middleware('can:edit_pages');
        Route::get('/delete/{id}', [PageController::class, 'destroy'])->middleware('can:delete_pages');
    });

    //Post
    Route::group(['prefix' => 'posts', 'middleware' => ['can:read_posts']], function () {
        Route::get('/', [PostController::class, 'index'])->name('posts.index');
        Route::get('datatable', [PostController::class,'getDatatable'])->name('posts.view');
        Route::get('/create', [PostController::class, 'create'])->middleware('can:add_posts');
        Route::post('/create', [PostController::class, 'store'])->middleware('can:add_posts');
        Route::get('/edit/{id}', [PostController::class, 'edit'])->middleware('can:edit_posts');
        Route::post('/edit/{id}', [PostController::class, 'update'])->middleware('can:edit_posts');
        Route::get('/delete/{id}', [PostController::class, 'destroy'])->middleware('can:delete_posts');
    });

    //Category Post
    Route::group(['prefix' => 'category_posts', 'middleware' => ['can:read_posts']], function () {
        Route::get('/', [PostCategoryController::class, 'index']);
        Route::get('/create', [PostCategoryController::class, 'create'])->middleware('can:add_posts');
        Route::post('/create', [PostCategoryController::class, 'store'])->middleware('can:add_posts');
        Route::get('/edit/{id}', [PostCategoryController::class, 'edit'])->middleware('can:edit_posts');
        Route::post('/edit/{id}', [PostCategoryController::class, 'update'])->middleware('can:edit_posts');
        Route::get('/delete/{id}', [PostCategoryController::class, 'destroy'])->middleware('can:delete_posts');
    });

    //Menu
    Route::group(['prefix' => 'menus', 'middleware' => ['can:read_menus']], function () {
        Route::get('/', [MenuController::class, 'index']);
        Route::get('/create', [MenuController::class, 'create'])->middleware('can:add_menus');
        Route::post('/create', [MenuController::class, 'store'])->middleware('can:add_menus');
        Route::get('/edit/{id}', [MenuController::class, 'edit'])->middleware('can:edit_menus');
        Route::post('/edit/{id}', [MenuController::class, 'update'])->middleware('can:edit_menus');
        Route::get('/builder/{id}', [MenuController::class, 'builder'])->middleware('can:edit_menus');
        Route::get('/delete/{id}', [MenuController::class, 'destroy'])->middleware('role:'.config('permission.role_dev'));

        //Menu Items
        Route::group(['prefix' => 'item'], function () {
            Route::post('/order', [MenuController::class, 'orderItem'])
            ->middleware('can:edit_menus')->name('menus.item.order');
            Route::post('/add', [MenuController::class, 'addItem'])
            ->middleware('can:add_menus')->name('menus.item.add');
            Route::post('/add_custom', [MenuController::class, 'addItemCustom'])
            ->middleware('can:add_menus')->name('menus.item.addcustom');
            Route::post('/update/{id}', [MenuController::class, 'updateItem'])
            ->middleware('can:edit_menus')->name('menus.item.update');
            Route::get('/delete/{id}', [MenuController::class, 'deleteItem'])
            ->middleware('can:delete_menus')->name('menus.item.delete');
        });
    });


    //Setting
    Route::group(['prefix' => 'settings', 'middleware' => ['can:read_settings']], function () {
        Route::get('/', [SettingController::class, 'index']);
        Route::post('/', [SettingController::class, 'update'])->middleware('can:edit_settings');
        Route::post('/create', [SettingController::class, 'store'])
        ->middleware('role:'.config('permission.role_dev'))->name('settings.add');
        Route::get('/delete/{id}', [SettingController::class, 'destroy'])
        ->middleware('role:'.config('permission.role_dev'))->name('settings.delete');
        Route::post('/order', [SettingController::class, 'order'])->name('settings.order');

        Route::get('points', [SettingController::class,'getConfigProduct'])->middleware('can:edit_settings')->name('admin.config.points');
        Route::post('points', [SettingController::class,'postConfigProduct'])->middleware('can:edit_settings');
    });

    //Shortcode
    Route::group(['prefix' => 'shortcodes', 'middleware' => ['can:read_settings']], function () {
        Route::get('/', [ShortcodeController::class, 'index']);
        Route::get('/create', [ShortcodeController::class, 'create'])->middleware('can:add_settings');
        Route::post('/create', [ShortcodeController::class, 'store'])->middleware('can:add_settings');
        Route::get('/edit/{id}', [ShortcodeController::class, 'edit'])->middleware('can:edit_settings');
        Route::post('/edit/{id}', [ShortcodeController::class, 'update'])->middleware('can:edit_settings');
        Route::get('/delete/{id}', [ShortcodeController::class, 'destroy'])->middleware('can:delete_settings');
    });

    //Media
    Route::group(['prefix' => 'media', 'middleware' => ['can:read_media']], function () {
        Route::get('/', [MediaController::class, 'index']);
        Route::group(['prefix' => 'filemanager'], function () {
            \UniSharp\LaravelFilemanager\Lfm::routes();
        });
    });

    //Category Product
    Route::group(['prefix' => 'categories', 'middleware' => ['can:read_products']], function () {
        Route::get('/', [CategoryController::class, 'index']);
        Route::get('/create', [CategoryController::class, 'create'])->middleware('can:add_products');
        Route::post('/create', [CategoryController::class, 'store'])->middleware('can:add_products');
        Route::get('/edit/{id}', [CategoryController::class, 'edit'])->middleware('can:edit_products');
        Route::post('/edit/{id}', [CategoryController::class, 'update'])->middleware('can:edit_products');
        Route::get('/delete/{id}', [CategoryController::class, 'destroy'])->middleware('can:delete_products');
    });

    //Supplier
    Route::group(['prefix' => 'suppliers', 'middleware' => ['can:read_products']], function () {
        Route::get('/', [SupplierController::class, 'index']);
        Route::get('/create', [SupplierController::class, 'create'])->middleware('can:add_products');
        Route::post('/create', [SupplierController::class, 'store'])->middleware('can:add_products');
        Route::get('/edit/{id}', [SupplierController::class, 'edit'])->middleware('can:edit_products');
        Route::post('/edit/{id}', [SupplierController::class, 'update'])->middleware('can:edit_products');
        Route::get('/delete/{id}', [SupplierController::class, 'destroy'])->middleware('can:delete_products');
    });

    //Warehouse
    Route::group(['prefix' => 'warehouses', 'middleware' => ['can:read_products']], function () {
        Route::get('/', [WarehouseController::class, 'index']);
        Route::get('/create', [WarehouseController::class, 'create'])->middleware('can:add_warehouses');
        Route::post('/create', [WarehouseController::class, 'store'])->middleware('can:add_warehouses');
        Route::get('/edit/{id}', [WarehouseController::class, 'edit'])->middleware('can:edit_warehouses');
        Route::post('/edit/{id}', [WarehouseController::class, 'update'])->middleware('can:edit_warehouses');
        Route::get('/delete/{id}', [WarehouseController::class, 'destroy'])->middleware('can:delete_warehouses');

        //Api
        Route::post('/store/{store_id}', [WarehouseController::class, 'getWarehouseByStoreId'])->name('warehouse.store');
    });

    //Store
    Route::group(['prefix' => 'stores'], function () {
        Route::get('/', [StoreController::class, 'index']);
        Route::get('/create', [StoreController::class, 'create'])->middleware('can:add_stores');
        Route::post('/create', [StoreController::class, 'store'])->middleware('can:add_stores');
        Route::get('/edit/{id}', [StoreController::class, 'edit'])->middleware('can:edit_stores');
        Route::post('/edit/{id}', [StoreController::class, 'update'])->middleware('can:edit_stores');
        Route::get('/delete/{id}', [StoreController::class, 'destroy'])->middleware('can:delete_stores');

        //Api
        Route::post('active/{store_id}', [StoreController::class, 'postActiveStore']);
        Route::get('check-store', [StoreController::class, 'checkCurrentStore']);
    });

    //Brand
    Route::group(['prefix' => 'brands', 'middleware' => ['can:read_products']], function () {
        Route::get('/', [BrandController::class, 'index']);
        Route::get('/create', [BrandController::class, 'create'])->middleware('can:add_products');
        Route::post('/create', [BrandController::class, 'store'])->middleware('can:add_products');
        Route::get('/edit/{id}', [BrandController::class, 'edit'])->middleware('can:edit_products');
        Route::post('/edit/{id}', [BrandController::class, 'update'])->middleware('can:edit_products');
        Route::get('/delete/{id}', [BrandController::class, 'destroy'])->middleware('can:delete_products');
    });

    //Themes
    Route::group(['prefix' => 'themes', 'middleware' => ['can:read_themes']], function () {
        Route::get('/', [ThemeController::class, 'index']);
        Route::get('/edit/{id}', [ThemeController::class, 'edit'])->middleware('can:edit_themes');
        Route::post('/edit/{id}', [ThemeController::class, 'update'])->middleware('can:edit_themes');
    });

    //Images
    Route::group(['prefix' => 'images'], function () {
        Route::post('/upload', [ImageController::class, 'upload'])->name('images.upload');
        Route::post('/order', [ImageController::class, 'order'])->name('images.order');
        Route::post('/delete', [ImageController::class, 'delete'])->name('images.delete');
    });

    //Redirect
    Route::group(['prefix' => 'redirects', 'middleware' => ['can:read_redirects']], function () {
        Route::get('/', [RedirectController::class, 'index']);
        Route::get('/create', [RedirectController::class, 'create'])->middleware('can:add_redirects');
        Route::post('/create', [RedirectController::class, 'store'])->middleware('can:add_redirects');
        Route::get('/edit/{id}', [RedirectController::class, 'edit'])->middleware('can:edit_redirects');
        Route::post('/edit/{id}', [RedirectController::class, 'update'])->middleware('can:edit_redirects');
        Route::get('/delete/{id}', [RedirectController::class, 'destroy'])->middleware('can:delete_redirects');
    });

    //Logs
    Route::group(['prefix' => 'logs', 'middleware' => ['can:read_logs']], function () {
        Route::get('/', [LogController::class, 'index']);
        Route::get('datatable', [LogController::class,'getDatatable'])->name('logs.view');
        Route::get('details/{id}', [LogController::class,'show']);
    });

    //Convert data
    Route::group(['prefix' => 'convert'], function () {
        // Route::get('/user', [ConvertDataController::class, 'convertUsers']);
        // Route::get('/customer', [ConvertDataController::class, 'convertCustomer']);
        // Route::get('/customer-points', [ConvertDataController::class, 'convertCustomerPoints']);
        // Route::get('/customer-orders', [ConvertDataController::class, 'convertCustomerOrders']);
        Route::get('/customer-exchange', [ConvertDataController::class, 'exchangeCustomerBySale']);
        // Route::get('/customer-change', [ConvertDataController::class, 'exchangeCustomer']);
        // Route::get('/inventory-status', [ConvertDataController::class, 'convertOrderInventoryStatus']);
        // Route::get('/fix-export', [ConvertDataController::class, 'fixExport']);
        // Route::get('/brand', [ConvertDataController::class, 'convertBrand']);
        // Route::get('/product', [ConvertDataController::class, 'convertProduct']);
        // Route::get('/inventory', [ConvertDataController::class, 'convertInventory']);
        // Route::get('/inventory-detail', [ConvertDataController::class, 'convertInventoryCode']);
        // Route::get('/order', [ConvertDataController::class, 'convertOrders']);
        // Route::get('/export', [ConvertDataController::class, 'createExportProduct']);
    });

    //Order
    Route::group(['prefix' => 'orders'], function () {
        Route::get('/', [OrderController::class, 'index'])->name('orders.index')->middleware('can:read_orders');
        Route::get('datatable', [OrderController::class,'getDatatable'])->name('orders.view');
        Route::get('list', [OrderController::class,'getLists'])->name('order.list')->middleware('can:read_orders');
        Route::get('create', [OrderController::class,'create'])->middleware('can:add_orders')->middleware('check_store');
        Route::post('create', [OrderController::class,'store'])->middleware('can:add_orders');
        Route::get('/detail/{id}', [OrderController::class, 'detailOrder'])->name('orders.detail');
        Route::get('/delete/{id}', [OrderController::class, 'destroy'])->middleware('can:delete_orders');
        Route::get('/customer/{id}', [OrderController::class, 'getOrdersByCustomer']);
        Route::get('/shipping/{id}', [OrderController::class, 'getShipping']);
        Route::get('/print/{id}', [OrderController::class, 'printOrder'])->name('order.print');

        //Api
        Route::post('/update-api', [OrderController::class, 'updateApi'])->name('order.api.update')->middleware('can:edit_orders');
        Route::get('/fix', [OrderController::class, 'fix']);
    });

    //Report
    Route::group(['prefix' => 'reports'], function () {
        //Revenue
        Route::get('revenue', [ReportController::class, 'revenue'])->middleware('can:read_reports');
        Route::get('filter-revenue', [ReportController::class,'getFilterRevenue'])->name('report.revenue');
        Route::get('report-revenue', [ReportController::class, 'getFilterRevenueTotal'])->name('report.revenue.analytic');

        //Revenue Agency
        Route::get('revenue-agency', [ReportController::class, 'revenueAgency'])->middleware('can:read_reports');
        Route::get('filter-revenue-agency', [ReportController::class,'getFilterRevenueAgency'])->name('report.revenue.agency');
        Route::get('report-revenue-agency', [ReportController::class, 'getFilterRevenueAgencyTotal'])->name('report.revenue.agency.analytic');

        //Revenue Product
        Route::get('revenue-product', [ReportController::class, 'revenueProduct'])->middleware('can:read_reports');
        Route::get('filter-revenue-product', [ReportController::class,'getFilterRevenueProduct'])->name('report.revenue.product');
        Route::get('report-revenue-product', [ReportController::class, 'getFilterRevenueTotalProduct'])->name('report.revenue.analytic.product');

        //Import
        Route::get('import', [ReportController::class, 'import'])->middleware('can:read_product_imports');
        Route::get('filter-import', [ReportController::class,'getFilterImport'])->name('report.import');
        Route::get('report-import', [ReportController::class, 'getFilterImportAnalytic'])->name('report.import.analytic');

        //Export
        Route::get('export', [ReportController::class, 'export'])->middleware('can:read_product_exports');
        Route::get('filter-export', [ReportController::class,'getFilterExport'])->name('report.export');
        Route::get('report-export', [ReportController::class, 'getFilterExportAnalytic'])->name('report.export.analytic');
    });

    //Debt (công nợ)
    Route::group(['prefix' => 'debts'], function () {
        Route::get('/', [DebtController::class, 'index'])->middleware('can:read_debts');
        Route::get('get-list', [DebtController::class,'getLists'])->name('debt.list');
        Route::get('get-analytic', [DebtController::class, 'getAnalytic'])->name('debt.analytic');

        Route::get('export', [DebtController::class,'export'])->name('debt.export');
        Route::get('/print/{customer_id}', [DebtController::class, 'print'])->name('debt.print');
    });

    //Receipts (phiếu thu)
    Route::group(['prefix' => 'receipts'], function () {
        Route::get('/', [ReceiptController::class, 'index'])->name('receipt.index')->middleware('can:read_orders');
        Route::get('datatable', [ReceiptController::class,'getDatatable'])->name('receipt.view');
        Route::get('list', [ReceiptController::class,'getLists'])->name('receipt.list')->middleware('can:read_orders');
        Route::get('create/{debt_id?}', [ReceiptController::class,'create'])
                ->name('receipt.add')
                ->middleware('check_store')
                ->middleware('can:add_orders');
        Route::post('create/{debt_id?}', [ReceiptController::class,'store'])->middleware('can:add_orders');
        // Route::get('/detail/{id}', [ReceiptController::class, 'detailOrder'])->name('orders.detail');
        // Route::get('/delete/{id}', [ReceiptController::class, 'destroy'])->middleware('can:delete_orders');

        Route::get('/print/{id}', [ReceiptController::class, 'print'])->name('receipt.print');
    });

    //Shipping
    Route::group(['prefix' => 'shippings', 'middleware' => ['can:read_shippings']], function () {
        Route::get('/', [ShippingController::class, 'index'])->name('shipping.index');
        Route::get('datatable', [ShippingController::class,'getDatatable'])->name('shipping.view');
        Route::get('/order/{id}', [ShippingController::class, 'getShipping'])->middleware('can:add_shippings');
        Route::post('/order/{id}', [ShippingController::class, 'postShipping'])->name('shipping.add')->middleware('can:add_shippings');
    });

    //Imports
    Route::group(['prefix' => 'imports', 'middleware' => ['can:read_product_imports']], function () {
        Route::get('/', [ImportController::class, 'index'])->name('import.index');
        Route::get('datatable', [ImportController::class,'getDatatable'])->name('import.view');
        Route::get('create', [ImportController::class,'create'])
                ->middleware('can:add_product_imports')
                ->middleware('check_store');
        Route::post('create', [ImportController::class,'store'])->middleware('can:add_product_imports');
        Route::get('/detail/{id}', [ImportController::class, 'detailImport'])->name('import.detail');
        Route::get('/delete/{id}', [ImportController::class, 'destroy'])->middleware('can:delete_product_imports');
    });

    //Exports
    Route::group(['prefix' => 'exports', 'middleware' => ['can:read_product_exports']], function () {
        Route::get('/', [ExportController::class, 'index'])->name('export.index');
        Route::get('datatable', [ExportController::class,'getDatatable'])->name('export.view');
        Route::get('create', [ExportController::class,'create'])
                ->middleware('check_store')
                ->middleware('can:add_product_exports');
        Route::post('create', [ExportController::class,'store'])->name('export.add')->middleware('can:add_product_exports');
        Route::get('/detail/{id}', [ExportController::class, 'detailExport'])->name('export.detail');
        Route::get('/delete/{id}', [ExportController::class, 'destroy'])->middleware('can:delete_product_exports');
    });

    //Inventory
    Route::group(['prefix' => 'inventories', 'middleware' => ['can:read_inventories']], function () {
        Route::get('/', [InventoryController::class, 'index'])->name('inventory.index');
        Route::get('/detail/{product_id?}', [InventoryController::class, 'detail'])->name('inventory.detail');
        Route::get('/detail-view', [InventoryController::class, 'detailView'])->name('inventory.detail.view');
        Route::get('datatable', [InventoryController::class,'getDatatable'])->name('inventory.view');
        Route::get('list', [InventoryController::class,'getLists'])->name('inventory.list');
        Route::get('report-inventory', [InventoryController::class, 'getFilterInventoryAnalytic'])->name('inventory.analytic');
    });

    //Customer
    Route::group(['prefix' => 'customers'], function () {

        //Customer
        Route::get('/', [CustomerController::class, 'index'])->name('customers.index')->middleware('can:read_customers');
        Route::get('datatable', [CustomerController::class,'getDatatable'])->name('customers.view');
        Route::get('list', [CustomerController::class,'getLists'])->name('customers.list');
        
        //Agency
        Route::get('/agency', [CustomerController::class, 'listAgency'])->name('customers.index')->middleware('can:read_agents');;
        Route::get('datatable/agency', [CustomerController::class,'getDatatableAgency'])->name('agency.view');
        Route::get('list/agency', [CustomerController::class,'getListsAgency'])->name('agency.list');

        //Saler
        Route::get('/saler', [CustomerController::class, 'listSaler'])->name('customers.saler.index')->middleware('can:read_customer_salers');;
        Route::get('datatable/saler', [CustomerController::class,'getDatatableSaler'])->name('saler.view');
        Route::get('list/saler', [CustomerController::class,'getListsSaler'])->name('saler.list');

        Route::post('/care', [CustomerController::class, 'customerCare'])->name('customers.care')->middleware('can:edit_customers');
        Route::get('/search', [CustomerController::class, 'search']);
        Route::get('/get-detail/{id}', [CustomerController::class, 'getCustomerDetail']);
        Route::get('/detail/{id}', [CustomerController::class, 'detail'])->middleware('can:read_customers');
        Route::get('/create', [CustomerController::class, 'create'])->middleware('can:add_customers');
        Route::post('/create', [CustomerController::class, 'store'])->middleware('can:add_customers');
        Route::get('/edit/{customers}', [CustomerController::class, 'edit'])->middleware('can:edit_customers');
        Route::post('/edit/{id}', [CustomerController::class, 'update'])->middleware('can:edit_customers');
        Route::get('/delete/{id}', [CustomerController::class, 'destroy'])->middleware('can:delete_customers');

        //Api
        Route::post('/create-api', [CustomerController::class, 'createApi'])->name('customer.api.add');
        Route::post('/update-api', [CustomerController::class, 'updateApi'])->name('customer.api.update');
        Route::post('/update-points', [CustomerController::class, 'updatePointsApi'])->name('customer.api.update.points');
        Route::get('/convert-points', [CustomerController::class, 'calcPoint']);
    });

    //Task
    Route::group(['prefix' => 'tasks', 'middleware' => ['can:read_tasks']], function () {
        Route::get('/', [TaskController::class, 'index'])->name('task.index');
        Route::get('datatable', [TaskController::class,'getDatatable'])->name('task.view');
        Route::get('list', [TaskController::class,'getLists'])->name('task.list');
        Route::get('/delete/{id}', [TaskController::class, 'destroy'])->middleware('can:delete_tasks');

        //Api
        Route::post('/create-api', [TaskController::class, 'createApi'])->name('task.api.add');
        Route::post('/update-api', [TaskController::class, 'updateApi'])->name('task.api.update');
        Route::post('/show-edit', [TaskController::class, 'showEdit'])->name('task.show.edit');
        
    });

    Route::group(['prefix' => 'api'], function () {
        Route::group(['prefix' => 'chart'], function () {
            Route::post('/order-source', [ApiController::class, 'chartOrderSourceAction'])->name('api.chart.order.source');
            Route::post('/order-customer', [ApiController::class, 'chartOrderCustomerAction'])->name('api.chart.order.customer');
            Route::post('/order-revenue', [ApiController::class, 'chartOrderRevenueAction'])->name('api.chart.order.revenue');
            Route::post('/order-employee', [ApiController::class, 'chartOrderEmployeeAction'])->name('api.chart.order.employee');
        });
    });

    //Comment
    Route::group(['prefix' => 'comments', 'middleware' => ['can:read_comments']], function () {
        Route::get('/', [CommentController::class, 'index'])->name('comments.index');
        Route::post('/edit', [CommentController::class, 'edit'])->name('comments.edit')
        ->middleware('can:edit_comments');
        Route::get('/delete/{id}', [CommentController::class, 'delete'])->middleware('can:delete_comments');
    });

    Route::get('cache', function(){
        Cache::flush();
        return back();
    })->name('cache.clear');

});

//****************/ SITE /*********************

//Cart
Route::group(['prefix' => 'cart'], function () {
    Route::post('add_item', [CartController::class, 'add'])->name('cart.add');
    Route::post('add_item_single', [CartController::class, 'addByProductId']);
    Route::post('update', [CartController::class, 'update'])->name('cart.update');
    Route::post('remove', [CartController::class, 'remove'])->name('cart.remove');
});

//Checkout
Route::group(['prefix' => 'checkout'], function () {
    Route::get('/', [CheckoutController::class, 'index'])->name('checkout');
    Route::post('/', [CheckoutController::class, 'store'])->name('checkout.store');
    Route::get('load', [CheckoutController::class, 'load'])->name('checkout.load');
    Route::get('success/{order_id}', [CheckoutController::class, 'thanksPage'])->name('checkout.success');
});

//Comment
Route::group(['prefix' => 'comments'], function () {
    Route::post('/create', [App\Http\Controllers\Site\CommentController::class, 'create'])->name('comments.create');
});

//Rating
Route::group(['prefix' => 'ratings'], function () {
    Route::post('/increment', [App\Http\Controllers\Site\RatingController::class, 'increment'])->name('ratings.increment');
});

Route::post('register', [App\Http\Controllers\Site\CustomerController::class, 'register'])->name('register.form');
Route::get('province/{id}', [CheckoutController::class, 'getDistrict'])->name('district.get');
Route::get('district/{id}', [CheckoutController::class, 'getWard'])->name('ward.get');



//Site Route
Route::get('/', function(){
    return redirect('/login', 301);
});

Route::get('/{url}', [RouteController::class, 'handle'])->where('url', '.*');